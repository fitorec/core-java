## Aprendiendo core-java en 20 minutos.

>  **NOTA**: El proyecto **core-java** nace de la necesidad de crear un pequeña librería que diera soporte a las aplicaciones desarrolladas en Java. inicialmente el proyecto nace del `framework` [**ActiveJDBC**](http://javalite.io/activejdbc) tomando ideas que las implementamos a nuestra manera. Esto explica el nombre de **core-java** que intentaba ser el núcleo de las aplicaciones que lo implementen, sin embargo tras la implementación del **core-java** en múltiples proyectos nos hemos dado cuenta que la necesidad de crear un `framework` que no sólo de soporte a la capa de la base de datos si no que tambien atienda a la necesidad estructurar un buen proyecto en java.

> Por estas razones posiblemente este proyecto deje de existir para crear uno nuevo....


## Configuración:

Lo primero que tenemos que hacer es configurar los parametros que se requieren para establecer conexión con nuestra base de datos, para esto agregamos el siguiente código en la clase principal de nuestra aplicación:

```Java
	//importando
	import static com.mundosica.core.Model.DataBase.*;
	///
	config("bd"   , "nombre_base_datos");
	config("user" , "username");
	config("password" , "secret");
	// O bien
	config(
		"bd"   , "nombre_base_datos",
		"user" , "username",
		"password" , "secret"
	);
```

#### Parametros de configuración aceptados para la conexión a la base de datos:

 - **host** : El host en donde se encuentre el servidor de bases de datos
 - **port** : El puerto este por defecto es el puerto 3306
 - **bd**   : El nombre de la base de datos a conectar.
 - **user** : El nombre del usuario con permisos a la base de datos
 - **password**: La contraseña del usuario.

## Ejemplo:

Imagine que tiene la tabla **clientes** definida de la siguiente forma:

```SQL
CREATE TABLE `clientes` (
	`id` INT( 5 ) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`rfc` VARCHAR ( 50 ) NOT NULL UNIQUE KEY,
	`descripcion` VARCHAR( 150 ) NULL,
	`created` DATETIME NULL,
	`modified` DATETIME NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;
```

## Creando un Modelo

> Un modelo es responsable de la recuperación de datos convirtiéndolos en conceptos significativos para la aplicación, así como su procesamiento, validación, asociación y cualquier otra tarea relativa a la manipulación de dichos datos.

```Java
import com.mundosica.core.Model.Model;
class ClienteModel extends Model {
}
```

## Cargando datos:

```Java
ClienteModel cliente = new ClienteModel();
cliente.load("1");
// Mostrando todos sus datos
cliente.print();
// opteniendo el rfc del cliente
cliente.get("rfc");

// Creando un nuevo registro
ClienteModel nuevoCliente = new ClienteModel();
nuevoCliente.set(
	"rfc", "AM456",
	"descripcion", "Tienda de conveniencia"
);
nuevoCliente.save();

```

## Campos mágicos.

> Los campos mágicos son auto-generados en el momento de agregar/modificar un registro:


 - **created**: guardara la fecha/hora de cuando se genero el registro(`INSERT`)
 - **modified**: En el momento de realizar un update se cambiara.


## Valores por defecto.


## Soporte de Callbacks

Medodo  | Descripción | Callback previo | Callback posterior
------------- | ------------- | -------------------  | ----------------------
load() | Evento al cargar un registro | boolean beforeLoad() | boolean afterLoad()
save() | Evento al insertar(update) o actualizar(update) | boolean beforeSave() | boolean afterSave()
update() | Evento al actualizar un registro existente | boolean beforeUpdate() | boolean afterUpdate()
insert() | Evento al agregar un nuevo registro | boolean beforeInsert() | boolean afterInsert()
delete() | Evento al borrar un registro | boolean beforeDelete() | boolean afterDelete()
validate() | Evento al validar | boolean beforeValidation() | boolean afterValidation()

## Seguridad

El `core-java` cuanta con un sub-paquete `Security` que nos proporciona seguridad a la hora de cifrar contraseñas de los usuarios, esto usando mecanismos de cifrado irreversibles como `md5` y `sha1` similar a distintos sistemas como cakePHP, veamos un ejemplo:
```Java
	//importando paquete CakePHP
	import com.mundosica.core.Security.CakePHP;
	// Configuramos el mismo security Salt de nuestra aplicación
	CakePHP.securitySalt("some_salt");
	// ciframos algún password
	CakePHP.password("secret");
```
> **NOTA:** Resulta buena idea meter este código en el callback beforeInsert sobre el ClienteModel

# SWTF Dialog
Ventanas complementarias:

> **NOTA:** Este paquete seguramente sera remplazado por alguna nueva implementación que haga uso de la librería [javaFX.](http://docs.oracle.com/javase/8/javase-clienttechnologies.htm)

## Alert.

Util para mostrar un mensaje al usuario.

```Java
	SWTF.dialog.alert("Hola mundo");
```

## Error.

Util para mostrar un mensaje al usuario.

```Java
	SWTF.dialog.error("Error Seleccione una impresora valida");
```
## Warning.

Util para mostrar un mensaje al usuario.

```Java
	SWTF.dialog.warning("Precaucion etc.. etc..");
```


### Ideas por plasmar
Tenemos algunas ideas que esperamos pronto implementar, estas son algunas de ellas respecto al uso de los modelos.

```Java
ClienteModel c = new ClienteModel();
String query = "select .. from .. where .. limit 1";
c.select(query);
c.get("rfc");

// Uso de colectores.
Vector<ClienteModel> clientesDeudores = c.collector.select(
	"fields", "rfc, nombre, credito",
	"where", "credito > 0",
	"order", "credito ASC",
	"limit", "5"
);

for(i = 0; i< clientesDeudores.size(); i++) {
	ClienteModel cActual = clientesDeudores.get(i);
	System.out.println(cActual);
}

```

# Developers.

## Estilo de códifición

Nos basamos en el **Google Java Style** para mayor información visitar la siguiente url:


 - <https://google.github.io/styleguide/javaguide.html>
