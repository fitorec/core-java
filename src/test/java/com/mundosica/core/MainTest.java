package com.mundosica.core;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.mundosica.core.Model.DataBase;
import com.mundosica.core.Utils.TaskManager;
import com.mundosica.core.common.CoreApp;

public class MainTest {

	@BeforeClass
	public static void startTest() {
		System.out.println("AppSample.start()");
		DataBase.config("bd"   , "carniceria");
		DataBase.config("user" , "cruvi_user");
		DataBase.config("password" , "cruvi_pass"); 
	}

	@AfterClass
	public static void endTest() {
		TaskManager.end();
		DataBase.end();
		Thread.currentThread().interrupt();
	}
}
