package com.mundosica.core.ModelTest;


import static org.junit.Assert.*;

import java.util.Vector;

import org.junit.Ignore;
import org.junit.Test;

import com.mundosica.core.MainTest;
import com.mundosica.core.Model.ColumnMetadata;
import com.mundosica.core.Model.Model;
import com.mundosica.core.Model.QueryManager;

import static com.mundosica.core.common.Util.*;

/**
 * Prueaba para el modelo
 * 
 * @author fitorec
 */
public class ModelTest extends MainTest {
	
	@Ignore
	public void disabledAutoCreated() {
		ClienteModel cliente = new ClienteModel("7");
		cliente.print();
		for(int i=0; i < 200; i+=50) {
			cliente.load("7");
			int limite_credito = Integer.parseInt(cliente.get("limite_credito")) + i;
			cliente.set("limite_credito", limite_credito + "");
			cliente.save();
			cliente.print();
		}
		/*CorteModel corte = new CorteModel();
		//corte.auto_created = false;
		corte.set(
				"id", "1:" + QueryManager.now(),
				"sucursal_id", "2"
		);
		corte.save();*/
	}

	@Test
	public void pruebaCargar() {
		ClienteModel cliente = new ClienteModel();
		cliente.enableDebug();
		System.out.println("modified:" + cliente.get("modified"));
		cliente.load("1");
		cliente.print();
		
		cliente.load("60");
		cliente.print();
		
		cliente.load("1");
		cliente.print();
	}

	@Ignore
	public void corteTest() {
		CorteModel corte = new CorteModel();
		ClienteModel cliente = new ClienteModel();
		DiaModel dia = new DiaModel();
		System.out.println(corte.tableName());
		System.out.println(corte.toJSON());
		//
		Vector<ColumnMetadata>campos = corte.getFields();
		for (int i = 0; i < campos.size(); i++) {
			System.out.println(campos.get(i));
		}
		System.out.println(dia.tableName());
		campos = dia.getFields();
		for (int i = 0; i < campos.size(); i++) {
			System.out.println(campos.get(i));
		}
		ClienteModel cliente2 = new ClienteModel();
		cliente2.id(2);
		cliente2.load();
		
		cliente.id(1);
		cliente.load();
		System.out.println(cliente.get("razon_social"));
		System.out.println(cliente2.get("razon_social"));
		cliente.id(3);
		cliente.load();
		System.out.println(cliente.get("razon_social"));
	}

	/**
	 * Probando el load
	 */
	@Ignore
	public void loadTest() {
		ClienteModel cliente1 = new ClienteModel("1");
		ClienteModel cliente2 = new ClienteModel();
		cliente2.load("1");
		assertEquals("Los nombres no coinciden", cliente1.get("razon_social"), cliente2.get("razon_social"));
	}

	/**
	 * Probando cargado personalizado(Ver Modelo)
	 */
	@Ignore
	public void customLoad() {
		DiaModel dia = new DiaModel();
		dia.set(
				"sucursal", "matriz",
				"fecha", "2011-12-31"
		);
		System.out.println(dia.get("sucursal"));
		System.out.println(dia.get("fecha"));
		dia.load();
		System.out.println(dia.get("ip"));
		assertEquals("Ip Invalida", "189.130.16.247", dia.get("ip"));
	}

	@Ignore
	public void probandoInsert() {
		ClienteModel cliente = new ClienteModel();
		cliente.set(
			"razon_social", "El panda Show"
		);
		if (cliente.insert() ) {
			print("insercción correcta");
		} else {
			print("Error enla insercción");
		}
		print(cliente);
	}

	@Ignore
	public void probandoUpdate() {
		ClienteModel cliente = new ClienteModel("3825");
		cliente.set(
			"razon_social", "EdwinFitoRex"
		);
		if (cliente.update() ) {
			print("insercción correcta");
		} else {
			print("Error en la insercción");
		}
		print(cliente);
	}
	
	@Ignore
	public void probandoSave() {
		ClienteModel cliente = new ClienteModel("3825");
		cliente.set("razon_social", "si aplica el descuento");
		if (cliente.save()) {
			print("Error al intentar guardar");
		}
		//Agregando un nuevo registro de cliente
		ClienteModel c2 = new ClienteModel();
		c2.set("razon_social", "guardado desde el save");
		if (c2.save()) {
			print("Error al intentar guardar");
		}
	}
	
	@Ignore
	public void pedidotest() {
		PedidoModel p = new PedidoModel(200);
		System.out.println(p);
		p.load("56");
		p.load("55");
		p.load("57");
		p.load("57");
		print(p);
	}
	
	@Ignore
	public void createdTest() {
		CancelacionModel c = new CancelacionModel();
		c.set(
				"pedido_id", "200",
				"descripcion", "chafisima",
				"empleado_id", "1",
				"estado", "--"
		);
		if (!c.save()) {
			print("Ocurrio un error");
		}
	}
	
	@Test
	public void paginacionTest() {
		System.out.println("============prueba pagination==========");
		PedidoModel p = new PedidoModel();
		p.limit(10);
		Vector<String> ids = p.pagine();
		for(int i = 0; i< ids.size(); i++) {
			p.load(ids.get(i));
			p.print();
		}
	}
	
}

class DiaModel extends Model {
	/**
	 * Sintaxis valores por defecto
	 */
	final static String defaultSucursal = "matriz";
	final static String defaultFecha    =  "2015-02-04";
	/**
	 * Sobreencimando el metodo load del padre
	 **/
	public Boolean load() {
		String sql = "SELECT *"
				+ " FROM dias"
				+ " WHERE fecha='" + this.get("fecha") + "' AND sucursal = '"+ this.get("sucursal") +"'"
				+ " LIMIT 1;";
		return this.loadByQuery(sql);
	}
}

class CancelacionModel extends Model {
	public static String primary_key = "pedido_id";
	
	public Boolean afterCreate() {
		System.out.println("afterCreate cambiando al pedido dejandolo cancelado");
		return false;
	}
}
