package com.mundosica.core.ModelTest;

import com.mundosica.core.Model.Model;

public class ClienteModel extends Model{

	public ClienteModel(String idStr) {
		super(idStr);
	}

	public ClienteModel() {
	}

	/**
	 * Soporte para callbacks
	 */
	/**
	 * beforeSave
	 * @params si devuelve true continua la ejecución si devuelve false se detiene. 
	 */
	public Boolean beforeUpdate() {
		if (this.get("razon_social") == null) {
			System.out.println("La razón social no puede ser nula");
			return false;
		}
		if (this.get("cobrador_id") == null) {
			this.set("cobrador_id", "83");
		}
		if (this.get("movil") == null) {
			this.set("movil", "123345");
		}
		if (!this.get("movil").matches("[0-9- ()]+")) {
			this.set("movil", "54321");
		}
		if (this.get("notas") == null) {
			this.set("notas", "--notas vacias--");
		}
		if (this.get("RFC") != null) {
			this.set("RFC", this.get("RFC").toUpperCase());
		}
		return true;
	}

	/**
	 * afterSave
	 * @params si devuelve true continua la ejecución si devuelve false se detiene. 
	 */
	public Boolean afterUpdate() {
		/** codigo a realizar despues de realizar la insercción */
		return true;
	}
}
