package com.mundosica.core.ModelTest;

//import static org.junit.Assert.*;
import org.junit.BeforeClass;
import org.junit.Test;

import com.mundosica.core.MainTest;
import com.mundosica.core.Model.ConfiguracionModel;
import com.mundosica.core.testTest.ConfigApp;

public class ConfiguracionModelTest extends MainTest {
	@BeforeClass
	static public void inicio() {
		new ConfigApp();
	}

	@Test
	public void test() {
		ConfiguracionModel c = new ConfiguracionModel("sucursal");
		c.set("valor", "cruvi_candiani");		
		System.out.println(c);
	}
	
	@Test
	public void customField() {
		ConfiguracionModel c = new ConfiguracionModel("mi-campo-personalizado");
		System.out.println(c);
		c.set("valor", "valor nuevo");
		ConfiguracionModel c2 = new ConfiguracionModel("mi-campo-personalizado");
		System.out.println(c2);
		System.out.println("-------------------------");
		ConfiguracionModel c3 = new ConfiguracionModel("pedidos_id_actual");
		System.out.println("primer valor:"+c3.get("valor"));
		c3.set("nombre","ticket_pie_msg");
		c3.load();
		System.out.println("segudo valor:"+c3.get("valor"));
	}
}
