package com.mundosica.core.ModelTest;
import com.mundosica.core.Model.Model;

/**
 * La clase del pedido Model.
 * 
 */
public class PedidoModel extends Model {
	
    //Recursive
    public int recursive = 1;


    /**
     * Instantiates a new pedido model.
     */
    public PedidoModel() {
    }

    /**
     * Instantiates a new pedido model.
     *
     * @param id the id
     */
     public PedidoModel(int id) {
        this.set("id",id+"");
        this.load();
    }
     
     public boolean load(int id) {
    	 this.set("id",id+"");
         return this.load();
     }

     public boolean cancelado() {
    	 return this.get("cancelado").equals("1")? true : false;
     }



}//End Class
