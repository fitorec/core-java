package com.mundosica.core.testTest;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import org.junit.Test;
import com.mundosica.core.MainTest;
import com.mundosica.core.Utils.Config;
import com.mundosica.core.Utils.Fecha;

/**
 *
 * Se encarga de configurar los parametros de la base de datos
 */

public class ConfigApp extends MainTest {


	@Test
	public void customField() {
		Config.customField("NUEVO_CAMPO_PERSONALIZADO", "valor");
		Config.customField("gitFileConfig", Config.rootPath() + Config.DS + ".git" + Config.DS + "config");
		String sucursal = "matriz";
		Config.customField("sucursal", sucursal);
		Config.customField("sucursalSlug", Config.customField("sucursal").trim().toLowerCase().replace(' ', '_'));
		Config.customField("pathSucursal", Config.dataPath() + Config.customField("sucursalSlug") + Config.DS);
		Config.customField("cakePhp", "php " + Config.rootPath()+Config.DS+"web"+Config.DS+"app"+Config.DS+"Console"+Config.DS+"cake.php ");
		Config.customField("fecha", Fecha.fechaActual());
	}
	public void escribirEnArchivo(String string) {
        try {
            FileOutputStream os = new FileOutputStream(Config.dataPath()+"ticket.txt");
            PrintStream ps = new PrintStream(os);
            ps.print(string);
            ps.close();
            try {
            	String cmd = "lpr";
            	if (Config.os().equals("Win")) {
            		cmd = "cat";
            		System.out.println(cmd + " " + Config.dataPath() + "ticket.txt");
            	}
                Runtime.getRuntime().exec(cmd + " " + Config.dataPath() + "ticket.txt");
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
