package com.mundosica.core.print;

import static org.junit.Assert.*;

import java.util.Vector;

import org.junit.Test;

public class Print {

	@Test
	public void test() {
		PrintManager p = new PrintManager();
		Text t = new Text("123456789012");
		t.setBounds(20, 20, 100, 20);
		p.addElement(t);
		
		//======================
		Vector<String> texto = new Vector<String>();
		texto.add("hola que pedo");
		texto.add("hola que pedossssss1");
		texto.add("hola que pedosssssssss2");
		texto.add("hola que pedo3");
		texto.add("hola que pedo4");
		texto.add("hola que pedosssssssssssss5");
		texto.add("hola que pedo6");
		TextArea a = new TextArea(texto);
		a.setBounds(10, 40, 300, 110);
		//a.posicionTexto(Drawable.RIGHT);
		//a.fontSize(10);
		p.addElement(a);
		//=========================
		Vector<String> texto3 = new Vector<String>();
		texto3.add("hola que pedo");
		texto3.add("hola que pedossssss1");
		texto3.add("hola que pedosssssssss2");
		texto3.add("hola que pedo3");
		texto3.add("hola que pedo4");
		texto3.add("hola que pedosssssssssssss5");
		texto3.add("hola que pedo6");
		TextArea b = new TextArea(texto3);
		b.setBounds(500, 40, 300, 110);
		//a.posicionTexto(Drawable.RIGHT);
		//a.fontSize(10);
		p.addElement(b);
		//=========================
		Vector<String[]> texto2 = new Vector<String[]>();
		texto2.add(new String[]{"dat"});
		///*
		texto2.add(new String[]{"dato"});
		texto2.add(new String[]{"dathy"});
		texto2.add(new String[]{"dat"});
		////*/
		Table tab = new Table(texto2);
		tab.setBounds(0, 300, 100, 100);
		tab.widthCeldas(new int[]{50});
		tab.posicionTexto(Drawable.CENTER);
		tab.cabecera(new String[]{"ca"});
		tab.fontSize(15);
		tab.heightCelda(20);
		p.addElement(tab);
		//=========================
		Vector<String[]> texto5 = new Vector<String[]>();
		texto5.add(new String[]{"dat"});
		///*
		texto5.add(new String[]{"dato"});
		texto5.add(new String[]{"dathy"});
		texto5.add(new String[]{"dat"});
		////*/
		Table tab2 = new Table(texto5);
		tab2.setBounds(400, 300, 200, 100);
		tab2.widthCeldas(new int[]{50});
		//tab2.posicionTexto(Drawable.CENTER);
		tab2.fontSize(15);
		//tab2.heightCelda(20);
		p.addElement(tab2);
		//=========================
		p.impresora("Generic-CUPS-PDF-Printer");
		p.imprimir();
	}

}
