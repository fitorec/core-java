package com.mundosica.core.ViewTest;

import static org.junit.Assert.*;

import java.util.Vector;

import org.junit.Ignore;
import org.junit.Test;
import org.eclipse.swt.SWT;

//AGREGANDO DEPENDENCIA
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Shell;

import com.mundosica.core.View.Helper;
import com.mundosica.core.Utils.Fecha;

/**
 * @author fitorec
 *
 */
@Ignore public class HelperTest {

	@Test
	public void testString() {
		String msg = " Se adjunta SetTest.java, una batería de pruebas para comprobar que funciona correctamente la clase java.util.HashSet como implementación de la clase java.util.Set. ";
		Vector<String> renglones = Helper.cortarEnRenglones(msg, 40);
		for (int i=0; i<renglones.size(); i++) {
			String linea = renglones.get(i);
			/*assertTrue(
				"Error la linea '"+linea+"' mide mas de 40 caracteres",
				linea.length() >40
			);*/
			//System.out.println("[" + linea + "]" + linea.length());
			System.out.println(Helper.padCenter(linea, 40));
		}
	}
    /**
     * Probando la función es númerica
     */
	@Test
	public void test() {
		assertEquals("es númerico", true, Helper.isNumeric("5"));
		assertEquals("es númerico", true, Helper.isNumeric("50.00"));
		assertEquals("es númerico", true, Helper.isNumeric("$ 20.00"));
		assertEquals("es númerico", true, Helper.isNumeric(" 5.321"));
		assertEquals("es númerico", true, Helper.isNumeric("1,000.50"));
		assertEquals("No es númerico", false, Helper.isNumeric("czv.,xcvsa"));
		/**
		 * Prueba dinamica
		 */
		for (int num = 0; num < 1000; num++) {
			for (int spaces = 1; spaces < 6; spaces++) {
				String numStr = String.format("%0"+spaces+"d", num);
				assertEquals(numStr + ": es númerico", true, Helper.isNumeric(numStr));
				assertEquals("- " + numStr + ": es númerico", true, Helper.isNumeric("- " + numStr));
			}
		}
		/**
		 * PROBANDO FUNCIOALIDAD DE CONVERSIÓN
		 */
		assertEquals("es númerico", (Integer)(-5), Helper.str2Int("-5"));
		assertEquals("es númerico", (Integer)50, Helper.str2Int("50.00"));
		assertEquals("es númerico", (Integer)20, Helper.str2Int("$ 20.90"));
		assertEquals("es númerico", (Integer)5, Helper.str2Int(" 5.321"));
		assertEquals("es númerico", (Integer)1000, Helper.str2Int("1,000.50"));
		assertEquals("es númerico", (Integer)0, Helper.str2Int("0.50"));
		assertEquals("es númerico", (Integer)0, Helper.str2Int(".50"));
		assertEquals("es númerico", (Integer)0, Helper.str2Int("-.10"));
		assertEquals("es númerico", (Integer)Integer.MAX_VALUE, Helper.str2Int(""+Integer.MAX_VALUE));
		assertEquals("es númerico", (Integer)Integer.MIN_VALUE, Helper.str2Int(""+Integer.MIN_VALUE));
		assertEquals("No es númerico", null, Helper.str2Int("czv.,xcvsa"));
		/**
		 * PRUEBA DINAMICA
		 */
		for (Integer num = 0; num < 1000; num++) {
			for (int spaces = 1; spaces < 6; spaces++) {
				String numStr = String.format("%0"+spaces+"d", num);
				Integer numNeg = num * -1;
				assertEquals(numStr + ": es númerico", num, Helper.str2Int(numStr));
				assertEquals(numNeg + ": es númerico", numNeg, Helper.str2Int("- " + numStr));
			}
		}
	}

	/**
	 * PROBANDO LA FUNCIÓN TOSTRING PARA FECHAS
	 */
	@Test
	public void testDatetime2String() {
		Group grupoElement = new Group(new Shell(), SWT.NONE);
		DateTime fecha = Fecha.dateTimeMesAnterior(grupoElement);
		fecha.setDate(2006, 05, 14); //14 DE JUNIO DEL AÑO 2006
		assertEquals("Es una fecha", "2006-06-14", Helper.strFormat(fecha));

		//31 DE DICIEMBRE DEL AÑO 20014
		fecha.setDate(2014, 11, 31);
		assertEquals("Es una fecha", "2014-12-31", Helper.strFormat(fecha));
		
		//3 DE OCTUBRE DEL AÑO 1985
		fecha.setDate(1985, 9, 3);
		assertEquals("Es una fecha", "1985-10-03", Helper.strFormat(fecha));

		//CASO NULL, DEVUELVE VACIO
		assertEquals("Esto no es un objeto valido", "", Helper.strFormat(null));
	}
	
	@Test
	public void pruebaDiaSemana() {
		String fecha = "2014-"; //+ 00:00:00";
		for (int i = 1; i < 13; i++) {
			String fechaAux = fecha + ((i<10)? "0"+i: i) + "-01 00:00:00";
			String diaStr = Helper.strDateTimeDiaSemana(fechaAux);
			String strFormat = Helper.strDateTimeFormatSmall(fechaAux);
			System.out.println(fechaAux + " - " + strFormat + " - "+ diaStr);
		}
	}
}
