package com.mundosica.core.ViewTest;

import java.io.File;

import org.junit.Test;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Shell;

import com.mundosica.core.swtf.FontAwosome;
import com.mundosica.core.swtf.SWTF;

public class IconsTest {

	@Test
	public void test() {
		//Icon.font();
		new VentanaDemo(new Shell());
	    ///
    }
}

class VentanaDemo extends Dialog {

    /** The result. */
    protected Object result;
    /** The shell. */
    protected Shell shell;
    //private Text fieldFecha;

    /**
     * Create the dialog.
     *
     * @param parent Shell ventana llamada
     */
    public VentanaDemo(Shell parent) {
        super(parent, SWT.PRIMARY_MODAL);
        this.open();
    }

    /**
     * Open the dialog.
     * @return the result
     */
    public Object open() {
        createContents();
        shell.open();
        shell.layout();
        Display display = getParent().getDisplay();
        while (!shell.isDisposed()) {
            if (!display.readAndDispatch()) {
                display.sleep();
            }
        }
        return result;
    }

    /**
     * Create contents of the dialog.
     */
    private void createContents() {
        shell = new Shell(getParent(),  SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
        shell.setText("Prueba de fuentes TTF");
        shell.setSize(1150, 700);
        Group grupo = new Group(shell, SWT.NONE);
		grupo.setText("Icono");
		grupo.setToolTipText("");
		grupo.setBounds(10, 0, 1130, 680);
		int posx = 0, posy = 0;
        Font font = FontAwosome.font(13);
        for (int i = 0; i < FontAwosome.fontawesomeCodes.length - 1; i += 2) {
        	Button btnDemo = new Button(grupo, SWT.NONE);
            btnDemo.setBounds(posx, posy, 33, 33);
            btnDemo.setFont(font);
            btnDemo.setText(FontAwosome.get(FontAwosome.fontawesomeCodes[i])  + "");
            btnDemo.setToolTipText(FontAwosome.fontawesomeCodes[i]);
            btnDemo.addSelectionListener(new SelectionAdapter() {
                @Override
                public void widgetSelected(SelectionEvent SelectEvent) {
                    shell.close();
                }
            });
            btnDemo.setFocus();
            posx += 35;
            if(posx > 1100) {
            	posx = 0;
            	posy += 35;
            }
        }
    }
}

