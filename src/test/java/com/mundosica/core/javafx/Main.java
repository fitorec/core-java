package com.mundosica.core.javafx;

import com.mundosica.core.javafx.dialog.DialogFx;
import com.mundosica.core.javafx.dialog.PopUpDialogFx;

import javafx.application.Application;
import javafx.stage.Stage;


public class Main extends Application {
	
	//generadores de css css3generator css3gen
	@Override
	public void start(Stage primaryStage) {
		try {
			EmprefacilCss plantilla = new EmprefacilCss();
			plantilla.path(getClass().getResource("application.css"));
			/*
			LoginFx v = new LoginFx(plantilla);
			v.addTryAuth(new TryAuth() {

				@Override
				public boolean hook(String username, String password) {
					System.out.println(username + ":" + password);
					return true;
				}
				
			});
			///////*/
			//Ventana v = new Ventana(plantilla);
			DialogFx.error(plantilla, "hola");
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
