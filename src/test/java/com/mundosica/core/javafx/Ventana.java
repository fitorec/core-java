package com.mundosica.core.javafx;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableView;
import javafx.scene.layout.Pane;

public class Ventana extends ClaseVentana {

	public Ventana(FXPlantillaCss plantilla) {
		super(plantilla);
		// TODO Auto-generated constructor stub
	}
	
	public void createContest() {
		Pane botonera = (Pane)rootElements.pane(100, 100, 10, 10, 10, 10);
		botonera.setStyle("-fx-background-color: #ffffff;");
		FXElementsEmprefacil elementsBototnera = new FXElementsEmprefacil(botonera,plantilla);
		elementsBototnera.button_blue(10, 10, 20, 0, 0, 0, "boton").setOnAction(new EventHandler<ActionEvent>() {
		    @Override public void handle(ActionEvent e) {
		        //label.setText("Accepted");
		    }
		});
		TableView<String[]> table = (TableView<String[]>)elementsBototnera.tableView(10, 50, 10, 10, 10, 10);
		String columns[] = {"col uno", "col dos","columna grande"};
		int minWidth [] = {100,200,200};
		elementsBototnera.addColums(table, columns, minWidth);
		String a[] = {"uno","dos","tres"};
		table.getItems().add(a);
		ComboBox<String> c = (ComboBox<String>)elementsBototnera.comboBox(10, 10, 10, 10, 10, 10);
		c.getItems().addAll("opcion1","opcion2","opcion3");
		//pane.getChildren().add();
	}

}
