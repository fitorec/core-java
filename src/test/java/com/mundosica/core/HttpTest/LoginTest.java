package com.mundosica.core.HttpTest;

import static org.junit.Assert.*;

import org.junit.AfterClass;
import org.junit.Test;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mundosica.core.Http.Login;

public class LoginTest {

	@AfterClass
	public static void exit() {
		Login.exit();
	}

	@Test
	public void test() {
		String url = "http://carnescruvi.com/punto_venta/users/login";
		// Prueba seteando un valor
		Login.set(
			"username", "soporte",
			"password", "soporte",
			"url", url
		);
		Login.startSession();
		/*assertEquals("Error en la funcion mediaPath: ", "AA", Login.username());
		// Prueba seteando dos valores
		assertEquals("Error en la funcion mediaPath: ", new Boolean(true), result);
		result = Login.set(
			"username", "BB",
			"url", "http://gogle.com.mx"
		);
		assertEquals("Error en la funcion mediaPath: ", "BB", Login.username());
		assertEquals("Error en la funcion mediaPath: ", "http://gogle.com.mx", Login.url());
		// Prueba seteando los tres valores
		assertEquals("Error en la funcion mediaPath: ", new Boolean(true), result);
		result = Login.set(
			"username", "soporte",
			"password", "123456",
			"url", url
		);
		assertEquals("Error en la funcion mediaPath: ", new Boolean(true), result);
		//Probando que se seteen bien los valores
		assertEquals("Error en la funcion mediaPath: ", "soporte", Login.username());
		assertEquals("Error en la funcion mediaPath: ", "123456", Login.password());
		assertEquals("Error en la funcion mediaPath: ", url, Login.url());

		//Probando error
		result = Login.set(
			"username", "error",
			"password", "xxx",
			"url"
		);
		assertEquals("Error en la funcion mediaPath: ", new Boolean(false), result);
		//Probando que se seteen bien los valores
		assertEquals("Error en la funcion mediaPath: ", "soporte", Login.username());
		assertEquals("Error en la funcion mediaPath: ", "123456", Login.password());
		assertEquals("Error en la funcion mediaPath: ", url, Login.url());*/
	}

	/*@Test
	public void probandoStarSession() {
		Login.set(
			"username", "soporte",
			"password", "654321",
			"url", "http://emprefacil.net/punto_venta/users/login"
		);
		Boolean result = Login.startSession();
		///
		HttpResponse<JsonNode> response = Login.getJSON("http://emprefacil.net/punto_venta/users/hola");
		System.out.println(response.getBody().toString());
	}*/

	@Test
	public void downloadTest() {
		String url = "https://pbs.twimg.com/media/B3k7eBEIYAAQjrE.jpg";
		String LocalPath = System.getProperty("user.home") + "prueba_download.jpg";
		Login.download(url, LocalPath);
	}

	/*@Test
	public void uploadTest() {
		Login.set(
				"username", "soporte",
				"password", "654321",
				"url", "http://emprefacil.net/punto_venta/users/login"
			);
		Boolean result = Login.startSession();
		//
		String fecha= "2013-02-11 10:42:31";
		//HttpResponse<JsonNode> response = Login.getJSON("http://emprefacil.net/punto_venta/productos/desde/"+ fecha);
		// print(response);
	}*/
}
