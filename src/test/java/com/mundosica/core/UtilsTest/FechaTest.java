package com.mundosica.core.UtilsTest;


import static org.junit.Assert.*;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Shell;
import org.junit.Ignore;
import org.junit.Test;

import com.mundosica.core.Utils.Fecha;

@Ignore
public class FechaTest {

	@Test
	public void test() {
		String mes = Fecha.nombreMes(Fecha.mesAnterior(1));//febrero
		assertEquals("Error: no es el mes correcto: ", "enero", mes);
		
		mes = Fecha.nombreMes(Fecha.mesAnterior(0));//enero
		assertEquals("Error: no es el mes correcto: ", "diciembre", mes);
		
		/////
		Group grpControles = new Group(new Shell(), SWT.NONE);
        grpControles.setText("Información por fecha");
        DateTime dateActual = new DateTime(grpControles, SWT.BORDER | SWT.DATE | SWT.DROP_DOWN);
        DateTime dateAnterior = Fecha.dateTimeMesAnterior(grpControles);
        String fechaActual = dateActual.getDay() + "-" +dateActual.getMonth() + "-" + dateActual.getYear();
        String fechaEsperada = "30-10-2015";
        String fechaDevuelta = dateAnterior.getDay() + "-" +dateAnterior.getMonth() + "-" + dateAnterior.getYear();
        
        System.out.println("---------fecha-mes anterior-----");
        System.out.println("fecha actual: " + fechaActual);
        System.out.println("fecha esperada: " + fechaEsperada);
        System.out.println("fecha devuelta: "+ fechaDevuelta);
        assertEquals("Error: no es la fecha: ", fechaEsperada, fechaDevuelta);
        
        System.out.println("-------fecha-dia siguiente------");
        DateTime dateDiaPosterior = Fecha.dateTimeDiaSiguiente(grpControles);
        fechaActual = dateActual.getDay() + "-" +dateActual.getMonth() + "-" + dateActual.getYear();
        fechaEsperada = "1-0-2016";
        fechaDevuelta = dateDiaPosterior.getDay() + "-" +dateDiaPosterior.getMonth() + "-" + dateDiaPosterior.getYear();
        System.out.println("fecha actual: " + fechaActual);
        System.out.println("fecha esperada: " + fechaEsperada);
        System.out.println("fecha devuelta: "+ fechaDevuelta);
        assertEquals("Error: no es la fecha: ", fechaEsperada, fechaDevuelta);
        
        System.out.println("---------fecha-dia anterior-----");
        fechaActual = "2015-01-01";
        fechaEsperada = "2014-12-31";
        fechaDevuelta = Fecha.diaPrevio(fechaActual);
        System.out.println("fecha actual: " + fechaActual);
        System.out.println("fecha esperada: " + fechaEsperada);
        System.out.println("fecha devuelta: "+ fechaDevuelta);
        assertEquals("Error: no es la fecha: ", fechaEsperada, fechaDevuelta);
        
        System.out.println("---------fecha-dia siguiente----");
        fechaActual = "2015-12-31";
        fechaEsperada = "2016-01-01";
        fechaDevuelta = Fecha.diaSiguiente(fechaActual);
        System.out.println("fecha actual: " + fechaActual);
        System.out.println("fecha esperada: " + fechaEsperada);
        System.out.println("fecha devuelta: "+ fechaDevuelta);
        assertEquals("Error: no es la fecha: ", fechaEsperada, fechaDevuelta);
	}

}
