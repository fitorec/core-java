package com.mundosica.core.UtilsTest;


import static org.junit.Assert.*;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import com.mundosica.core.Model.DataBase;
import com.mundosica.core.Security.CakePHP;
import com.mundosica.core.Utils.Config;

public class ConfigTest {

	@Before
	public void run() {
		DataBase.config("bd"   , "carniceria");
		DataBase.config("user" , "cruvi_user");
		DataBase.config("password" , "cruvi_pass");
		// Agregamos el security salt
		CakePHP.securitySalt("ADFVHGFHJK NBJYreEERWRTYTRU(I))KLfgzxcv#\"$\"%##&z<.,1287685");
		//Configuramos los directorios para ambos sistemas operativos
		Config.rootPath("linux", "/mi_app");
		Config.rootPath("windows", "C:\\mi_app");
	}

	@AfterClass
	static public void after() {
		System.out.println("********************");
		Config.imprimirConfig();
	}

	@Test
	public void testRootPath() {
		String rootPath = Config.rootPath();
		if (Config.os().equals("windows")) {
			assertEquals("Error en la funcion rootPath: ", "C:\\mi_app\\", rootPath);
		} else {
			assertEquals("Error en la funcion rootPath: ", "/mi_app/", rootPath);
		}
		System.out.println("Prueba testRootPath exitosa");
	}

	@Test
	public void customFieldTest() {
		///// Test
		//Config.imprimirConfig();
		Config.customField("mi-campo-personalizado", "hola mundo 1");
		Config.customField("mi-campo-personalizado", "hola mundo 2");
		Config.customField("mi-campo-personalizado", "hola mundo 3");
		Config.customField("mi-campo-personalizado", "hola mundo 4");
		///
		String customField = Config.customField("mi-campo-personalizado");
		assertEquals("Error en la funcion Config.customField: ", "hola mundo 4", customField);
		////
		for (int i=0; i < 100; i++) {
			Config.customField("variable"+i, "valor variable "+i);
		}
		for (int i=99; i >= 0; i--) {
			String cf = Config.customField("variable"+i);
			assertEquals("Error en la funcion Config.customField: ", cf, "valor variable "+i);
		}
		for (int i=-1000; i < 1000; i++) {
			Config.customField("variable"+i);
			Config.customFieldDelete("variable"+i);
		}
		String cf = Config.customField("variable"+2);
		for (int i=-100; i < 1000; i++) {
			cf = Config.customField("variable"+i);
			assertEquals("Error en la funcion Config.customField: "+i, null, cf);
		}
		System.out.println("Prueba customFieldTest exitosa");
	}

	@Test
	public void ipValidaTest() {
		String ipsValidas[] = {
			" 127.0.0.1"," 192.168.0.9    ", "200.0.0.1"
		};
		String ipsInValidas[] = {
			"0.0.0.0", " 192.168.0.0    ", "a ver si eres tan chingón"
		};
		for (int i=0; i < ipsValidas.length; i++) {
			assertEquals( "error en las ip validas", Config.validIP4(ipsValidas[i]), true);

		}
		for (int i=0; i < ipsInValidas.length; i++) {
			assertEquals( "error en las ip invalidas", Config.validIP4(ipsInValidas[i]), false);
		}
		System.out.println("Prueba ipValidaTest exitosa");
	}

	@Test
	public void mediaAndDataPath() {
		Config.rootPath("linux", "/otra_app");
		Config.rootPath("windows", "C:\\otra_app");
		String mediaPath = Config.mediaPath();
		String dataPath = Config.dataPath();
		if (Config.os().equals("windows")) {
			assertEquals("Error en la funcion mediaPath: ", "C:\\otra_app\\media\\", mediaPath);
			assertEquals("Error en la funcion dataPath: ", "C:\\otra_app\\data\\", dataPath);
		} else {
			assertEquals("Error en la funcion mediaPath: ", "/otra_app/media/", mediaPath);
			assertEquals("Error en la funcion dataPath: ", "/otra_app/data/", dataPath);
		}
		System.out.println("Prueba mediaAndDataPath exitosa");
	}

	@Test
	public void getOsPathTest() {
		assertEquals( "Las rutas no coinciden", "/as/n/", Config.getOsPath("/as/n"));
		System.out.println("Prueba getOsPathTest exitosa");
		if (Config.os().equals("windows")) {
			String path = Config.getOsPath("/Program Files/mi_app");
			assertEquals("Las rutas no coinciden", "C:\\Program Files\\mi_app\\", path);
		}
	}

	@Test
	public void cakeSet() {
		String baseCake = Config.cakeShell("/bin/mi_app/app/");
		String esperado = "php '/bin/mi_app/app/Console/cake.php' -working '/bin/mi_app/app/' ";
		assertEquals( "error en las ip invalidas", esperado, baseCake);
		baseCake = Config.cakeShell("/bin/mi_app/app/", "/usr/bin/php");
		esperado = "/usr/bin/php '/bin/mi_app/app/Console/cake.php' -working '/bin/mi_app/app/' ";
		assertEquals( "error en las ip invalidas", esperado, baseCake);
	}
	@Test
	public void archivoConfig() {
		///*
		Config.rootPath("windows", "C:\\Colectivo\\");
		Config.rootPath("linux", "/opt/Colectivo/");
		System.out.println("Root path ....\n"+Config.dataPath());
		System.out.println(Config.config("databaseHost"));
		Config.config("databaseHost","192.168.0.14");
		Config.config("que pedo","aaaaa");
		System.out.println(Config.config("que pedo"));
		////*/
		
		
		//System.out.println(Config.leerArchivo("/opt/Colectivo/data/config.conf"));
	}
}
