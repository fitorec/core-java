package com.mundosica.core.UtilsTest;

//import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import com.mundosica.core.Utils.Inflector;

public class InflectorTest {

	@Test
	public void testPlural() {
		//assertEquals("Error enla conversión plural: ", "cliente", Inflector.singular("clientes"));
		///////////////
		inflectorTest("Cancelaciones","Cancelacion");
		inflectorTest("Categorias","Categoria");
		inflectorTest("clientes","cliente");
		inflectorTest("configuraciones","configuracion");
		inflectorTest("Cortes","Corte");
		inflectorTest("Dias","Dia");
		inflectorTest("Empleados","Empleado");
		inflectorTest("Facturas","Factura");
		inflectorTest("Pedidos","Pedido");
		inflectorTest("Precios","Precio");
		inflectorTest("Productos","Producto");
		inflectorTest("roles","role");
		inflectorTest("Sucursales","Sucursal");
		inflectorTest("tipo_de_clientes","tipo_de_cliente");
		// Regla terminación con z
		inflectorTest("lapices","lapiz");
		inflectorTest("Voces","Voz");
		inflectorTest("veces","vez");
		///
	}
	
	private void inflectorTest(String pluralOriginal, String singularOriginal) {
		String pluralObtenido = Inflector.plural(singularOriginal);
		//String singularObtenido = Inflector.singular(pluralOriginal);
		assertEquals("Error:Plural: ", pluralObtenido, pluralOriginal);
		//assertEquals("Error:Singular: ", singularObtenido, singularOriginal);
	}
}
