package com.mundosica.core.UtilsTest;

import java.util.concurrent.TimeUnit;

import org.eclipse.swt.widgets.Shell;
import org.junit.Test;

import com.mundosica.core.MainTest;
import com.mundosica.core.Model.QueryManager;
import com.mundosica.core.Utils.TaskManager;

/**
 * Prueaba para el modelo
 * 
 * @author fitorec
 */
public class TaskManagerTest extends MainTest {


	/**
	 * Probando el load
	 */
	@Test
	public void loadTest() {
		MiManejadorTareas miManejador = new MiManejadorTareas(1);
		miManejador.start();
		TaskManagerTest.sleep(60);
		System.out.println("la prueba finalizo");
		TaskManager.end();
	}

	static void sleep(int mins) {
	    try {
	        TimeUnit.MINUTES.sleep(mins);
	    } catch (InterruptedException e) {
	        e.printStackTrace();
	    }
	}
}

class MiManejadorTareas extends TaskManager {

	public MiManejadorTareas(int interval_minutes) {
		super(new Shell()); 
	}

	/** 
	 * Función a sobre escribir que se ejecuta cada 5 minutos
	 */
	public void every5mins() {
		System.out.println("TaskManager cada 5 mins");
		if (this.haveInternet()) {
			System.out.println("Hay Internet"  + QueryManager.curtime());
		} else {
			System.out.println("No hay Internet" + QueryManager.curtime());
		}
	}
	/** 
	 * Función a sobre escribir que se ejecuta cada 10 minutos
	 */
	public void every10mins() {
		System.out.println("TaskManager cada 10 mins");
		if (this.haveInternet()) {
			System.out.println("Hay Internet" + QueryManager.curtime());
		} else {
			System.out.println("No hay Internet" + QueryManager.curtime());
		}
	}
}
