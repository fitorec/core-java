package com.mundosica.core.javafx;

import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
public class ClaseVentana extends Stage {
	
	public Pane root = new Pane();
	public Scene scene;
	public FXElementsCreator rootElements;
	public FXPlantillaCss plantilla;
	
	public ClaseVentana (FXPlantillaCss plantilla) {
		this.plantilla = plantilla;
		scene = new Scene(root,1050, 750);
		root.setStyle("-fx-background: #87b6b7;");
		rootElements = new FXElementsEmprefacil(root,plantilla);
		scene.getStylesheets().add(plantilla.getPath());
		createContest();
		setScene(scene);
	}
	
	public  ClaseVentana(FXPlantillaCss plantilla, int x, int y) {
		this.plantilla = plantilla;
		scene = new Scene(root,x, y);
		root.setStyle("-fx-background: #87b6b7;");
		rootElements = new FXElementsEmprefacil(root,plantilla);
		scene.getStylesheets().add(plantilla.getPath());
		createContest();
		setScene(scene);
	}
	
	public void createContest() {}
	
	public FXPlantillaCss getPlantilla() {
		return this.plantilla;
	}
}