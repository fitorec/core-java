package com.mundosica.core.javafx;

public class EmprefacilCss implements FXPlantillaCss {
	
	String path;
	public EmprefacilCss () {
		
	}
	
	public void path(String path) {
		this.path = path;
	}
	
	public void absolutePath(String path) {
		this.path = path;
	}
	
	public void path(java.net.URL path) {
		this.path = path.toExternalForm();
	}
	
	public String getPath() {
		return path;
	}

	@Override
	public String get(String id) {
		if(id.equals("boton-id")) {
			return "boton-emprefacil";
		}
		if(id.equals("boton-red-id")) {
			return "boton-red-emprefacil";
		}
		if(id.equals("boton-blue-id")) {
			return "boton-blue-emprefacil";
		}
		if(id.equals("boton-yellow-id")) {
			return "boton-yellow-emprefacil";
		}
		if(id.equals("boton-error-id")) {
			return "boton-error-emprefacil";
		}
		if(id.equals("boton-info-id")) {
			return "boton-info-emprefacil";
		}
		if(id.equals("boton-warning-id")) {
			return "boton-warning-emprefacil";
		}
		if(id.equals("label-id")) {
			return "label-emprefacil";
		}
		if(id.equals("comboBox-id")) {
			return "comboBox-emprefacil";
		}
		if(id.equals("text-field-id")) {
			return "text-field-emprefacil";
		}
		if(id.equals("pane-id")) {
			return "pane-emprefacil";
		}
		if(id.equals("border-pane-id")) {
			return "border-pane-emprefacil";
		}
		if(id.equals("table-view-id")) {
			return "table-view-emprefacil";
		}
		if(id.equals("image-view-id")) {
			return "image-view-emprefacil";
		}

		
		return null;
	}

}
