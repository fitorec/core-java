package com.mundosica.core.javafx;

public interface FXPlantillaCss {
	public String getPath();
	public String get(String id);
}
