package com.mundosica.core.javafx.dialog;

import com.mundosica.core.javafx.FXPlantillaCss;

public class DialogFx {

	public static void error(FXPlantillaCss plantilla, String mns) {
		PopUpDialogFx v = new PopUpDialogFx(plantilla);
		v.error(mns);
		v.showAndWait();
	}
	public static void warning(FXPlantillaCss plantilla, String mns) {
		PopUpDialogFx v = new PopUpDialogFx(plantilla);
		v.warning(mns);
		v.showAndWait();
	}
	public static void info(FXPlantillaCss plantilla, String mns) {
		PopUpDialogFx v = new PopUpDialogFx(plantilla);
		v.info(mns);
		v.showAndWait();
	}
}
