package com.mundosica.core.javafx.dialog;

import com.mundosica.core.javafx.FXElementsCreator;
import com.mundosica.core.javafx.FXElementsEmprefacil;
import com.mundosica.core.javafx.FXPlantillaCss;
import com.mundosica.core.swtf.dialog.TryAuth;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class LoginFx extends Stage {
	
	Pane root = new Pane();
	Scene scene;
	FXElementsCreator rootElements;
	FXPlantillaCss plantilla;
	private Button aceptar;
	private TryAuth tryAuth;
	private TextField pass;
	private TextField user;
	private Label error;
	private boolean allow = false;

	public LoginFx (FXPlantillaCss plantilla) {
		this.plantilla = plantilla;
		scene = new Scene(root,500,500);
		root.setStyle("-fx-background-color: linear-gradient(#351D00, #000000);");
		rootElements = new FXElementsEmprefacil(root,plantilla);
		scene.getStylesheets().add(plantilla.getPath());
		this.initModality(Modality.APPLICATION_MODAL);
		createContest();
		setScene(scene);
	}
	public void addTryAuth(TryAuth ta) {
    	this.tryAuth = ta;
    }
	
	public void createContest() {
		int padX = 5;
		int padY = 5;
		int padH = 5;
		int padW = 5;
		//String imagePath = Config.mediaPath() + "logo-emprefacil.png";
		String imagePath ="/opt/punto_venta/media/logo-emprefacil.png";
		this.rootElements.imageView(10, 10, 480,138, imagePath);
		Pane login = (Pane)rootElements.pane(70, 200, 20, 20, 20, 20);
		login.setStyle("-fx-background-color: #F1F1F1");
		FXElementsEmprefacil elementsLogin = new FXElementsEmprefacil(login,plantilla);
		Label titulo = elementsLogin.label(10, 10,padX, padY, padW, padH, 
				"Ingresa tu Informacion");
		titulo.setStyle(
				"-fx-font-size:23px;"+
				"-fx-font-family: 'Arial Black';"+
				"-fx-fill: #818181;"+
				"-fx-effect: innershadow( three-pass-box , rgba(0,0,0,0.7) , 6, 0.0 , 0 , 2 );");
		error = elementsLogin.label(30, 60, padX, padY, padW, padH,"");
		error.setStyle(
				"-fx-text-fill: #CDA639;"
				+ "-fx-background-color: #FCE4A4;"  
				+ "-fx-font-size: 18px;");
		error.setVisible(false);
		elementsLogin.label(10, 110, padX, padY, padW, padH, "Usuario");
		user = elementsLogin.textField(150,110, padX, padY, padW, padH);
		elementsLogin.label(10, 160, padX, padY, padW, padH, "Contraseña");
		pass = elementsLogin.textField(150, 160, padX, padY, padW, padH);
		this.aceptar = elementsLogin.button_info(220, 210, padX, 20, padW, 20, "Acceder");
		this.aceptar.setOnAction(new EventHandler<ActionEvent>() {
		    @Override public void handle(ActionEvent e) {
		    	aceptar();
		    }
		});
	}
	
	private void aceptar() {
    	String username = this.user.getText();
    	String password = this.pass.getText();
    	this.allow = false;
    	if (this.tryAuth != null) {
    		allow = this.tryAuth.hook(username, password);
    	}
    	if (allow) {
    		close();
    		return;
    	}
  		// Revisar este mensaje de error
    	this.error.setText("Tus datos son Incorrectos");
   		this.error.setVisible(true);
    }
}
