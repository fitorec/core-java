package com.mundosica.core.javafx.dialog;

import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import com.mundosica.core.javafx.FXElementsCreator;
import com.mundosica.core.javafx.FXElementsEmprefacil;
import com.mundosica.core.javafx.FXPlantillaCss;


public class PopUpDialogFx extends Stage {
	
	String mensage;
	Pane root = new Pane();
	Scene scene;
	FXElementsCreator rootElements;
	FXPlantillaCss plantilla;

	public PopUpDialogFx(FXPlantillaCss plantilla) {
		this.plantilla = plantilla;
	}
	
	public void error (String msg) {
		createContest();
		root.setStyle("-fx-background: #FA2C2C;");
		rootElements.label(10, 10, 5, 5, 5, 5, msg);
		((Button)rootElements.button_error(190, 150, 5, 20, 5, 20, "Aceptar")).setOnAction(new EventHandler<ActionEvent>() {
		    @Override public void handle(ActionEvent e) {
		    	close();
		    }
		});
	}
	
	public void info (String msg) {
		createContest();
		root.setStyle("-fx-background: #57BCFF;");
		rootElements.label(10, 10, 5, 5, 5, 5, msg);
		((Button)rootElements.button_info(190, 150, 5, 20, 5, 20, "Aceptar")).setOnAction(new EventHandler<ActionEvent>() {
		    @Override public void handle(ActionEvent e) {
		    	close();
		    }
		});
	}
	
	public void warning (String msg) {
		createContest();
		root.setStyle("-fx-background: #FAD12C;");
		rootElements.label(10, 10, 5, 5, 5, 5, msg);
		((Button)rootElements.button_warning(190, 150, 5, 20, 5, 20, "Aceptar")).setOnAction(new EventHandler<ActionEvent>() {
		    @Override public void handle(ActionEvent e) {
		    	close();
		    }
		});
	}
	
	public void createContest() {
		scene = new Scene(root,300, 200);
		rootElements = new FXElementsEmprefacil(root,plantilla);
		scene.getStylesheets().add(plantilla.getPath());
		this.initModality(Modality.APPLICATION_MODAL);
		//.initModality(Modality.APPLICATION_MODAL);
		setScene(scene);
		
	}

}
