package com.mundosica.core.javafx;

import java.io.File;

import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.util.Callback;

public class FXElementsEmprefacil extends FXElementsCreator {
	
	public FXElementsEmprefacil(Pane root, FXPlantillaCss plantilla) {
		super(root,plantilla);
	}
	
	@Override
	public Button button_blue(int x,int y, int padX,int padY,int padW,int padH ,String texto) {
		Button boton = this.button(x,y,padX,padY,padW,padH,texto);
		boton.setId(plantilla.get("boton-blue-id"));
		return boton;
	}
	
	@Override
	public Button button_red(int x,int y, int padX,int padY,int padW,int padH ,String texto) {
		Button boton = this.button(x,y,padX,padY,padW,padH,texto);
		boton.setId(plantilla.get("boton-red-id"));
		return boton;
	}
	
	@Override
	public Button button_yellow(int x,int y, int padX,int padY,int padW,int padH ,String texto) {
		Button boton = this.button(x,y,padX,padY,padW,padH,texto);
		boton.setId(plantilla.get("boton-yellow-id"));
		return boton;
	}
	
	@Override
	public Button button_info(int x,int y, int padX,int padY,int padW,int padH ,String texto) {
		Button boton = this.button(x,y,padX,padY,padW,padH,texto);
		boton.setId(plantilla.get("boton-info-id"));
		return boton;
	}
	
	@Override
	public Button button_error(int x,int y, int padX,int padY,int padW,int padH ,String texto) {
		Button boton = this.button(x,y,padX,padY,padW,padH,texto);
		boton.setId(plantilla.get("boton-error-id"));
		return boton;
	}
	
	@Override
	public Button button_warning(int x,int y, int padX,int padY,int padW,int padH ,String texto) {
		Button boton = this.button(x,y,padX,padY,padW,padH,texto);
		boton.setId(plantilla.get("boton-warning-id"));
		return boton;
	}
	
	@Override
	public Button button(int x,int y, int padX,int padY,int padW,int padH ,String texto) {
		Button boton = new Button(texto);
		boton.setId(plantilla.get("boton-id"));
		nodo(boton, x,y,padX,padY,padW,padH);
		return boton;
	}
	
	@Override
	public Label label (int x,int y, int padX,int padY,int padW,int padH ,String texto) {
		Label label = new Label(texto);
		label.setId(plantilla.get("label-id"));
		nodo(label, x,y,padX,padY,padW,padH);
		return label;
	}

	@Override
	public Text text(int x,int y, String texto) {
		Text text = new Text(texto);
		text.setId(plantilla.get("text-id"));
		text.setLayoutX(x);
		text.setLayoutY(y);
		this.root.getChildren().add(text);
		return text;
	}
	
	@Override
	public TextField textField(int x,int y, int padX,int padY,int padW,int padH) {
		TextField textField = new TextField();
		textField.setId(plantilla.get("text-field-id"));
		nodo(textField, x,y,padX,padY,padW,padH);
		return textField;
	}

	@Override
	public ComboBox<String> comboBox (int x,int y, int padX,int padY,int padW,int padH) {
		ComboBox<String> comboBox = new ComboBox<String> ();
		comboBox.setId(plantilla.get("comboBox-id"));
		nodo(comboBox, x,y,padX,padY,padW,padH);
		return comboBox;
	}
	
	@Override
	public Pane borderPane(int x,int y, int padX,int padY,int padW,int padH) {
		Pane pane = new Pane();
		pane.setId(plantilla.get("border-pane-id"));
		nodo(pane, x,y,padX,padY,padW,padH);
		return pane;
	}
	
	@Override
	public StackPane stackPane(int x,int y, int padX,int padY,int padW,int padH) {
		StackPane pane = new StackPane();
		pane.setId(plantilla.get("pane-id"));
		nodo(pane, x,y,padX,padY,padW,padH);
		return pane;
	}
	
	@Override
	public Pane pane(int x,int y, int padX,int padY,int padW,int padH) {
		Pane pane = new Pane();
		pane.setId(plantilla.get("pane-id"));
		nodo(pane, x,y,padX,padY,padW,padH);
		return pane;
	}
	
	@Override
	public TableView<String[]> tableView(int x,int y, int padX,int padY,int padW,int padH) {
		TableView<String[]> table = new TableView<String[]>();
		table.setId(plantilla.get("table-view-id"));
		nodo(table, x,y,padX,padY,padW,padH);
		return table;
	}
	
	@Override
	public void addColums(TableView<String[]> table,String columns[],int minWidth[]) {
		if(columns.length != minWidth.length) {
			throw new IllegalArgumentException("The number of arguments do not instead.");
		}
		for(int i = 0; i < columns.length; i++) {
			 TableColumn <String[],String>col = new TableColumn<String[],String>(columns[i]);
			 col.setMinWidth(minWidth[i]);
			 col.setMaxWidth(minWidth[i] + 5);
			 final int  index = i;
			 col.setCellValueFactory(new Callback<CellDataFeatures<String[], String>, ObservableValue<String>>() {

				@Override
				public ObservableValue<String> call(CellDataFeatures<String[], String> arg0) {
					return  new ReadOnlyObjectWrapper(arg0.getValue()[index]);
				}
			 });
			 table.getColumns().add(col);
		}
	}
	
	@Override
	public ImageView imageView(int x,int y, int width,int height,String path) {
		ImageView iv = new ImageView();
		iv.setId(plantilla.get("image-view-id"));
		iv.setLayoutX(x);
		iv.setLayoutY(y);
		iv.setFitHeight(height);
		iv.setFitWidth(width);
		this.root.getChildren().add(iv);
		if(this.existeArcivo(path)) {
			System.out.println(path);
			Image i = new Image(new File(path).toURI().toString());
			iv.setImage(i);
		} else {
			System.out.println("no existe la ruta: " + path);
		}
		return iv;
	}
	
	public Node nodo(Node nodo, int x,int y, int padX,int padY,int padW,int padH) {
		nodo.setLayoutX(x);
		nodo.setLayoutY(y);
		((Region) nodo).setPadding(new Insets(padX, padY,padW,padH));
		this.root.getChildren().add(nodo);
		return nodo;
	}
}
