package com.mundosica.core.javafx;

import java.io.File;

import javafx.scene.Node;
import javafx.scene.control.TableView;
import javafx.scene.layout.Pane;

public abstract class FXElementsCreator {
	
	Pane root;
	FXPlantillaCss plantilla;
	
	public FXElementsCreator (Pane root, FXPlantillaCss plantilla) {
		this.root = root;
		this.plantilla = plantilla;
	}

	public abstract Node button(int x,int y, int padX,int padY,int padW,int padH ,String texto);
	
	public abstract Node button_blue(int x,int y, int padX,int padY,int padW,int padH ,String texto);
	
	public abstract Node button_red(int x,int y, int padX,int padY,int padW,int padH ,String texto);
	
	public abstract Node button_yellow(int x,int y, int padX,int padY,int padW,int padH ,String texto);
	
	public abstract Node button_info(int x,int y, int padX,int padY,int padW,int padH ,String texto);
	
	public abstract Node button_error(int x,int y, int padX,int padY,int padW,int padH ,String texto);
	
	public abstract Node button_warning(int x,int y, int padX,int padY,int padW,int padH ,String texto);
	
	public abstract Node label (int x,int y, int padX,int padY,int padW,int padH ,String texto);

	public abstract Node text(int x,int y, String texto);
	
	public abstract Node comboBox (int x,int y, int padX,int padY,int padW,int padH);
	
	public abstract Node textField(int x,int y, int padX,int padY,int padW,int padH);
	
	public abstract Node borderPane(int x,int y, int padX,int padY,int padW,int padH);
	
	public abstract Node stackPane(int x,int y, int padX,int padY,int padW,int padH);
	
	public abstract Node pane(int x,int y, int padX,int padY,int padW,int padH);
	
	public abstract Node tableView(int x,int y, int padX,int padY,int padW,int padH);
	
	public abstract void addColums(TableView<String[]> table,String columns[],int minWidth[]);
	
	public abstract Node imageView(int x,int y, int width,int height,String path);
	
	public boolean existeArcivo(String path) {
		File f = new File(path);
		return f.exists();
	}
}
