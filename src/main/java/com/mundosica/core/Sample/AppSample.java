package com.mundosica.core.Sample;

import com.mundosica.core.Model.DataBase;
import com.mundosica.core.Utils.TaskManager;
import com.mundosica.core.common.AppInterface;
import com.mundosica.core.common.CoreApp;

public class AppSample extends CoreApp implements AppInterface {
	public AppSample() {
		super();
	}

	public void start() {
		System.out.println("AppSample.start()");
		DataBase.config("bd"   , "carniceria");
		DataBase.config("user" , "cruvi_user");
		DataBase.config("password" , "cruvi_pass");
	}
	public void run() {
		System.out.println("AppSample.run()");
	}
	public void end() {
		System.out.println("AppSample.end()");
	}

	//
	public static void main(String[] args) {
		new AppSample();
	}
}
