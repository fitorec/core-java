package com.mundosica.core.View;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Shell;


public class SWTDialog extends WindowHelper{

	public Shell shell;
	
	public SWTDialog(Shell shell) {
	}
	public void createShell(Shell shell) {
		this.shell = new Shell(shell,SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
		this.shell.isVisible();
	}
	public void startWindow() {
		createContents();
		shell.open();
	}
	public void createContents() {}
}
