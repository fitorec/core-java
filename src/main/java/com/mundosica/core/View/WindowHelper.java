package com.mundosica.core.View;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;

import com.mundosica.core.swtf.SWTResourceManager;

public class WindowHelper  {
	
	public Button makeButton(Group grupo,int SWT,int x, int y, int w, int h,int color,
			String text,SelectionListener listener) {
		Button boton = new Button(grupo,SWT);
		boton.setBounds(x,y,w,h);
		boton.setText(text);
		boton.addSelectionListener(listener);
		boton.setBackground(SWTResourceManager.getColor(color));
		return boton;
	}
	
	public Button makeButton(Shell shell,int SWT,int x, int y, int w, int h,int color,
			String text,SelectionListener listener) {
		Button boton = new Button(shell,SWT);
		boton.setBounds(x,y,w,h);
		boton.setText(text);
		boton.addSelectionListener(listener);
		boton.setBackground(SWTResourceManager.getColor(color));
		return boton;
	}
	
	public Label makeLabel (Group grupo,int swt,int x, int y, int w, int h, int alignment,
			KeyListener listener) {
		Label label = new Label(grupo,swt);
		label.setBounds(x,y,w,h);
		label.setFont(SWTResourceManager.getFont("Sans", 12, SWT.NORMAL));
		label.setAlignment(alignment);
		label.addKeyListener(listener);
		return label;
	}
	public Label makeLabel (Group grupo,int swt,int x, int y, int w, int h, int alignment) {
		Label label = new Label(grupo,swt);
		label.setBounds(x,y,w,h);
		label.setFont(SWTResourceManager.getFont("Sans", 12, SWT.NORMAL));
		label.setAlignment(alignment);
		return label;
	}
	
	public void makeColumns(Table table,int []swt,int []w, String []text) {
		for(int i = 0; i < text.length; i++) {
			this.makeColumn(table, swt[i], w[i], text[i]);
		}
	}
	
	public void makeColumn(Table table,int swt,int w, String text) {
		TableColumn column = new TableColumn(table, swt);
		column.setWidth(w);
		column.setText(text);
	}
	
	public Table makeTable(Group group,int x,int y, int w, int h, boolean header,boolean lines,
			MouseAdapter listening) {
		Table table = new Table(group,SWT.BORDER | SWT.FULL_SELECTION);
		table.setBounds(x,y,w,h);
		table.setHeaderVisible(header);
		table.setLinesVisible(lines);
		table.addMouseListener(listening);
		return table;
	}
	
	public Table makeTable(Group group,int x,int y, int w, int h, boolean header,boolean lines,
			KeyAdapter listening) {
		Table table = new Table(group,SWT.BORDER | SWT.FULL_SELECTION);
		table.setBounds(x,y,w,h);
		table.setHeaderVisible(header);
		table.setLinesVisible(lines);
		table.addKeyListener(listening);
		return table;
	}
	public Table makeTable(Group group,int x,int y, int w, int h, boolean header,boolean lines) {
		Table table = new Table(group,SWT.BORDER | SWT.FULL_SELECTION);
		table.setBounds(x,y,w,h);
		table.setHeaderVisible(header);
		table.setLinesVisible(lines);
		return table;
	}

}
