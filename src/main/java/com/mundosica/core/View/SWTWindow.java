package com.mundosica.core.View;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class SWTWindow extends WindowHelper {

	public Shell shell;
	Display display;
	
	public void createShell() {
		display = Display.getDefault();
		shell = new Shell(display);
	}
	public void startWindow(boolean dispose) {
		createContents();
		shell.open();
		while (!shell.isDisposed ()) {
			if (!display.readAndDispatch ()) display.sleep ();
		}
		if(dispose) {
			display.dispose ();
		}
		
	}
	public void createContents() {}
}
