package com.mundosica.core.View;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.mundosica.core.Utils.Config;
import com.mundosica.core.Model.DataBase;
import com.mundosica.core.Utils.Fecha;
import com.mundosica.core.swtf.SWTResourceManager;

import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Vector;

public class Helper {
	private static int width = 1366;
	private static int height = 717;
	private static FocusListener textFocuStyle;

	public static int w() {
		return Helper.width;
	}

	public static int h() {
		return Helper.height;
	}
	
	public static Vector<String> cortarEnRenglones(String str, Integer tam_renglon) {
		Vector<String> renglones = new Vector<String>();
		String parts[] = str.split(" ");
		Integer posInicial = 0;
		int pos = posInicial;
		do {
			int size = 0;
			for (; pos<parts.length; pos++) {
				size += parts[pos].length() + 1;
				if (size > tam_renglon) {
					break;
				}
			}
			String linea = "";
			for (int i=posInicial; i<pos; i++) {
				linea += " " + parts[i];
			}
			posInicial = pos;
			renglones.add(linea);
		} while (pos < parts.length);
		return renglones;
	}
 
	public static Group initGroupDerecho(Shell shell, String titulo, int pos_y, int alto) {
		Group grupo = new Group(shell, SWT.NONE);
		grupo.setText(titulo);
		grupo.setToolTipText(titulo);
		grupo.setBounds(730, pos_y, 620, alto);
		return grupo;
	}

	/**
	 * Inicializa el bloque del costado izquierdo en la interfaz.
	 * 
	 * @param shell contiene la shell swt.
	 * @param titulo contiene el titulo de la seccion izquierda.
	 * @param pos_y la posicion en Y
	 * @param alto la altura que tendra
	 * @return devuelve el grupo del costado izquierdo
	 */
	public static Group initGroupIzquierdo(Shell shell, String titulo, int pos_y, int alto) {
		Group grupo = new Group(shell, SWT.NONE);
		grupo.setText(titulo);
		grupo.setToolTipText(titulo);
		grupo.setBounds(20, pos_y, 700, alto);
		return grupo;
	}

	public static Group initGroupOnly(Shell shell, String titulo, int pos_y, int alto) {
		Group grupo = new Group(shell, SWT.NONE);
		grupo.setText(titulo);
		grupo.setToolTipText(titulo);
		grupo.setBounds(20, pos_y, 1330, alto);
		return grupo;
	}

	public static void insertLogo(Shell shell) {
		Label logo = new Label(shell, SWT.NONE);
		String ruta = Config.mediaPath() + "logo.png";
		logo.setImage(
			SWTResourceManager.getImage(ruta)
		);
		logo.setBounds(10, 5, 700, 129);
	}

	/**
	 * Inserta la imagen del caj�n de Dinero (Usado en /sucursal/VenInicioDia.java)
	 * Config.path() + "data" + Config.DS()+ "cajon_dinero.png"
	 * 
	 * @param grupo el grupo en donde se va agregar.
	 * @param ruta contiene la ruta de la imagen.
	 **/
	public static void insertCajonImg(Group grupo, String ruta) {
		Label logo = new Label(grupo, SWT.NONE);
		String pathImg = ruta;
		logo.setImage(SWTResourceManager.getImage(pathImg));
		logo.setBounds(10, 10, 135, 135);
	}
	
    public static void insertLogoSucursal(Shell shell, String nombreSucursal) {
    	String sucSlug = nombreSucursal.toLowerCase().replaceAll(" ", "_");
        Label logo = new Label(shell, SWT.NONE);
        String ruta = Config.mediaPath() + sucSlug + "/principal.png";
        System.out.println("insetLogo principal ruta: "+ruta);
        logo.setImage(SWTResourceManager.getImage(ruta));
        logo.setBounds(10, 5, 500, 129);
    }

    public static void insertLogoSmall(Group grupo) {
        Label logo = new Label(grupo, SWT.NONE);
        System.out.println("insetLogo small ruta: "+Config.mediaPath() + "logo-small.png");
        logo.setImage(SWTResourceManager.getImage(Config.mediaPath() + "logo-small.png"));
        logo.setBounds(10, 20, 135, 125);
    }
    public static void insertLogoSica(Group grupo) {
        Label logo = new Label(grupo, SWT.NONE);
        System.out.println("insetLogo sica ruta: "+Config.mediaPath() + "sica_logo.png");
        logo.setImage(SWTResourceManager.getImage(Config.mediaPath() + "sica_logo.png"));
        logo.setBounds(500, 20, 182, 125);
    }

	static public float str2Float(String str) {
		String numStr = Helper
				.stringNotNull(str)
				.replaceAll("[+^:,$ \t\n]", "");
			Float result;
			try {
				result = Float.parseFloat(numStr);
			} catch (Exception e) {
				result = (float)0;
			}
			return result;
	}

	static public boolean isNumeric(String str) {
	  return str.replaceAll(",", "").matches("-?\\d+(\\.\\d+)?");
	}

	/**
	 * Convierte un String a Entero
	 * 
	 * @param str el numero
	 * @return el valor numerico
	 */
	static public Integer str2Int(String str) {
		Integer result = null;
		if (null == str || 0 == str.length()) {
			return null;
		}
		try{
			result = Integer.parseInt(str);
		} catch (NumberFormatException e) {
			String numStr = Helper
					.stringNotNull(str)
					.replaceAll("[+^:,$ \t\n]", "");
			String negativeMode = "";
			if (numStr.indexOf('-') != -1) {
				negativeMode = "-";
			}
			numStr = numStr.replaceAll("-", "" );
			if (numStr.indexOf('.') != -1) {
				numStr = numStr.substring(0, numStr.indexOf('.'));
				if (numStr.length() == 0) {
					return (Integer)0;
				}
			}
			numStr = numStr.replaceAll("[^\\d]", "" );
			if (0 == numStr.length()) {
				return null;
			}
			result = Integer.parseInt(negativeMode + numStr);
		}
		return result;
	}

	static public String stringNotNull(String str) {
		if (str.trim().equals("")) {
			return "";
		} else {
			return str.trim();
		}
	}

	static public String subString(String s, int n) {
		if (s.length()<=n)
			return s;
		else
			return s.substring(0, n);
	}

	static public String strFormat(float f) {
		DecimalFormat inputFormat = new DecimalFormat("###,###,##0.00");
		return inputFormat.format(f);
	}

	static public String strFormat3(float f) {
		DecimalFormat inputFormat = new DecimalFormat("###,###,##0.000");
		return inputFormat.format(f);
	}

	static public String strFormat(int integer) {
		return  NumberFormat.getIntegerInstance().format(integer);
	}

	/**
	 * Convierte un dateTime a String
	 *
	 * @param dateTime un objeto del tipo org.eclipse.swt.widgets.DateTime
	 * @return resultado Un String que contiene la fecha/hora represantada por dateTime
	 */
	static public String strFormat(DateTime dateTime) {
 		if (null == dateTime) {
 			return "";
 		}
		return String.format("%04d", dateTime.getYear()) + "-" +
 				String.format("%02d", dateTime.getMonth()+1) + "-" +
 				String.format("%02d", dateTime.getDay());
	}

	/*
	 * String de entrada "2013-10-14 09:15:03"
	 * String de salida	"14/Oct/13 09:15:03"
	 */
	static public String strDateTimeFormat(String dateTime) {
		if (dateTime == null) {
			return "dd/mm/aa HH:MM";
		}
		String fecha = dateTime.substring(0, dateTime.indexOf(" "));
		String [] partsHora = dateTime.substring(dateTime.indexOf(" ")+1).split(":");
		String[] partsFecha = fecha.split("-");
		String dia =  partsFecha[2];
		String mes =  Fecha.nombreMesCapitalize(Integer.parseInt(partsFecha[1]) -1);
		String anio =  partsFecha[0];
		return  dia + "/"+ mes + "/" + anio + " " + partsHora[0] + ':' + partsHora[1];
	}

	static public String strDateTimeFormatSmall(String dateTime) {
		if (dateTime == null) {
			return "dd/mm/aa HH:MM";
		}
		String fecha = dateTime.substring(0, dateTime.indexOf(" "));
		String [] partsHora = dateTime.substring(dateTime.indexOf(" ")+1).split(":");
		String[] partsFecha = fecha.split("-");
		String dia =  partsFecha[2];
		String mes =  Fecha.nombreMesAbreviadoCapitalize(Integer.parseInt(partsFecha[1]) -1);
		String anio =  partsFecha[0].substring(2,4);
		return  dia + "/"+ mes + "/" + anio + " " + partsHora[0] + ':' + partsHora[1];
	}

	/**
	 * String de entrada recibe una fecha y devuelve el día de la seman correspondiente
	 * 
	 * @param dateTime un texto en formato fecha hora p.e.  "2013-10-14 09:15:03"
	 * @return diaSemana p.e. "domingo", "lunes", "martes", "miercoles", "jueves", "sabado"
	 */
	static public String strDateTimeDiaSemana(String dateTime) {
		if (dateTime == null) {
			return "---";
		}
		String fecha = dateTime.substring(0, dateTime.indexOf(" "));
		String[] partsFecha = fecha.split("-");
		int dia  = Integer.parseInt(partsFecha[2]);
		int mes  = Integer.parseInt(partsFecha[1]);
		int anio = Integer.parseInt(partsFecha[0]);
		Calendar c = Calendar.getInstance();
		c.set(Calendar.YEAR, anio);
		c.set(Calendar.MONTH, mes - 1); // 11 = december
		c.set(Calendar.DAY_OF_MONTH, dia);
		int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
		return Fecha.diaSemana(dayOfWeek);
	}

	static public String strHora(String datetime) {
		String[] fecha = datetime.split(" ");
		if (fecha.length>1) {
			String hora[] = fecha[1].split(":");
			if (hora.length>1) {
				return hora[0]+":" + hora[1];
			}
		}
		return "--";
	}
	static public int mesAnterior(DateTime dt) {
		if ( dt.getMonth()>1 ) {
			return dt.getMonth()-1;
		}
		return 11;
	}

	static public String redondearCifra(String cifra) {
		float result = Helper.str2Float(cifra);
		return Helper.redondearCifra(result);
	}
	static public String redondearCifra(float cifra) {
		return Helper.redondearCifra(Helper.strFormat(cifra));
	}
	static public float redondear(float cifra) {
		String strCifra = String.format("%.3f", cifra);
		strCifra = strCifra.replaceAll(",", ".");
		String numParts[] = strCifra.split("\\.");
		int parteEntera = Integer.parseInt(numParts[0]);
		int parteFraccionaria = Integer.parseInt(numParts[1]);
		if (parteFraccionaria > 75) {
			parteEntera++;
			parteFraccionaria = 0;
		} else if (parteFraccionaria > 25) {
			parteFraccionaria = 5;
		} else {
			parteFraccionaria = 0;
		}
		//return Float.parseFloat(parteEntera + "." + parteFraccionaria);
		return Float.parseFloat(strCifra);
	}
	static public float redondear(double cifra) {
		return Helper.redondear((float)cifra);
	}

	static public Text newText(Group grupo) {
		Text t = new Text(grupo, SWT.BORDER);
		t.addFocusListener(Helper.textFocuStyle());
		t.setFont(SWTResourceManager.getFont("Sans", 12, SWT.NORMAL));
		return t;
	}
	/*
	 * Regresa un control de Focus con color y Estilo
	 */
	static public FocusListener textFocuStyle() {
		if (Helper.textFocuStyle == null ) {
			Helper.textFocuStyle = new FocusListener() {
				public void focusGained(FocusEvent e) {
					Text fieldSource = (Text) e.getSource();
					fieldSource.setBackground(SWTResourceManager.getColor(SWT.COLOR_CYAN));
				}
				public void focusLost(FocusEvent e) {
					Text fieldSource = (Text) e.getSource();
					fieldSource.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
				}
			};
		}
		return Helper.textFocuStyle;
	}
	public static org.eclipse.swt.graphics.Color backgroundEdo(String estado) {
		switch (estado) {
			case "finalizado":
				return SWTResourceManager.getColor(255, 95, 95);
			case "reparto":
			return SWTResourceManager.getColor(136, 255, 129);
			case "produccion":
				return SWTResourceManager.getColor(255, 200, 141);
			default:
				return SWTResourceManager.getColor(160, 217, 255);
		}
	}

	/**
	 * Realiza un pad hacia la derecha
	 * 
	 * @param text el texto a formatear.
	 * @param size la longitud a expander
	 * @return strPadRight texto justificado hacia la derecha
	 * 
	 **/
	public static String padRight(String text, int size) {
		return String.format("%1$-" + size + "s", text.trim());
	}

	public static String padLeft(String s, int n) {
		String result = "";
		String tmp = s.trim();
		Integer c;
		for (c=tmp.length(); c<n; c++) {
			result += " ";
		}
		return result + tmp;
	}
	public static String padCenter(String text, int len) {
		String out = String.format("%"+len+"s%s%"+len+"s", "",text,"");
		float mid = (out.length()/2);
		float start = mid - (len/2);
		float end = start + len;
		return out.substring((int)start, (int)end);
	}
	public static float obtenerIVA(float importe) {
			return (float)(importe * IVA());
	}

	/**
	 * Devuelve el iva
	 * 
	 * Esto no deveria de estar aquí! no se en donde se ocupa pero no deberia de estar.
	 * 
	 * @return el valor del IVA
	 */
	@Deprecated
	private static float IVA () {
		String valor = "0.16";
		try {
			String sql = "SELECT valor FROM configuraciones WHERE nombre = 'IVA'";
			Statement s = DataBase.conexion().createStatement();
			ResultSet rs = s.executeQuery(sql);
			if (rs != null && rs.first()) {
				valor = rs.getString(1);
			}
		}
		catch(Exception as) {
			as.printStackTrace();
		}
		return str2Float(valor);
	}
}
