package com.mundosica.core.Utils;

import org.eclipse.swt.widgets.Shell;
import org.eclipse.swtbot.swt.finder.SWTBot;
import com.mundosica.core.common.CoreApp;

public class SwtTester extends CoreApp {
	public static Shell shell = null;
	public static SWTBot swtBot;
	public SwtTester() {
		super();
		this._run();
	}
	public SwtTester(Shell sh) {
		super();
		SwtTester.shell = sh;
		this._run();
	}

	/**
	 * Metodo run de la aplicación
	 * 
	 * Se encarga de mandar a llamar al metodo run de la aplicacion
	 */
	public void _run() {
		while (SwtTester.swtBot == null) {
			if (SwtTester.shell != null) {
				SwtTester.swtBot = new SWTBot();
			}
			this.waitASecond();
		}
		SwtTester.swtBot.activeShell();
		this.execMethod("run");
	}


	private void waitASecond() {
		if (SwtTester.swtBot != null) {
			return;
		}
		try {
			Thread.sleep(1000);
		} catch (InterruptedException ex) {
			Thread.currentThread().interrupt();
		}
	}
}
