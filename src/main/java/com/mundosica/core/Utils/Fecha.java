package com.mundosica.core.Utils;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Group;
import com.mundosica.core.View.Helper;

import java.util.Date;
import java.util.Vector;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
/**
 * Clase para el control de fechas
 * @author fitorec
 */

public class Fecha {
	private static int numDiasPorMes[] = {30,27,30,29,30,29,30,30,29,30,29,30};

	private static String nombreMeses[] = {
		"enero",
		"febrero",
		"marzo",
		"abril",
		"mayo",
		"junio",
		"julio",
		"agosto",
		"septiembre",
		"octubre",
		"noviembre",
		"diciembre"
	};

	private static String nombresDiasSemana[] = {
		"Domingo",	// 1
		"Lunes",	// 2
		"Martes",	// 3
		"Miercoles",// 4
		"Jueves",	// 5
		"Viernes",	// 6
		"Sabado"	// 7
	};

	/**
	 * Recibe el numero del dia en un rango del [1-7]
	 * 
	 * @param indexDia el indice del dia
	 * @return el nombre del dia de la semana  [domingo,...,sabado]
	 */
	static public String diaSemana(int indexDia) {
		int index = indexDia - 1;
		if (Fecha.rangoDiaSemanaValido(index)) {
			return Fecha.nombresDiasSemana[index];
		}
		return "[" + index + "]";
	}

	static public boolean rangoDiaSemanaValido(int indexDia) {
		return (indexDia >= 0 && indexDia < 7);
	}

	/**
	 * Devuelve el nombre del mes para el número dado.
	 *
	 * @param numeroDelMes el numero del mes en un rango del [0-11]
	 * @return Devuelve el nombre del mes de [enero-diciembre]
	 **/
	static public String nombreMes(int numeroDelMes) {
		if (Fecha.rangoNumMesValido(numeroDelMes)) {
			return Fecha.nombreMeses[numeroDelMes];
		}
		return "Error:nombreMes";
	}

	static public String nombreMesCapitalize(int numeroDelMes) {
		String strMes = Fecha.nombreMes(numeroDelMes);
		Character c = new Character(strMes.charAt(0));
		strMes = Character.toUpperCase(c) + strMes.substring(1);
		return strMes;
	}

	static public String nombreMesAbreviado(int numeroDelMes) {
		return Fecha.nombreMes(numeroDelMes).substring(0, 3);
	}

	static public String nombreMesAbreviadoCapitalize(int numeroDelMes) {
		return Fecha.nombreMesCapitalize(numeroDelMes).substring(0, 3);
	}

	static public int ultimoDiaDelMes(int numeroDelMes) {
		if (Fecha.rangoNumMesValido(numeroDelMes)) {
			return Fecha.numDiasPorMes[numeroDelMes];
		}
		return -1;
	}

	static public int mesAnterior(DateTime date) {
		return mesAnterior(date.getMonth());
	}

	static public int mesAnterior(int mes) {
		return (mes>0 )? mes-1 :11;
	}
	static public int mesSiguiente(DateTime date) {
		return mesSiguiente(date.getMonth());
	}

	static public int mesSiguiente(int mes) {
		return ( mes < 11 )? mes+1 :1;
	}

	public static DateTime dateTimeMesAnterior(Group grupo) {
		DateTime date = new DateTime(grupo, SWT.BORDER | SWT.DATE | SWT.DROP_DOWN);
		int mes = Fecha.mesAnterior(date);
		if (date.getDay()>Fecha.ultimoDiaDelMes(mes)+1) {
			date.setDay(Fecha.ultimoDiaDelMes(mes)+1);
		}
		if (mes == 11) {
			date.setYear(date.getYear() - 1);
		}
		date.setMonth(mes);
		return date;
	}
	public static DateTime dateTimeMesSiguiente(Group grupo) {
		DateTime date = new DateTime(grupo, SWT.BORDER | SWT.DATE | SWT.DROP_DOWN);
		int mes = Fecha.mesSiguiente(date);
		if (date.getDay() > Fecha.ultimoDiaDelMes(mes)+1) {
			date.setDay(Fecha.ultimoDiaDelMes(mes)+1);
		}
		if (mes == 11) {
			date.setYear(date.getYear() - 1);
		}
		date.setMonth(mes);
		return date;
	}

	public static DateTime dateTimeDiaSiguiente(Group grupo) {
		DateTime date = new DateTime(grupo, SWT.BORDER | SWT.DATE | SWT.DROP_DOWN);
		int year = date.getYear();
		int mounth = date.getMonth();
		int day = date.getDay();

		int ultimoDia = Fecha.ultimoDiaDelMes(mounth) + 1;
		if (ultimoDia > day) {
			day++;
		} else {
			day = 1;
			if (mounth<11) {
				++mounth;
			} else {
				++year;
				mounth = 0;
			}
		}
		//System.out.println(day+"-"+mounth+"-"+year);
		date.setDate(year, mounth, day);
		return date;
	}

	public static String fechaActual() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		return dateFormat.format(date);
	}

	public static String fechaHoraActual() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		return dateFormat.format(date);
	}
	/*
	 * Devuelve 15 dias previos
	 */
	public static Vector<String> diasPrevios(String dia) {
		Vector<String> dias = new Vector<String>();
		String dia_actual = dia;
		for (int i = 0; i<15; i++) {
			dia_actual = Fecha.diaPrevio(dia_actual);
			dias.add(dia_actual);
		}
		return dias;
	}
	/*
	 * Revise una cadena de la forma AAAA-dd-mm
	 * Y devuelve otra cadena en el mismo formato (AAAA-dd-mm) pero del día previo
	 *
	 */
	public static String diaPrevio(String dia) {
		String nuevo_dia = null;
		if (dia.length() != 10) {
			return null;
		}
		try {
			int year = Helper.str2Int(dia.substring(0, 4));
			int mounth = Helper.str2Int(dia.substring(5, 7));
			int day = Helper.str2Int(dia.substring(8, 10));
			nuevo_dia = ""+year;
			if (day>1) {
				day--;
			} else {
				if (mounth>1) {
					--mounth;
				} else {
					--year;
					mounth = 12;
				}
				day = Fecha.numDiasPorMes[mounth-1] + ((mounth==2 && year%4==0)? 2 : 1);
			}
			nuevo_dia = year+"-"+(mounth>9? "": "0") + mounth+"-"+(day>9? "": "0")+day;
		} catch (Exception e) {
			return "Error de conversion";
		}
		return nuevo_dia;
	}
	/*
	 * Revise una cadena de la forma AAAA-dd-mm
	 * Y devuelve otra cadena en el mismo formato (AAAA-dd-mm) pero del día previo
	 *
	 */
	public static String diaSiguiente(String dia) {
		String nuevo_dia = null;
		if (dia.length() != 10) {
			return null;
		}
		try {
			int year = Helper.str2Int(dia.substring(0, 4));
			int mounth = Helper.str2Int(dia.substring(5, 7));
			int day = Helper.str2Int(dia.substring(8, 10));
			nuevo_dia = ""+year;

			int ultimoDia = Fecha.ultimoDiaDelMes(mounth-1) + 1;
			if (ultimoDia > day) {
				day++;
			} else {
				day = 1;
				if (mounth<12) {
					++mounth;
				} else {
					++year;
					mounth = 1;
				}
			}
			nuevo_dia = year+"-"+(mounth>9? "": "0") + mounth+"-"+(day>9? "": "0")+day;
		} catch (Exception e) {
			return "Error de conversion";
		}
		return nuevo_dia;
	}
	/*
	 * Devuelve TRUE si numeroDelMes esta en el rango de [0-11]
	 */
	static public boolean rangoNumMesValido(int numDelMes) {
		return (numDelMes>=0 && numDelMes<12);
	}
	
	/*
	 * regresara 
	 * 0 = si f1 es igual a f2
	 * valor positivo = si f1 mayor a f2
	 * valor negativo = si f1 es menor a f2
	 * null si alguna fecha esta mal
	 */
	
	static public int compararFechas(String f1, String f2) {
		try {
			DateFormat formatter = new SimpleDateFormat("yyyy-MM-DD"); 
			Date date1 = (Date)formatter.parse(f1);
			Date date2 = (Date)formatter.parse(f2);
			return date1.compareTo(date2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return -1;
	}
	/*
	 * regresara 
	 * 0 = si f1 es igual a f2
	 * valor positivo = si f1 mayor a f2
	 * valor negativo = si f1 es menor a f2
	 * null si alguna fecha esta mal
	 */
	static public Integer compararFechasConHora(String f1, String f2) {
		try {
			DateFormat formatter = new SimpleDateFormat("yyyy-MM-DD HH:mm:ss"); 
			Date date1 = (Date)formatter.parse(f1);
			Date date2 = (Date)formatter.parse(f2);
			return date1.compareTo(date2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}

	public static void main(String[] args) {
		/*for (int i=0; i<12; i++)
			System.out.println(Fecha.nombreMesAbreviadoCapitalize(i) + ' ' + Fecha.nombreMesCapitalize(i));
		 */
		/*String dia = Fecha.diaPrevio("2013-07-25");*/
		//System.out.println(Fecha.fechaHoraActual());
		//Vector<String> dias = Fecha.diasPrevios("2013-03-10");
		//for (int i = 0; i<15; i++) {
		//	System.out.println(dias.get(i));
		//}
		String fechaActual = Fecha.fechaHoraActual(); //Fecha.fechaActual();
		String fechalocochona = "2016-02-10 18:00:00";
		System.out.println(fechaActual);
		Integer a = Fecha.compararFechasConHora(fechaActual, fechalocochona);
		System.out.println(a);
	}
}
