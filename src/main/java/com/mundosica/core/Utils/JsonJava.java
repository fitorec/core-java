package com.mundosica.core.Utils;

import java.util.HashMap;
import java.util.Vector;

public class JsonJava {
	private static char scapeArray[] = {'#', '~', '_', 'ñ', '%'};

	public static Vector<HashMap<String,String>> VectorHash(String json) {
		Vector<HashMap<String,String>> vec = new Vector<HashMap<String,String>>();
		if (json == null) {
			return vec;
		}
		String scape = JsonJava.introspect(json);
			json = json.replaceAll("\\\\\"", scape).replaceAll("\"", "");
		Vector<String> jsonObject =  VectorJsonObject(json);
		for (int i = 0; i < jsonObject.size(); i++) {
			vec.add(fiel_values(jsonObject.get(i), scape));
		}
		return vec;
	}

	public static String introspect(String json) {
		for (int i=0; i<scapeArray.length; i++) {
			if(json.indexOf(scapeArray[i]) == -1) {
				return  scapeArray[i] + "";
			}
		}
		return "";
	}

	public static Vector<String> VectorJsonObject(String jsonString) {
		Vector<String> vec = new Vector<String>();
		char caracteres[] = jsonString.toCharArray();
		boolean intoObject = false;
		String jsonObject = "";
		for (int i = 0; i < caracteres.length; i++) {
			if(caracteres[i] == '}') {
				intoObject = false;
				vec.add(jsonObject);
				jsonObject = "";
			}
			if(intoObject) {
				jsonObject += caracteres[i];
			}
			if(caracteres[i] == '{') {
				intoObject = true;
			}
		}
		return vec;
	}
	
	public static HashMap<String,String> fiel_values(String jsonObject, String scape) {
		HashMap<String,String> hash = new HashMap<String,String>();
		String fiel_value[] = jsonObject.split(",");
		for (int i = 0; i < fiel_value.length; i++) {
			String campo = fiel_value[i].split(":")[0];
			String valor = fiel_value[i].replaceAll(campo+":", "").replaceAll(scape, "\"");
			if(valor.equals("true")) {
				valor = "1";
			}
			if(valor.equals("false")) {
				valor = "0";
			}
			hash.put(campo, valor);
		}
		return hash;
	}

	public static void main (String...  ar) {
		String json = "{\"id\":\"165\",\"username\":\"grupo \\\"tomasin\\\"\",\"password\":\"4c6a34aafcea226d1b275f51264fe437db287630\",\"role\":\"Alumno\",\"nombre\":\"MARGARITA\",\"apellido_paterno\":\"MU\u00d1OZ \",\"apellido_materno\":\"HERNANDEZ\",\"email\":\"mugrosita_76@hotmail.com\",\"fecha_nacimiento\":\"2011-09-17\",\"telefono\":\"\",\"telefono_movil\":\"\",\"compania_telefonica\":\"\",\"num_mensajes_pendientes\":null,\"created\":\"2014-09-03 14:29:43\",\"modified\":\"2015-05-07 18:13:25\",\"genero\":\"M\",\"email_token\":null,\"email_token_expires\":null,\"last_login\":\"2015-04-27 18:13:25\",\"nombre_completo\":\"MARGARITA MU\u00d1OZ  HERNANDEZ\"},{\"id\":\"175\",\"username\":\"LD131GARM950912\",\"password\":\"84a590d890f7c4d5291a91c9d225d71497120384\",\"role\":\"Alumno\",\"nombre\":\"MICHAEL YOSHIO \",\"apellido_paterno\":\"GARCIA\",\"apellido_materno\":\"REYES\",\"email\":\"yoshio-96@hotmail.com\",\"fecha_nacimiento\":null,\"telefono\":\"\",\"telefono_movil\":null,\"compania_telefonica\":null,\"num_mensajes_pendientes\":null,\"created\":\"2014-09-03 14:42:07\",\"modified\":\"2015-05-07 18:13:25\",\"genero\":\"\",\"email_token\":null,\"email_token_expires\":null,\"last_login\":\"2015-04-27 23:27:53\",\"nombre_completo\":\"MICHAEL YOSHIO  GARCIA REYES\"}";
		Vector<HashMap<String,String>> vec  = JsonJava.VectorHash(json);
		System.out.println(vec.get(1).get("username"));
	}

}