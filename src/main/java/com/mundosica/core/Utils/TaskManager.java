package com.mundosica.core.Utils;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Set;

import javax.swing.JFrame;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;

import com.mundosica.core.Model.QueryManager;
import com.mundosica.core.Network.Host;
import com.mundosica.core.Network.NetworkHelper;

abstract public class TaskManager  extends Thread {
	static private Boolean stop = false;
	private Boolean have_internet = false;

	public TaskManager() {}

	public TaskManager(Shell shell) {
		shell.addListener(SWT.Close, new Listener() {
			@Override
			public void handleEvent(Event arg0) {
				System.out.println("intentando cerrar");
				TaskManager.forceStop();
			}
		});
	}
	
	public TaskManager(JFrame frame) {
		frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
		frame.addWindowListener(new java.awt.event.WindowAdapter() {
		    @Override
		    public void windowClosing(java.awt.event.WindowEvent windowEvent) {
		    	System.out.println("intentando cerrar jframe");
				TaskManager.forceStop();
				//System.exit(0);
		    }
		});
	}

	public void run() {
		this.checkIfHaveInternet();
		while (TaskManager.stop == false) {
			String curTime = QueryManager.curtime();
			String curTimeData[] = curTime.split(":");
			int currentMinutes = Integer.parseInt(curTimeData[1]);
			int currentSeconds = Integer.parseInt(curTimeData[2]);
			if (currentSeconds != 0) {
				int secondsRemaining = 60 - currentSeconds;
				this.waitASeconds(secondsRemaining);
				continue;
			}
			this.every1min();
			if (currentMinutes % 5 == 0) {
				this.every5mins();
			}
			if (currentMinutes % 10 == 0) {
				this.every10mins();
			}
			this.checkIfHaveInternet();
			this.waitASecond();
		}
		this.interrupt();
	}

	public static Boolean end() {
		return TaskManager.stop;
	}

	@SuppressWarnings("deprecation")
	public static void forceStop() {
		TaskManager.stop = true;
		Set<Thread> proccess = Thread.getAllStackTraces().keySet();
		for (Thread t : proccess) {
			if (t.getState() == Thread.State.RUNNABLE) { 
				t.interrupt();
				t.stop();
			}
		}
	}

	/**
	 * Esta función es disparada cada 5 minutos
	 */
	public void every5mins() {
	}
	/**
	 * Esta función es disparada cada 5 minutos
	 */
	public void every10mins() {
	}
	
	/**
	 * Esta función es disparada cada minuto
	 */
	public void every1min() {
	}

	/**
	 * Se encarga de esperar 1 minuto
	 */
	public void waitASecond() {
		this.waitMiliSeconds(1000);
	}

	/**
	 * Se encarga de esperar num_seconds segundos
	 */
	public void waitASeconds(int num_seconds) {
		for (int i=0;  i<num_seconds; i++) {
			this.waitASecond();
		}
	}

	private void waitMiliSeconds(int numMiliSeconds) {
		if (TaskManager.stop) {
			return;
		}
		try {
			Thread.sleep(numMiliSeconds);
		} catch (InterruptedException ex) {
			Thread.currentThread().interrupt();
		}
	}

	public boolean haveInternet() {
		return this.have_internet;
	}

	/**
	 * Revisa si hay conexión a Internet de, ser así obtiene la IP
	 * y la guarda en la BD por medio de un customField de configuración
	 */
	public void checkIfHaveInternet() {
		if (TaskManager.stop) {
			return;
		}
		//saber si hay internet
		NetworkHelper net = new NetworkHelper();
		this.have_internet = net.ping("http://google.com");
		Host host = net.localHost();
		Config.customField("IP", host.getIp(0));
		//System.out.println(host.getIp(0));
		/*
		//obtener ip de salida
		String url = "http://emprefacil.com/mi-ip";
		HttpURLConnection connect;
		URL ur = null;
		try {
			ur = new URL(url);
			connect = (HttpURLConnection) ur.openConnection();
			InputStream stream = connect.getInputStream();
			int codigo = connect.getResponseCode();
			if (stream != null && codigo == 200) {
				BufferedReader buffer = new BufferedReader(new InputStreamReader(stream));
				String ip = buffer.readLine();
				buffer.close();
				//if(Config.validIP4(ip)) {// checar la validacion de las ips
				if(true) {
					 Config.customField("IP", ip);
					 System.out.println("IP : " + ip);
					 System.out.println("Codigo response: " + codigo);
					 System.out.println("Stream: " + stream);
				} else {
					System.out.println("IP invalida: " + ip);
				}
				return;
			} else {
				System.out.println("Codigo response: " + codigo);
				System.out.println("Stream: " + stream);
			}
		} catch (Exception e) {
			//e.printStackTrace();
			System.out.println("Error no se puede obtener la ip");
			Config.customField("IP", net.localHost().getIp(0));
		}
		////*/
		
	}
}
