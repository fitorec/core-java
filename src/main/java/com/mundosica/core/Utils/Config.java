package com.mundosica.core.Utils;

import java.io.IOException;
import java.net.URL;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.Vector;
import java.util.regex.*;

import org.json.JSONObject;

import com.mundosica.core.Model.ConfiguracionModel;

public class Config {
	protected static String rootPath;
	protected static String media;
	protected static String DATA;
	public static  Character DS = '\\';
	private static String runCake;
	private static String urlServer;
	private static String os = null;

	private static void setRutas() {
		if (Config.media == null) {
			Config.os();
			if (Config.os.equals("windows")) {		 // Windows OS
				Config.media = Config.getOsPath(Config.rootPath) + "media" + Config.DS+Config.DS;
				Config.DATA  = Config.getOsPath(Config.rootPath) + "data" + Config.DS+Config.DS;
				// Cargamos la configuración del archivo
				// C:\wamp\ecokami\data\wampmanager.conf
				String phpVersion = "php5.3.13";
				String  wampFileConf = "C:\\wamp\\wampmanager.conf";
				try{
					InputStream input = new FileInputStream(wampFileConf);
					Properties prop = new Properties();
					prop.load(input);
					phpVersion = prop.getProperty("phpVersion").replaceAll("\"","");
					System.out.println("Version de php: ["+phpVersion+"]");
				} catch (IOException ex) {
					ex.printStackTrace();
					System.out.println("Error al intentar leer el archivo de configuración:" + wampFileConf);
				}
				String phpPath  = Config.getOsPath("C:\\wamp\\bin\\php\\php\\"+phpVersion) + "php.exe";
				String cakePath = "C:\\wamp\\emprefacil\\web\\app";
				Config.cakeShell(cakePath, phpPath);

			} else if (Config.os.equals("linux")) { //Linux y Unix
				Config.media = Config.getOsPath(Config.rootPath) + "media" + Config.DS;
				Config.DATA  = Config.getOsPath(Config.rootPath) + "data" + Config.DS;
				Config.cakeShell("/bin/cruvi/web", "/usr/bin/php");
			} else {
				System.out.println("Sistema operativo indefinido");
			}
		}
	}

	/**
	 * Setea el rootPath si es el s.o. valido
	 *
	 * @param OS Sistema operativo: windows ó linux
	 * @param path : La ruta que se intenta setear
	 * @return rootPath regresa el valor actual del rootPath una vez que revise si corresponde al OS
	 */
	public static String rootPath(String OS, String path) {
		Config.os();
		if (Config.os.equals(OS)) {
			Config.rootPath = Config.getOsPath(path);
		}
		return Config.rootPath();
	}

	public static String rootPath() {
		return Config.rootPath;
	}

	public static String os() {
		if (Config.os == null) {
			String osCurrent = System.getProperty("os.name").toLowerCase();
			if (osCurrent.indexOf("win") >= 0) {// Windows OS
				Config.os = "windows";
			} else {
				Config.os = "linux";
			}
		}
		Config.DS = File.separatorChar;
		return Config.os;
	}

	public static String cakeShell() {
		if (Config.runCake  == null) {
			Config.setRutas();
		}
		return Config.runCake;
	}

	public static String cakeShell(String pathCake) {;
		return Config.cakeShell(pathCake, null);
	}

	public static String cakeShell(String pathCake, String pathPhp) {
		if (pathPhp == null) {
			pathPhp = "php";
		}
		pathCake = Config.getOsPath(pathCake);
		Config.runCake = pathPhp + " '" +  pathCake + "Console"+Config.DS+"cake.php'";
		Config.runCake += " -working '" + pathCake + "' ";
		return Config.cakeShell();
	}

	/**
	 *
	 * Devuelve una ruta valida para el sistema operativo.
	 *
	 * <a href="http://stackoverflow.com/questions/13153697/how-to-replace-with-in-a-java-string#answer-13153894"> ver detalles en stackOverFlow</a>
	 * @param path : la ruta del path
	 * @return El path ya valido para el sistema operativo
	 */
	public static String getOsPath(String path) {
		Config.os();
		String slash = Config.DS.toString()+Config.DS.toString();
		path = path
			.replaceAll("/", Config.DS.toString())
			.replaceAll("\\\\", slash+slash);
		if (path.charAt(0) == Config.DS && Config.os().equals("windows") ) {
			path = "C:" + path;
		}
		if (path.charAt(path.length() -1) != Config.DS) {
			path += Config.DS;
		}
		System.out.println("com.mundosica.core.Utils.getOsPath = " + path);
		return path;
	}

	public static String urlServer() {
		if (Config.urlServer  == null) {
			Config.setRutas();
		}
		return Config.urlServer;
	}

	public static String dataPath() {
		if (Config.DATA  == null) {
			Config.setRutas();
		}
		return Config.DATA;
	}

	public static String mediaPath() {
		if (Config.media == null) {
			Config.setRutas();
		}
		return Config.media;
	}

	/**
	 * SET: Establece un campo personalizado
	 *
	 * @param nameField : el nombre del campo
	 * @param value : el valor a dicho campo
	 * @return devuelve el valor ya seteado.
	 */
	public static String customField(String nameField, String value) {
		ConfiguracionModel c = new ConfiguracionModel(nameField);
		c.set("valor", value);
		c.save();
		return value;
	}

	/**
	 * Regresa el valor de un campo personalizado
	 * 
	 * @param nameField : el nombre del campo a devolver.
	 * @return devuelve el valor correspondiente al nameField
	 */
	public static String customField(String nameField) {
		ConfiguracionModel c = new ConfiguracionModel(nameField);
		return c.get("valor");
	}

	/**
	 * Imprime la configuración.
	 * 
	 */
	public static void imprimirConfig() {
		System.out.println("rootPath: " + Config.rootPath);
		System.out.println("media: " + Config.media);
		System.out.println("data: " + Config.DATA);
		System.out.println("ds: " + Config.DS);
		System.out.println("runCake: " + Config.runCake);
		System.out.println("urlServer: " + Config.urlServer);
		System.out.println("os: " + Config.os);
		ConfiguracionModel c = new ConfiguracionModel();
		Vector<String> namesConfs = c.pagine();
		int numConfigs = namesConfs.size();
		for (int i = 0; i < numConfigs; i++) {
			String nameConf = namesConfs.get(i);
			c.load(nameConf);
			System.out.println(c.get("nombre") + ": " + c.get("valor"));
		}
	}

	/**
	 * Elimna una variable del vector customField
	 *
	 * @param nameField : el nombre del campo a borrar
	 */
	public static void customFieldDelete(String nameField) {
		(new ConfiguracionModel(nameField)).delete();
	}

	public static boolean validIP4(String ip) {
		String exprecionRegular = "^(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})$";
		Pattern patron = Pattern.compile(exprecionRegular);
		if (ip == null || ip.length() < 8) {
			return false;
		}
		ip = ip.trim();
		String octeto[] = ip.split("\\.");
		if (octeto.length != 4) {
			return false;
		}
		//if (octeto[0].equals("0") || octeto[3].equals("0")) {
		if (octeto[0].equals("0")) {

			return false;
		}
		Matcher validador = patron.matcher(ip);
		return (boolean) validador.matches();
	}
	
	public static String leerArchivo(String path) {
		String cadena = null;
		try {
			File archivo = new File(path);
			if(!archivo.exists()) {
				if(archivo.createNewFile()) {
					escribirArchivo(
						path,
						"{\"caja_id\":\"1\",\"databaseHostMac\":\"undefined\","
						+ "\"databaseHostIp\":\"localhost\"}");
				}
			}
			FileReader f = new FileReader(path);
	        BufferedReader b = new BufferedReader(f);
	        String salida = b.readLine();
	        cadena = salida!= null ? "":null;
	        while(salida != null) {
	            cadena += salida;
	            salida = b.readLine();
	        }
	        b.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cadena;
	}
	
	public static void escribirArchivo(String path,String text) {
		FileWriter fichero = null;
	    PrintWriter pw = null;
	    try {
	    	fichero = new FileWriter(path);
	        pw = new PrintWriter(fichero);
	        pw.println(text);
	    } catch (Exception e) {
	    	e.printStackTrace();
	    }
	    try {
	    	if(fichero != null) {
		    	fichero.close();
		    }
	    } catch (Exception e) {
	    	e.printStackTrace();
	    }
	}
	
	public static JSONObject archivoConfig() {
		String json = leerArchivo(Config.DATA + "config.conf");
		if(json == null) {
			return null;
		}
		return new JSONObject(json);
	}
	
	public static String config(String field) {
		JSONObject config = archivoConfig();
		String value = null;
		try {
			if(config == null) {
				return null;
			}
			value = config.getString(field);
		} catch (Exception e) {
			System.out.println("no esta la configuracion para: " + field);
			//e.printStackTrace();
		}
		return value;
		
	}
	public static void config(String field,String value) {
		String json = leerArchivo(Config.DATA + "config.conf");
		if(json == null) {
			return;
		}
		JSONObject config = new JSONObject(json);
		config.put(field, value);
		escribirArchivo(Config.DATA + "config.conf",config.toString()); 
	}
}
