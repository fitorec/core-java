package com.mundosica.core.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Inflector {
	private static Vector<IrregularRule> irregular;

	public static String singular(String word) {
		Inflector.initIrregular();
		Iterator<IrregularRule> i = Inflector.irregular.iterator();
		while (i.hasNext()) {
			IrregularRule rule = (IrregularRule) i.next();
			if (rule.plural.equals(word)) {
				return rule.singular;
			}
		}
		word = word
				//Las palabras que terminan en -Z, cambian a –CES
				.replaceAll("ces$", "z")
				.replaceAll("es$", "");
		return word.replaceAll("s$", "");
	}

	public static String plural(String singularWord) {
		singularWord = singularWord
				//Las palabras que terminan en -Z, cambian a –CES
				.replaceAll("z$", "ce")
				//Las palabras que terminan en x,s y son Agudas 
				.replaceAll("([áéíóú])([xs])$", "\1ses");
		Inflector.initIrregular();
		Iterator<IrregularRule> i = Inflector.irregular.iterator();
		while (i.hasNext()) {
			IrregularRule rule = (IrregularRule) i.next();
			if (rule.plural.equals(singularWord)) {
				return rule.plural;
			}
		}
		///
		Pattern singular = Pattern.compile("[rlnd]+$");
		Matcher matcher = singular.matcher(singularWord);
		if (matcher.find()) {
			singularWord += "e";
		}
		singular = Pattern.compile("[taeiouj]+$");
		matcher = singular.matcher(singularWord);
		if (matcher.find()) {
			singularWord += "s";
		}
		return singularWord;
	}

	public static void appendIrregular(String singular, String plural) {
		Inflector.initIrregular();
		Inflector.irregular.add(new IrregularRule(singular, plural));
	}

	private static void initIrregular() {
		if (Inflector.irregular == null) {
			Inflector.irregular = new Vector<IrregularRule>();
		}
	}

	/**
     * Convierte de formato CamelCase a underscores p.e: "AliceInWonderLand" devuelve:
     * "alice_in_wonderland"
     *
     * @param camel camel case input
     * @return result converted to underscores.
     */
    public static String underscore(String camel) {
        List<Integer> upper = new ArrayList<Integer>();
        byte[] bytes = camel.getBytes();
        for (int i = 0; i < bytes.length; i++) {
            byte b = bytes[i];
            if (b < 97 || b > 122) {
                upper.add(i);
            }
        }

        StringBuilder b = new StringBuilder(camel);
        for (int i = upper.size() - 1; i >= 0; i--) {
            Integer index = upper.get(i);
            if (index > 0) {
                b.insert(index, "_");
            }
        }

        return b.toString().toLowerCase();
    }

    /**
     * Generates a camel case version of a phrase from underscore.
     *
     * @param underscore underscore version of a word to converted to camel case.
     * @return camel case version of underscore.
     */
    public static String camelize(String underscore) {
        return camelize(underscore, true);
    }


    /**
     * Generates a camel case version of a phrase from underscore.
     *
     * @param underscore underscore version of a word to converted to camel case.
     * @param capitalizeFirstChar set to true if first character needs to be capitalized, false if not.
     * @return camel case version of underscore.
     */
    public static String camelize(String underscore, boolean capitalizeFirstChar) {
        StringBuilder result = new StringBuilder();
        StringTokenizer st = new StringTokenizer(underscore, "_");
        while (st.hasMoreTokens()) {
            result.append(capitalize(st.nextToken()));
        }
        return capitalizeFirstChar ? result.toString() : result.substring(0, 1).toLowerCase() + result.substring(1);
    }

    /**
     * Capitalizes a word  - only a first character is converted to upper case.
     *
     * @param word word/phrase to capitalize.
     * @return same as input argument, but the first character is capitalized.
     */
    public static String capitalize(String word) {
        return word.substring(0, 1).toUpperCase() + word.substring(1);
    }
}

class IrregularRule {
	public String singular;
	public String plural;
	public IrregularRule(String singular, String plural) {
		this.singular = singular;
		this.plural = plural;
	}
}
