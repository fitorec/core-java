package com.mundosica.core.swtf;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.printing.PrintDialog;
import org.eclipse.swt.printing.PrinterData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.ColorDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FontDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;

public class Dialog {

	public void alert(String msg, String title, Shell sh) {
		MessageBox box = new MessageBox(sh, SWT.ICON_INFORMATION | SWT.OK);
		box.setText(title);
		box.setMessage(msg);
		box.open();
	}

	public void warning(String msg, String title, Shell sh) {
		MessageBox box = new MessageBox(sh, SWT.ICON_WARNING | SWT.OK);
		box.setText(title);
		box.setMessage(msg);
		box.open();
	}

	public void error(String msg) {
		MessageBox box = new MessageBox(new Shell(), SWT.ICON_ERROR | SWT.OK);
		box.setText("Error");
		box.setMessage(msg);
		box.open();
	}

	public boolean confirm(String msg, String title, Shell sh) {
		MessageBox box = new MessageBox(sh, SWT.ICON_QUESTION | SWT.OK| SWT.CANCEL);
		box.setText(title);
		box.setMessage(msg);
		return (box.open() == SWT.OK);
	}

	public static RGB pickColor(String msg) {
		ColorDialog cd = new ColorDialog(new Shell());
		cd.setText(msg);
		cd.setRGB(new RGB(255, 255, 255));
		return cd.open();
	}

	/**
	 * Selecciona un tipo de fuente y devuelve dicha información
	 *
	 * @param title
	 * @return FontData informacion de la fuente seleccionada
	 */
	public static FontData pickFont(String title) {
		FontDialog fd = new FontDialog(new Shell(), SWT.NONE);
		fd.setText(title);
		fd.setRGB(new RGB(0, 0, 255));
		FontData newFont = fd.open();
		if (newFont == null) {
			return new FontData("Courier", 10, SWT.BOLD);
		}
		return newFont;
	}
	public static FontData pickFont() {
		return Dialog.pickFont("Seleccionar tipografia");
	}

	public static PrinterData pickPrint(String title) {
		PrintDialog printDialog = new PrintDialog(new Shell(), SWT.NONE);
        printDialog.setText(title);
        return printDialog.open();
	}
	public static PrinterData pickPrint() {
		return Dialog.pickPrint("Seleccione una impresora");
	}

	public static void main(String... arg) {
		PrinterData infoImpresora = SWTF.dialog.pickPrint();
		System.out.println(infoImpresora.name);
		
		//System.out.println(Dialog.pickFont());
		//Dialog.alert("Swt alert!", "titulo", new Shell());
		/*if( Dialog.confirm("Swt alert!", "titulo", new Shell())) {
			System.out.println("Haceptar");
		};*/
		/*Prompt p = new Prompt(new Shell());
		String f = p.open();
		System.out.println("Resultado" + f);
		new ShowFileDialog().run(); */
		//RGB color = Dialog.pickColor("Pinche maricon el edwin");
		//System.out.println(SWTResourceManager.RGB2str(color));
	}
}

/** **/
class Prompt extends  org.eclipse.swt.widgets.Dialog {
	String value = "";
	public Prompt(Shell sh) {
		super(sh, SWT.PRIMARY_MODAL);
		// TODO Auto-generated constructor stub
	}

		/**
		 * Makes the dialog visible.
		 *
		 * @return
		 */
		public String open() {
		Shell parent = getParent();
		final Shell shell = new Shell(parent, SWT.TITLE | SWT.BORDER | SWT.APPLICATION_MODAL);
		shell.setSize(740, 350);
		shell.setText("Prompt Ventana");
		shell.setLayout(new GridLayout(2, true));

		Label label = new Label(shell, SWT.NULL);
		label.setText("Por favor inserte un text:");

		final Text text = new Text(shell, SWT.SINGLE | SWT.BORDER);

		final Button buttonOK = new Button(shell, SWT.PUSH);
		buttonOK.setText("Aceptar");
		buttonOK.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_END));
		Button buttonCancel = new Button(shell, SWT.PUSH);
		buttonCancel.setText("Cancelar");

		text.addListener(SWT.Modify, new Listener() {
			public void handleEvent(Event event) {
			try {
				value = text.getText();
				buttonOK.setEnabled(true);
			} catch (Exception e) {
				buttonOK.setEnabled(false);
			}
			}
		});

		buttonOK.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event event) {
			shell.dispose();
			}
		});

		buttonCancel.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event event) {
			value = null;
			shell.dispose();
			}
		});

		shell.addListener(SWT.Traverse, new Listener() {
			public void handleEvent(Event event) {
			if(event.detail == SWT.TRAVERSE_ESCAPE)
				event.doit = false;
			}
		});

		text.setText("");
		shell.pack();
		shell.open();

		Display display = parent.getDisplay();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch())
			display.sleep();
		}
		return value;
	}

	public static void main(String[] args) {
		Shell shell = new Shell();
		Prompt dialog = new Prompt(shell);
		System.out.println(dialog.open());
	}
}

