package com.mundosica.core.swtf.dialog;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.mundosica.core.Utils.Config;
import com.mundosica.core.View.Helper;
import com.mundosica.core.View.SWTWindow;
import com.mundosica.core.swtf.SWTF;
import com.mundosica.core.swtf.SWTResourceManager;

public class LoginDialog  extends SWTWindow{
	
    private Text username;
    private Text password;
    private static final String errorMsg = "Error al intentar logearse";
    private Label lblError;
    private TryAuth tryAuth;
    private boolean allow = false;
    private Map <String, String> fields = new HashMap<String, String>();

    public LoginDialog(String...arguments) {
    	createShell();
        configuraciones(arguments);
    }

    public boolean allow() {
    	return this.allow;
    }
    public void addTryAuth(TryAuth ta) {
    	this.tryAuth = ta;
    }

    private void aceptar() {
    	String username = this.username.getText();
    	String password = this.password.getText();
    	this.allow = false;
    	if (this.tryAuth != null) {
    		allow = this.tryAuth.hook(username, password);
    	}
    	if (allow) {
    		shell.close();
    		return;
    	}
  		// Revisar este mensaje de error
    	this.lblError.setText(this.fields.get("error"));
   		this.lblError.setVisible(true);
    }
    
    private void configuraciones(String... args) {
    	if((args.length % 2) != 0 ) {
    		return;
    	}
    	fields.clear();
    	fields.put("img", "logo.png"); //en media
    	fields.put("layout", "horizontal"); //valores horizontal, vertical
    	fields.put("title", "Por favor identifiquese para ingresar"); //en media
    	fields.put("error", "Revise sus datos"); //en media
    	for (int i = 0; i < args.length -1 ; i +=2 ) {
    		if (args[i] != null && args[i+1] != null) {
    			this.fields.put(args[i],args[i+1] );
    		}
    	}
    }

    public void createContents() {
        this.shell.setText(this.fields.get("title"));
        
        Label imagen = new Label(this.shell, SWT.None);
        String path = Config.mediaPath() + fields.get("img");
        System.out.println(path);
        imagen.setImage(SWTResourceManager.getImage(path));
        Group grpDatosEmpleado = Helper.initGroupIzquierdo(this.shell, "Identifiquese", 150, 150);

        lblError = new Label(grpDatosEmpleado,SWT.NONE);
        lblError.setAlignment(SWT.LEFT);
        lblError.setBackground(SWTResourceManager.getColor(255, 205, 205));
        lblError.setFont(SWTResourceManager.getFont("tahoma", 12, 30));
        lblError.setVisible(false);
       
        Label iconUser = SWTF.labelIcon(grpDatosEmpleado,
        		"icon", 	"user",
             	"top", 		"45px",
             	"left", 	"15px",
             	"width", 	"30px"
         );
        Label nicname = SWTF.label(grpDatosEmpleado,
        		"text", "Nickname:",
        		"font-size", "12px",
             	"top", 		"45px",
             	"left", 	"45px",
             	"width", 	"100px",
             	"height",	"15px"
         );
        this.username = Helper.newText(grpDatosEmpleado);
        this.username.setFocus();

        Label iconPass = SWTF.labelIcon(grpDatosEmpleado,
        		"icon", 	"lock",
             	"top", 		"95px",
             	"left", 	"15px",
             	"width", 	"30px"
         );
        Label pass = SWTF.label(grpDatosEmpleado,
        		"text", "Password:",
        		"font-size", "12px",
             	"top", 		"95px",
             	"left", 	"45px",
             	"width", 	"100px",
             	"height",	"15px"
         );
        
        this.password = new Text(grpDatosEmpleado, SWT.PASSWORD | SWT.BORDER);
        this.password.addFocusListener(Helper.textFocuStyle());
        this.password.setFont(SWTResourceManager.getFont("Sans", 12, SWT.NORMAL));
        this.password.addKeyListener(new KeyAdapter() {
            public void keyReleased(KeyEvent ek) {
                if (ek.keyCode == SWT.CR) {
                	aceptar();
                }
            }
        });
        Button btnAceptar = SWTF.btn(grpDatosEmpleado,
        	"text", 	"✓ &Acceder",
          	"tooltip", 	"Ingresar Al sistema",
          	"font-size","15px",
         	"top", 		"30px",
         	"left", 	"350px",
         	"width", 	"250px",
         	"height", 	"40px"
        );
        btnAceptar.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent event) {
            	aceptar();
            }
        });

        Button btnCancelar =SWTF.btn(grpDatosEmpleado,
            	"text", 	"✘ &Cancelar",
              	"tooltip", 	"Ingresar Al sistema",
              	"font-size","15px",
             	"top", 		"90px",
             	"left", 	"350px",
             	"width", 	"250px",
             	"height", 	"40px"
            );
        btnCancelar.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent event) {
                shell.close();
            }
        });
        
        this.shell.setSize(740, 350);
        imagen.setBounds(10,10,710,130);
        lblError.setBounds(100, 10, 500, 17);
        this.username.setBounds(145,39,170,30);
        this.password.setBounds(145,89,170,30);
        
        if(this.fields.get("layout").equals("vertical")) {
        	this.shell.setSize(450, 540);
            imagen.setBounds(10,10,420,200);
            grpDatosEmpleado.setBounds(10,270,430,200);
            lblError.setBounds(10, 20, 410, 17);
            iconUser.setBounds(10,50,30,30);
            nicname.setBounds(50,50,130,30);
            this.username.setBounds(200,50,170,30);
            iconPass.setBounds(10,90,30,30);
            pass.setBounds(50,90,130,30);
            this.password.setBounds(200,90,170,30);
            btnAceptar.setBounds(10, 130, 200, 40);
            btnCancelar.setBounds(220,130,200,40);
    	}
    }
    
    public void open() {
    	this.startWindow(false);
    }
}

