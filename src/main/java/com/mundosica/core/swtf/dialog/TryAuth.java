package com.mundosica.core.swtf.dialog;

public abstract class TryAuth {
	public abstract boolean hook(String username, String password);
}
