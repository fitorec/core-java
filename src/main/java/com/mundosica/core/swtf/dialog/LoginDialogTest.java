package com.mundosica.core.swtf.dialog;

import static org.junit.Assert.*;

import org.eclipse.swt.widgets.Shell;
import org.junit.Before;
import org.junit.Test;

import com.mundosica.core.Utils.Config;

public class LoginDialogTest {
	@Before
	public void before() {
		Config.rootPath("linux", "/opt/cruvi");
	}

	@Test
	public void test() {
		
		LoginDialog loginWindows = new LoginDialog(
				"img", "logo.png", //en media
				//"layout", "vertical", //valores horizontal, vertical
				"title", "Bienvenido sucursal esmeralda", //en media
				"error", "Revise sus datos" //en media
		);
		loginWindows.addTryAuth(new TryAuth() {
			@Override
			public boolean hook(String username, String password) {
				if(username.equals("fitorec") && password.equals("papi chuy")) {
					return true;
				}
				return false;
			}
		} );
		 loginWindows.open();
		 if (loginWindows.allow()) {
			 System.out.println("login correcto");
		 } else {
			 System.out.println("Error");
		 }
	}

}
