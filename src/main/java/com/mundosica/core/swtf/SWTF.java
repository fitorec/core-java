package com.mundosica.core.swtf;


import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;

import com.mundosica.core.View.Helper;

public class SWTF {
	public static com.mundosica.core.swtf.Dialog dialog = new Dialog();
	/**
	 * <strong>btnIcon</strong> genera un swt.widgets.Button con una iconografía FontAwosome.
	 * 
	 * 
	 * Ejemplo:
	 * <pre>
	 * <code>
	 * Button btnDemo = SWTF.btnIcon(grupo,
	 * 	"icon", "play",
	 * 	"tooltip", "play rigth now"
	 * 	"font-size", "15px"
	 * 	"top", "30px"
	 * 	"left", "30px"
	 * 	"width", "30px"
	 * );
	 * </code>
	 * </pre>
	 * 
	 * @param gruop the Group of SWT
	 * @param args the arguments, check the example
	 * @return Button a btn result
	 */
	public static Button btnIcon(Group gruop, String... params) {
		Button btn = SWTF.btn(gruop, params);
        //Icon set icon
		int width = intronspectWithDefault("width", params, 30);
        int fontSize = intronspectWithDefault("font-size", params, width / 2);
        String icon = intronspectWithDefault("icon", params, "play");
     	btn.setFont(FontAwosome.font(fontSize));
     	btn.setText(FontAwosome.get(icon)  + "");
     	return btn;
	}
	
	public static Label labelIcon(Group group, String... params) {
		Label label = SWTF.label(group, params);
		//position
		int width = intronspectWithDefault("width", params, 30);
		int fontSize = intronspectWithDefault("font-size", params, width / 2);
        String icon = intronspectWithDefault("icon", params, "play");
     	label.setFont(FontAwosome.font(fontSize));
     	label.setText(FontAwosome.get(icon)  + "");
     	return label;
	}
	public static Label label(Group group, String... params) {
		Label label = new Label(group, SWT.NONE);
		//position
		int top = intronspectWithDefault("top", params, 5);
		int left = intronspectWithDefault("left", params, 5);
		int width = intronspectWithDefault("width", params, 30);
		int height = intronspectWithDefault("height", params, width);
		label.setBounds(left, top, width, height);
		//Text
        int fontSize = intronspectWithDefault("font-size", params, height / 2);
        String fontFace = intronspectWithDefault("font-family", params, "Sans");
        label.setFont(SWTResourceManager.getFont(fontFace, fontSize, SWT.NORMAL));
        String text = intronspectWithDefault("text", params, "");
        label.setText(text);
     	return label;
	}

	/**
	 * Arguments example:
	 * <pre>
	 * Button btnDemo = SWTF.btn(
	 * 		grupo,
	 * 		"icon", "play",
	 *  	"tooltip", "play rigth now"
	 *  	"font-size", "15px"
	 * 		"top", "30px"
	 * 		"left", "30px"
	 * 		"width", "30px"
	 * );
	 * </pre>
	 * @param gruop the Group of SWT
	 * @param args the arguments, check the example
	 * @return Button a btn result
	 */
	public static Button btn(Group gruop, String... params) {
			if (params.length % 2 == 1) {
				throw new IllegalArgumentException("The number of arguments must be pair.");
			}
			Button btn = new Button(gruop, SWT.NONE);
			// tooltip
			String tooltip = SWTF.intronspectWithDefault("tooltip", params, "");
			btn.setToolTipText(tooltip);
			//position
			int top = intronspectWithDefault("top", params, 5);
			int left = intronspectWithDefault("left", params, 5);
			int width = intronspectWithDefault("width", params, 30);
			int height = intronspectWithDefault("height", params, width);
	        btn.setBounds(left, top, width, height);
	        //Text
	        int fontSize = intronspectWithDefault("font-size", params, height / 2);
	        String fontFace = intronspectWithDefault("font-family", params, "Sans");
	        btn.setFont(SWTResourceManager.getFont(fontFace, fontSize, SWT.NORMAL));
	        String text = intronspectWithDefault("text", params, "");
	        btn.setText(text);
	        return btn;
	}

	/**
	 * Convert str a un number, p.e. 30px to 30
	 * @param str
	 */
	public static Integer convertToInt(String str) {
		return Helper.str2Int(str.toLowerCase().replaceAll("px", ""));
	}
	
	/**
	 * 
	 */
	public static String intronspect(String element, String params[]) {
		if (params.length % 2 == 1) {
			throw new IllegalArgumentException("The number of arguments must be pair.");
		}
		for (int i = 0; i < params.length - 1; i += 2) {
			if (params[i].equalsIgnoreCase(element)) {
				return params[i + 1];
			}
		}
		return null;
	}

	/**
	 * 
	 */
	public static String intronspectWithDefault(String element, String params[], String defaultStr) {
		String result = intronspect(element, params);
		if(result != null) {
			return result;
		}
		return defaultStr;
	}
	/**
	 * 
	 */
	public static Integer intronspectWithDefault(String element, String params[], Integer defaultStr) {
		String result = intronspect(element, params);
		if(result != null) {
			return convertToInt(result);
		}
		return defaultStr;
	}
	
	public Integer[] rgbaHexa2Int(String hexaCode) {
		hexaCode = hexaCode.replaceAll("#", "");
		if (hexaCode.length() != 6) {
			return null;
		}
		Integer[] intData = {
				Integer.parseInt(hexaCode.substring(0,1), 16),
				Integer.parseInt(hexaCode.substring(2,3), 16),
				Integer.parseInt(hexaCode.substring(4,5), 16)
		};
		return intData;
	}
}
