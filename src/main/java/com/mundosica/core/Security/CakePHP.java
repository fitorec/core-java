package com.mundosica.core.Security;


public class CakePHP {
	private static String security_salt = null;

	/*
	 * 
	 * @link http://book.cakephp.org/2.0/en/core-utility-libraries/security.html#Security::hash
	 */
	public static String password(String txt) {
		return Hash.sha1(CakePHP.security_salt + txt);
	}
	public static String securitySalt(String salt) {
		CakePHP.security_salt = salt;
		return CakePHP.security_salt;
	}
}
