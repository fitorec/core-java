package com.mundosica.core.Network;

import java.util.HashMap;
import java.util.Vector;

public class Host {
	
	public String red = null;
	public Vector<String> ip = new Vector<String> ();
	
	//para los sistemas Linux
	public HashMap<String,String> ethX = new HashMap<String,String>();
	public HashMap<String,String> wlanX = new HashMap<String,String>();
	
	@Override
	public String toString() {
		return "eth"+ethX.toString() + "\nwlan" + wlanX.toString();
	}
	
	public String findIpByMac(String mac) {
		//regresa la primera ip que encuentre que coinsida con la mac
		String ip = ethX.get(mac);
		return ip == null ? wlanX.get(mac):ip;
	}
	
	public String findIpByMac(String mac,String interfaz) {
		if(interfaz.equals("eth")) {
			return ethX.get(mac);
		}
		if(interfaz.equals("wlan")) {
			return wlanX.get(mac);
		}
		
		return null;
	}
	
	public String findMacByIp() {
		return null;
	}
	
	public void red(String ip) {
		if(red == null) {
			String octeto [] = ip.split("\\.");
			red = octeto[0] + "."+ octeto[1] + "." + octeto[2] + ".";
		}
	}
	public String getIp(int index) {
		if(ip.isEmpty()) {
			return "127.0.0.1";
		}
		return ip.get(index);
	}
}
