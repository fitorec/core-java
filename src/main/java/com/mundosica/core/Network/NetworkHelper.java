package com.mundosica.core.Network;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.mundosica.core.Utils.Config;

public class NetworkHelper {
	
	boolean detenerScann = false;
	
	public NetworkHelper() {
	}
	
	/*Buscara en la red la ip asociada con la mac y devolvera el resultado inmediatamente*/
	public String getIpByMac2(String mac) {
		detenerScann = false;
		Host local = this.localHost();
		if(local == null) {
			return null;
		}
		for(int i = 0; i < 256 && !detenerScann; i++) {
			Host h = hostRemote(local.red + i);
			try {
				if(h != null && h.ethX.get(mac) != null) {
					return h.ethX.get(mac);
				}
			} catch (Exception e) { }
			
		}
		//////*/
		return null;
	}
	
	/*Buscara en la red la ip asociada con la mac*/
	public String getIpByMac(String mac) {
		Vector<Host> hosts = netScanner();
		String ip = null;
		for(int i = 0; i < hosts.size(); i++) {
			Host host =  hosts.get(i);
			ip = host.findIpByMac(mac);
			//System.out.println("host: " + host);
			//System.out.println("ip: " + ip);
			if(ip != null) {
				return ip;
			}
		}
		System.out.println("ip no encontrada");
		return null;
	}
	public String getMacByIp() {
		return null;
	}
	
	public Vector<Host> netScanner() {
		detenerScann = false;
		Vector<Host> hosts = new Vector<Host>();
		Host local = this.localHost();
		if(local == null) {
			return hosts;
		}
		for(int i = 0; i < 256 && !detenerScann; i++) {
			Host h = hostRemote(local.red + i);
			if(h != null) {
				hosts.add(h);
			}
		}
		//////*/
		return hosts;
	}
	
	public void detenerScann() {
		this.detenerScann = true;
	}
	
	/*regresa la  mac de un host que este en la misma red*/
	public Host hostRemote(String ip) {
		System.out.println(ip);
		Host host = null;
		try {
			if(!ping(ip)) {
				return null;
			}
			/////////*/
			Process p = Runtime.getRuntime().exec("arp -a " + ip);
			p.waitFor();
			BufferedReader reader=new BufferedReader(new InputStreamReader(p.getInputStream()));
			String linea = reader.readLine();
			while (linea != null ) {
				String mac = buscarMacEnString(linea);
				if(mac != null) {
					if(host == null) {
						host = new Host();
					}
					host.ethX.put(mac, ip);
					host.red(ip);
				}
				linea = reader.readLine();
			}
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		return host;
	}

	/*
	 * regresa la ip en la red local de la maquina que ejecute esta app
	 */
	public Host localHost() {
		if(Config.os().equals("windows")) {
			return this.ipconfig();
		} else {
			return this.ifconfig();
		}
	}
	
	public Host ipconfig() {
		Host host = new Host();
		try {
			Process p=Runtime.getRuntime().exec("cmd /c ipconfig");
			p.waitFor();
			BufferedReader reader=new BufferedReader(new InputStreamReader(p.getInputStream()));
			//System.out.println(reader.readLine());
			String linea = reader.readLine();
			while (linea != null ) {
				if(siEsta(".*IPv4\\.",linea)) { 
					String ip = buscarIpEnString(linea);
					//host.ethX.put("undefined", ip);
					host.red(ip);
					host.ip.add(ip);
				}
				////*/
				linea = reader.readLine();
			}
			System.out.println(host.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return host;
	}
	public Host ifconfig() {
		Host host = new Host();
		try {
			//Process p=Runtime.getRuntime().exec("cmd /c dir");
			Process p=Runtime.getRuntime().exec("ifconfig");
			p.waitFor();
			BufferedReader reader=new BufferedReader(new InputStreamReader(p.getInputStream()));
			//System.out.println(reader.readLine());
			String linea = reader.readLine();
			while (linea != null ) {
				/*en la primera linea esta la mac en ifconfig*/
				/*en la segunda linea estan la ip,difu,masks*/
				/*
				 Ejemplo:
				 wlan0     Link encap:Ethernet  direcciónHW c4:6e:1f:16:fc:3d
				 		   Direc. inet:192.168.0.16  Difus.:192.168.0.255  Másc:255.255.255.0
				*/
				String [] co = linea.split(" ");
				if(co[0].length()>=3 && co[0].substring(0, 3).equals("eth")) {
					String mac = buscarMacEnString(linea);
					linea = reader.readLine();// leemos la segunda linea
					String ip = buscarIpEnString(linea);
					if(ip != null) {
						host.ethX.put(mac, ip);
						host.red(ip);
						host.ip.add(ip);
					}
				} 
				if(co[0].length()>=4 && co[0].substring(0, 4).equals("wlan")) {
					String mac = buscarMacEnString(linea);
					linea = reader.readLine();// leemos la segunda linea
					String ip = buscarIpEnString(linea);
					if(ip != null) {
						host.wlanX.put(mac, ip);
						host.red(ip);
						host.ip.add(ip);
					}
				}
				linea = reader.readLine();
			}
			//System.out.println(host.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return host;
	}
	public String buscarMacEnString(String linea) {
		linea = linea.replaceAll("-", ":");
		Pattern pattern = Pattern.compile(".{2}:.{2}:.{2}:.{2}:.{2}:.{2}");
		Matcher matcher = pattern.matcher(linea);
		return matcher.find()?matcher.group(0):null;
	}
	/*regresara la primera ip que encuetre*/
	public String buscarIpEnString(String linea) {
		//System.out.println(linea);
		String regex = "((1|2)?[1-9]?[0-9]+\\.){3}((1|2)?[1-9]?[0-9]+)";
		Pattern pattern = 
				Pattern.compile(regex);
		Matcher matcher = pattern.matcher(linea);
		return matcher.find()?matcher.group(0):null;
	}
	
	public boolean siEsta(String regex,String string) {
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(string);
		return matcher.find();
	}
	
	public  boolean ping( String ip) {
		String so = Config.os();
		String ping = so.equals("windows")?
				"cmd /c ping -w 100 -n 1 ":"ping -c 1 -n -i 0.2 -W1 ";
		try {
			Process p=Runtime.getRuntime().exec(ping + ip);
			p.waitFor();
			
			BufferedReader reader=new BufferedReader(new InputStreamReader(p.getInputStream()));
			String linea = reader.readLine();
			////*
			String error = so.equals("windows")?"100% perdido":"0 received,";
			//String error ="(100% perdidos)";
			while (linea != null ) {	
				if(siEsta(error,linea)) {		
					return false;	
				} 
				linea = reader.readLine();
			}
			return true;
		} catch(Exception e) {
			e.printStackTrace();
		}
		return false;
	}
}
