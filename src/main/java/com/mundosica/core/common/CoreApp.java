package com.mundosica.core.common;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import com.mundosica.core.Model.DataBase;
import com.mundosica.core.Utils.TaskManager;

public class CoreApp {
	public CoreApp() {
		this._start();
		this._run();
		this._end();
	}

	/**
	 * Metodo de arranque de la aplicación
	 * 
	 * Se encarga de mandar a llamar al metodo start de la aplicacion
	 */
	public void _start() {
		System.out.println("CoreApp.__start()");
		this.execMethod("start");
	}

	/**
	 * Metodo run de la aplicación
	 * 
	 * Se encarga de mandar a llamar al metodo run de la aplicacion
	 */
	public void _run() {
		System.out.println("CoreApp.__run()");
		this.execMethod("run");
	}

	/**
	 * Metodo que finaliza la aplicación
	 * 
	 * Se encarga de finalizar los hilos, la base de datos y mandar a llamar al metodo
	 * end de la aplicacion
	 */
	public void _end() {
		System.out.println("CoreApp.__end()");
		TaskManager.end();
		DataBase.end();
		Thread.currentThread().interrupt();
		this.execMethod("end");
	}

	/**
	 * Ejecuta un metodo sin argumentos a partir de su nombre.
	 * 
	 * @param methodName el nombre del metodo a ejecutar
	 */
	public void execMethod(String methodName) {
		Method methodFinded;
		try {
			methodFinded = this.getClass().getMethod(methodName);
			if (methodFinded != null) {
				try {
					methodFinded.invoke(this);
				} catch (IllegalAccessException | IllegalArgumentException
						| InvocationTargetException e) {
					e.printStackTrace();
				}
			}
		} catch (NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
		}
	}
}
