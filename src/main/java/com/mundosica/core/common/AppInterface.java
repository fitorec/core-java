package com.mundosica.core.common;

public interface AppInterface {
	public void start();
	public void run();
	public void end();
}
