package com.mundosica.core.Model;

import com.mundosica.core.common.Util;
import com.mundosica.core.Network.NetworkHelper;
import com.mundosica.core.Utils.Config;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Administra la conexión en la base de datos.
 *
 * @author fitorec &lt;chenerec@gmail.com&gt;
 */
public class DataBase {
/**
 * El <b>host</b> de la conexión
 */
	private static String host = "localhost";

/**
 * El <b>puerto</b> en el que se establece la conexión
 */
	public static String port = "3306";

/**
 * El nombre de la base de datos
 */
	private static String bd   = "bdName";

/**
 * El nombre del <b>usuario</b> para la conexión
 */
	private static String user = "cruvi_user";

/**
 * El <b>password</b> del usuario
 */
	private static String password = "cruvi_pass";

/**
 * La conexión.
 */
	private static Connection conexion;

/**
 * Setea una variable de configuración
 *
 * @param fieldName el campo de configuración
 * @param value el valor a setearle
 * @return el valor final de campo
 */
	public static String config(String... namesAndValues) {
		if (namesAndValues.length == 1) {
			return DataBase.configValue(namesAndValues[0]);
		}
		if (namesAndValues.length % 2 == 1) {
			throw new IllegalArgumentException("The number of arguments must be pair.");
		}
		for (int i = 0; i < namesAndValues.length - 1; i += 2) {
			if (namesAndValues[i] != null && namesAndValues[i + 1] != null) {
				DataBase.setConfig(namesAndValues[i].toString(), namesAndValues[i + 1]);
			}
		}
		return Util.join(namesAndValues, ",");
	}

/**
 * Devuelve la variable de configuración con el nombre campo
 *
 * @param fieldName el nombre del campo
 * @return El valor del campo en caso de ser null devuelve un valor por defecto
 */
	private static String configValue(String fieldName) {
		switch (fieldName.toLowerCase()) {
			case "host":
				return DataBase.host;
			case "port":
				return DataBase.port;
			case "bd":
				return DataBase.bd;
			case "user":
				return DataBase.user;
			case "password":
				return DataBase.password;
			default:
				System.out.println("Campo incorrecto de configuración : " + fieldName);
				return fieldName;
		}
	}

	private static String setConfig(String fieldName, String value) {
		switch (fieldName.toLowerCase()) {
			case "host":
				DataBase.host = value;
				break;
			case "port":
				DataBase.port = value;
				break;
			case "bd":
				DataBase.bd = value;
				break;
			case "user":
				DataBase.user = value;
				break;
			case "password":
				DataBase.password = value;
		}
		return DataBase.config(fieldName);
	}

/**
 * Inicializa la conexión en caso que se encuentre cerrada
 *
 */
	public static boolean initBD() {
		if (DataBase.conexion == null ) {
			try {
				DriverManager.registerDriver(new org.gjt.mm.mysql.Driver());
				DataBase.conexion = DriverManager.getConnection (
					DataBase.url(),
					user,
					password
				);
				return true;
			} catch (Exception e) {
				System.out.println("No se pudo conectar revise sus permisos, estado de la conexión: ");
				System.out.println(DataBase.status());
				e.printStackTrace();
				return false;
			}
		}
		return true;
	}

/**
 * Regresa la URL para la conexión
 **/
	public static String url() {
		return "jdbc:mysql://" + DataBase.host + ":" + DataBase.port + "/" + DataBase.bd;
	}

/**
 * Regresa el conexión.
 *
 * @return la conexión
 */
	public static Connection conexion() {
		DataBase.initBD();
		return DataBase.conexion;
	}

/**
 * Regresa un String que nos muestra el objeto BD
 * y el estado de la conexión que maneja
 *
 * @return status un string que contiene el edo de la conexión
 */
	public static String status() {
		String out =
			"host     : " + DataBase.config("host") +  "\n" +
			"port     : " + DataBase.config("port") +  "\n" +
			"Password : " + DataBase.config("password").replaceAll(".", "*") +  "\n" +
			"user     : " + DataBase.config("user") +  "\n" +
			"bd	      : " + DataBase.config("bd") +  "\n" +
			"URL      : " + DataBase.url()  +  "\n\n";
		if (DataBase.conexion != null ) {
			out += "Conexión abierta";
		} else {
			out += "Conexión cerrada";
		}
		return out;
	}

/**
 * Cierra la conexión en la base de datos
 *
 **/
	public static void end() {
		if (DataBase.conexion != null ) {
			try {
				DataBase.conexion.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}
