package com.mundosica.core.Model;

import com.mundosica.core.Http.Request;
import com.mundosica.core.Utils.Inflector;
import java.lang.reflect.Field;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Hashtable;
import java.util.Vector;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.swt.widgets.DateTime;
import org.json.JSONArray;
import org.json.JSONObject;
import static com.mundosica.core.common.Util.*;

/**
 * <b>Model</b> Clase abstracta para administrar los modelos.
 *
 */
public abstract class Model extends CallBacksSupport {
	/**
	 * Control de Transacciones
	 */
	public boolean transaccionActiva = false;

	/**
	 * Objeto para realizar acciones en grupo.
	 */
	public Collector collector;

	/**
	 * la paginación
	 *
	 */
	protected int pagine = 1;

	/** El nombre de la tabla. */
	protected String table_name = null;
	/**
	 * Bandera auto created setea los campos que se llamen created y son del tipo datetime
	 * si se encuentra activa
	 */
	public boolean auto_created = true;
	/**
	 * Bandera auto created setea los campos que se llamen created y son del tipo datetime
	 * si se encuentra activa
	 */
	public boolean auto_modified = true;

	/**
	 * Bandera que nos permite llevar el modo debug, imprimiendo las consultas realizadas.
	 */
	protected boolean debug_mode = false;

	/** The limit. */
	protected int limit = 30;

	//Vector para almacenar los valores de las columnas.
	protected Hashtable<String, String> values = new Hashtable<String, String>();

	/** Vector que contiene los errores */
	private Vector<String> errors = new Vector<String>();
	/** Un indice para manejar los errores */
	private int error_index = 0;

	/** Agregando un orden **/
	protected String order = null;

	/**
	 * Constructor simple
	 */
	public Model() {
		BDTables.addIfNotExits(this.tableName());
	}


	/**
	 * Inicializa un nuevo Modelo.
	 *
	 * @param primaryKeyValue el valor de la clave primarya
	 */
	public Model(String primaryKeyValue) {
	   BDTables.addIfNotExits(this.tableName());
	   this.load(primaryKeyValue);
	}

	/**
	 * Construye un nuevo Modelo vacio
	 *
	 * @param id el identificador del registro
	 */
	@Deprecated
	public Model(int id) { BDTables.addIfNotExits(this.tableName()); }

	/**
	 * Devuelve el nombre de la tabla correspondiente al nombre de la clase
	 *
	 * @return table_name el nombre de la tabla correpondiente al modelo
	 */
	public String tableName() {
		if (this.table_name == null) {
			this.table_name = this.getClass().getSimpleName().replaceAll("Model$", "");
			this.table_name = Inflector.plural(this.table_name.toLowerCase());
		}
		return this.table_name;
	}

	/**
	 * Obtiene los valores para el Primaky_key
	 *
	 * @return un string que contiene el nombre del campo primary_key
	 */
	public String primaryKey() {
		/*String primary_key_name = DefaultModel.primary_key;
		try {
			Field primary_key = this.getClass().getDeclaredField("primary_key");
			try {
				primary_key.setAccessible(true);
				primary_key_name = (String)primary_key.get(this);
			} catch (IllegalArgumentException | IllegalAccessException e) {
			}
		} catch (NoSuchFieldException noExisteField) { // En caso que no exista el campo
		} catch (SecurityException e1) {
		}*/
		return BDTables.info(this.tableName()).primaryKey();
	}

	/**
	 * Append error.
	 *
	 * @param error Un string que describe el error
	 */
	public void appendError(String error) {
		this.errors.add(error);
	}

	/**
	 * use count()
	 *
	 * @return num_registros el numero de registros
	 */
	public int numRegistros() {
		return this.numRegistros("");
	}

	/**
	 * use count(String conditions)
	 *
	 * @param conditions las condiciones que debera de cumplir.
	 * @return el número de registros existentes en la tabla
	 */
	@Deprecated
	public int numRegistros(String conditions) {
		return this.count(conditions);
	}

	private void debug(String msg) {
		System.out.println(msg);
	}
	/**
	 * Regresa el número de registros existentes en la tabla.
	 *
	 * @param conditions las condiciones que deberá de cumplir.
	 * @return counter el numero de registros
	 */
	public int count(String conditions) {
		String query = "SELECT COUNT(`"+this.primaryKey()+"`) as counter"
				+ " FROM `" + this.tableName() + "`"
				+ " WHERE ";
		if (conditions != null && !conditions.equals("")) {
			query += conditions ;
		} else {
			query += "1";
		}
		if (this.debug_mode) {
			this.debug("count: " + query);
		}
		int counter = 0;
		try {
			ResultSet rs = QueryManager.select(query);
			if (rs.next()) {
				counter = rs.getInt("counter");
			}
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
			this.appendError("Error en Core.Model.count sobre la consulta: \n" + query);
		}
		return counter;
	}

	/**
	 * Regresa el número de registros existentes en la tabla.
	 *
	 * @return counter el numero de registros
	 */
	public int count() {
		return this.count(null);
	}

	/**
	 * Errors.
	 *
	 * @return the vector
	 */
	@Deprecated
	public Vector<String> errors() {
		return this.errors;
	}

	/**
	 * Devuelve el siguiente error.
	 *
	 * @return the string
	 */
	@Deprecated
	public String nextError() {
		if ((this.error_index+1) >= this.errors.size()) {
			return null;
		}
		this.error_index++;
		return this.errors.get(this.error_index);
	}

	/**
	 * Devuelve el valor actual.
	 *
	 * @return the string
	 */
	@Deprecated
	public String currentError() {
		if (this.error_index >= this.errors.size()) {
			return null;
		}
		return this.errors.get(this.error_index);
	}

	/**
	 * Imprime los errores.
	 */
	@Deprecated
	public void printErrors() {
		for ( int i = 0, n_errors = this.errors.size(); i<n_errors; i++) {
			System.out.println(this.errors.get(i));
		}
	}

	/**
	 * Regresa el valor del id,  use: this.get("id")
	 *
	 * @return the id
	 */
	@Deprecated
	public int id() {
		String value = this.get("id");
		if (value != null) {
			return Integer.parseInt(value);
		}
		return -1;
	}

	/**
	 * Cambia el campo id, siempre y cuando sea mayor a cero, use use: this.set("id", valor)
	 *
	 * @param newId el valor del nuevo identificador
	 * @return id el nuevo valor del idmodificado
	 */
	@Deprecated
	public int id(int newId) {
		if (newId > 0) {
			this.set("id", newId+"");
			return newId;
		}
		return -1;
	}

	//funciones limit
	/**
	 * Regresa el limit.
	 *
	 * @return the limit
	 */
	public int limit() {
		return this.limit;
	}

	/**
	 * Cambia el campo limit.
	 *
	 * @param limit the new limit
	 * @return el limite utilizado en las busquedas SQL
	 */
	public int limit(int limit) {
		this.limit = limit;
		return this.limit();
	}

	/**
	 * Cambia el campo un limit.
	 */
	public void unLimit() {
		this.limit = 1000000;
	}

	/**
	 * Esta función ya no deveria de existir
	 *
	 * @param primaryKeyValue El valor de la clave primaria
	 * @return true si existe false en caso contrario.
	 */
	@Deprecated
	public Boolean existe(Integer primaryKeyValue) {
		return this.exists(primaryKeyValue+"");
	}

	/**
	 * Regresa true, si existe algún registro con este Identificador.
	 *
	 * @param primaryKeyValue the id
	 * @return the boolean
	 */
	public Boolean exists(String primaryKeyValue) {
		return this.existByFieldValue(this.primaryKey(), primaryKeyValue);
	}

	/**
	 * Devuelve true si existe algun registro.
	 *
	 * @return the boolean Regresa true, si existe algún registro con este Identificador.
	 */
	public Boolean exists() {
		return this.existByFieldValue(this.primaryKey(), this.get(this.primaryKey()));
	}

	/**
	 * Devuelve true o false en caso que exista un registro con un campo con determinado valor.
	 *
	 * @param fieldName El nombre del campo a buscar p.e. id, curp, etc..
	 * @param fieldValue El valor conrrespondiente a dicho campo
	 * @return exists true si existe, false en caso contrario
	 */
	public Boolean existByFieldValue(String fieldName, String fieldValue) {
		String query  = "SELECT `" + fieldName + "`"
				+ " FROM  " + this.tableName()
				+ " WHERE " + fieldName;
		if (fieldValue == null) {
			query += " IS NULL";
		} else {
			query += " = '"+ fieldValue+"'";
		}
		query += " LIMIT 1;";
		if (this.debug_mode) {
			this.debug("existByFieldValue: " + query);
		}
		try {
			ResultSet rs = QueryManager.select(query);
			boolean result = rs.first();
			close(rs);
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			this.appendError("No se pudo ejecutar existosamente la consulta \n" + query);
			return false;
		}
	}

	/**
	 * Devuelve un entero con el identificador de la ultima insercción ultima insercion.
	 * <p><b>Preacaución:</b> Esta función requiere que la clave primaria sea una columna del tipo entero</p>
	 *
	 * @return the integer
	 */
	public Integer ultimaInsercion() {
		String query  = "SELECT MAX("+this.primaryKey()+") as max_id"
					+ " FROM " + this.tableName();
		if (this.debug_mode) {
			this.debug("ultimaInsercion: " + query);
		}
		try {
			ResultSet rs = QueryManager.select(query);
			if ( rs.first() ) {
				int result = rs.getInt("max_id");
				close(rs);
				return result;
			} else {
				this.appendError("No se pudo ejecutar existosamente la consulta \n" + query);
				return (Integer)(-1);
			}
		} catch (Exception e) {
			e.printStackTrace();
			this.appendError("No se pudo ejecutar existosamente la consulta \n" + query);
			return (Integer)0;
		}
	}

	/**
	 * Borrar use this.delete(primaryKeyValue);
	 *
	 * @param id the id
	 * @return the boolean
	 */
	@Deprecated
	public Boolean borrar(int id) {
		return this.deleted(""+ id);
	}

	/**
	 * Borrar.
	 *
	 * @return the boolean
	 */
	public Boolean borrar() {
		return this.borrar(this.id());
	}

	/**
	 * Lee un registro que coincida con clave primaria.
	 *
	 * @param primaryKeyValue el valor de la clave primaria
	 * @return the result set
	 */
	public ResultSet read(String primaryKeyValue) {
		this.set(this.primaryKey(), primaryKeyValue);
		return this.read();
	}

	/**
	 * Read por defecto, se encarga de realizar un <i>SELECT</i> generico para la lectura de un registro a la tabla correspondiente.
	 *
	 * @return rs un ResultSet que contiene el resultado del <i>SELECT</i>
	 */
	public ResultSet read() {
		String query = "SELECT *"
				+ " FROM " + this.tableName()
				+ " WHERE `" + this.primaryKey() + "` = '" + this.get(this.primaryKey()) +"'"
				+ " LIMIT 1";
		if (this.debug_mode) {
			this.debug("read: " + query);
		}
		return QueryManager.select(query);
	}

	/**
	 * Load vacio carga un registro donde empate su ID
	 *
	 * @return success, true si lo pudo cargar, false en caso contrario
	 */
	public Boolean load() {
		ResultSet rs = this.read();
		return this.load(rs);
	}

	/**
	 * LoadByField Se encarga de cargar un registro a partir de un campo especifico.
	 *
	 * @param fieldName el nombre del campo en cuestión
	 * @return success true si lo pudo cargar de forma correcta, false en caso contrario
	 */
	public Boolean loadByField(String fieldName) {
		return this.loadByField(fieldName, this.get(fieldName));
	}

	/**
	 * LoadByField Se encarga de cargar un registro a partir de un campo especifico.
	 *
	 * @param fieldName el nombre del campo en cuestión
	 * @param fieldValue el valor del campo en cuestión
	 * @return success true si lo pudo cargar de forma correcta, false en caso contrario
	 */
	public Boolean loadByField(String fieldName, String fieldValue) {
		String query  = "SELECT `" + fieldName + "`"
				+ " FROM  " + this.tableName()
				+ " WHERE " + fieldName;
		if (fieldValue == null) {
			query += " IS NULL";
		} else {
			query += " = '"+ fieldValue+"'";
		}
		query += " LIMIT 1;";
		ResultSet rs =  QueryManager.select(query);
		return this.load(rs);
	}

	/**
	 * Load con ID, setea el ID y carga un registro donde empate su ID
	 *
	 * @param primaryKeyValue el valor de la clave primaria
	 * @return success, true si lo pudo cargar, false en caso contrario
	 */
	public Boolean load(String primaryKeyValue) {
		if(this.debug_mode) {
			System.out.println("Setting primary_key (" + this.primaryKey() + ") = " + primaryKeyValue);
		}
		this.set(this.primaryKey(), primaryKeyValue);
		return this.load();
	}

	/**
	 * Load con query, ejecuta el `query` del tipo select y carga el 1er registro resultante
	 *
	 * @param query un String que contiene la consulta
	 * @return success, true si lo pudo cargar, false en caso contrario
	 */
	public Boolean loadByQuery(String query) {
		ResultSet rs =  QueryManager.select(query);
		return this.load(rs);
	}

	/**
	 * Load con ResultSet, carga el 1er registro contenido en ResultSet.
	 *
	 * @param rs ResultSet a cargar
	 * @return success, true si lo pudo cargar, false en caso contrario
	 */
	public Boolean load(ResultSet rs) {
		boolean result = false;
		if (!this.beforeLoad()) {
			return result;
		}
		try {
			if (rs != null && rs.first()) {
				result = this.setValuesByResultSet(rs);
				rs.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
			this.appendError("Error en Model.load("+this.id()+")");
		}
		return this.afterLoad();
	}

	/**
	 * Setea el 1er registro de ResultSet en el modelo actual.
	 *
	 * @param rs el ResultSet
	 * @return success, true si lo pudo setear, false en caso contrario
	 */
	public Boolean setValuesByResultSet(ResultSet rs) {
		if (rs == null) {
			return null;
		}
		Vector<ColumnMetadata>fields = this.getFields();
		for (int i = 0; i < fields.size(); i++) {
			ColumnMetadata field = fields.get(i);
			try {
				String value = rs.getString(field.name());
				if (field != null && value != null && field.type().equals("DATETIME") && value.indexOf(".") > 0 ) {
						value = value.substring(0,value.indexOf("."));
				}
				this.set(field.name(), value);
			} catch (SQLException e) {
				if(field.type().equals("DATETIME") ) {
					this.set(field.name(), "0000-00-00 00:00:00");
				} else if(field.type().equals("DATE")) {
					this.set(field.name(), "0000-00-00");
				} else {
					e.printStackTrace();
					return false;
				}
			}
		}
	  return true;
	}

	public String get(String columnName) {
		return this.values.get(columnName);
	}

	public void set(String... namesAndValues) {
		if (namesAndValues.length % 2 == 1) {
			throw new IllegalArgumentException("The number of arguments must be pair.");
		}
		for (int i = 0; i < namesAndValues.length - 1; i += 2) {
			if (namesAndValues[i] != null && namesAndValues[i + 1] != null) {
				this.values.put(namesAndValues[i].toString(), namesAndValues[i + 1]);
			}
		}
	}

	/**
	 * Use {@link #pagine(int page)}}
	 *
	 * @param pagine the pagine
	 * @return null no devuelve nada
	 */
	@Deprecated
	public Vector<Integer> pagineIds(int pagine) {return null;}

	/**
	 * Use {@link #pagine()}}
	 *
	 * @return null no devuelve nada
	 */
	@Deprecated
	public Vector<Integer> pagineIds() { return null; }

	/**
	 * Regresa un vector con claves primarias
	 *
	 * @param pagine la pagina en donde se encuentra
	 * @return the pagine ids
	 */
	public Vector<String> pagine(int pagine) {
		if (pagine > 0) {
			this.pagine = pagine;
		}
		return this.pagine();
	}

	/**
	 * Regresa un vector con claves primarias
	 *
	 * Revisar la paginación no muestra el primer registro.
	 *
	 * @return the pagine ids
	 */
	public Vector<String> pagine() {
		Vector<String> ids = new Vector<String>();
		int limit_pagine_inf = (this.limit * (this.pagine - 1));
		int limit_pagine_sup = this.limit * this.pagine;
		String qOrder = this.order;
		if (qOrder == null) {
			qOrder = this.primaryKey() + " DESC";
		}
		String query = "SELECT " + this.primaryKey()
					+ " FROM " + this.tableName()
					+ " ORDER BY " + qOrder
					+ " LIMIT " + limit_pagine_inf + " , " + limit_pagine_sup;
		if (this.debug_mode) {
			this.debug("pagine: " + query);
		}
		try {
			ResultSet rs = QueryManager.select(query);
			while (rs.next()) {
				ids.add(rs.getString(this.primaryKey()));
			}
		} catch (Exception e) {
			e.printStackTrace();
			this.appendError("Error en pagineIds con \n: " + this.pagine);
		}
		return ids;
	}

	/*
	 * Función util para buscar apartir de una columna a partir de su valor.
	 *
	 * @param field the field
	 * @param c_buscada the c_buscada
	 * @return the vector
	 */
	public Vector<Integer> buscarPorField(String field, String c_buscada) {
		Vector<Integer> ids = new Vector<Integer>();
		String query = "SELECT id"
					+ " FROM `" + this.tableName() + "`"
					+ " WHERE `" + field + "`";
		String qOrder = this.order;
		if (qOrder == null) {
			qOrder = this.primaryKey() + " DESC";
		}
		query += " ORDER BY " + qOrder;
		if (c_buscada == null) {
			query += " IS NULL";
		} else {
			query += " = '" + c_buscada + "'";
		}
		query += " LIMIT " + this.limit + "";
		if (this.debug_mode) {
			this.debug("buscarPorField: " + query);
		}
		try {
			ResultSet rs = QueryManager.select(query);
			while (rs.next()) {
				ids.add(rs.getInt("id"));
			}
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
			this.appendError("Error en BuscarPorField, consulta :\n" + query);
		}
		return ids;
	}

	/*
	 * Función util para buscar apartir de una columna a partir de su valor.
	 *
	 * @param field the field
	 * @param c_buscada the c_buscada
	 * @return the vector
	 */
	public Vector<Integer> buscarParecidoPorField(String field, String c_buscada) {
		Vector<Integer> ids = new Vector<Integer>();
		String qOrder = this.order;
		if (qOrder == null) {
			qOrder = this.primaryKey() + " DESC";
		}
		String query = "SELECT id"
					+ " FROM `" + this.tableName() + "`"
					+ " WHERE `" + field + "` LIKE '%" + c_buscada + "%'"
					+ " ORDER BY " + qOrder
					+ " LIMIT " + this.limit;
		if (this.debug_mode) {
			this.debug("buscarPorField: " + query);
		}
		try {
			ResultSet rs = QueryManager.select(query);
			while (rs.next()) {
				ids.add(rs.getInt("id"));
			}
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
			this.appendError("Error en BuscarPorField, consulta :\n" + query);
		}
		return ids;
	}

	/**
	 * Regresa el field by id.
	 *
	 * @param id the id
	 * @param field the field
	 * @return the field by id
	 */
	public String fieldById(int id, String field) {
		String query = "SELECT " + field
				+ " FROM " + this.tableName()
				+ " WHERE id = " + id;
		if (this.debug_mode) {
			this.debug("fieldById: " + query);
		}
		String result = null;
		try {
			ResultSet rs = QueryManager.select(query);
			if (rs.next()) {
				result = rs.getString(field);
			}
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
			this.appendError("Error en fieldById, consulta \n" + query);
		}
		return result;
	}

	/**
	 * Regresa el field.
	 *
	 * @param field the field
	 * @return the field
	 */
	public String field(String field) {
		return this.fieldById(this.id(), field);
	}

	/**
	 *  <p style="color:red">Convierte la fecha de DateTime a Date</p>
	 *
	 *  <strong>Atención:</strong> Esto no deberia de estar aqui!
	 *
	 * @param dt the dt
	 * @return the java.sql. date
	 */
	@Deprecated
	public static java.sql.Date DateTime2SqlDate(DateTime dt) {
		String strFecha = String.valueOf(dt.getYear())+"-"+
						  String.valueOf(dt.getMonth())+"-"+
						  String.valueOf(dt.getDay());
		return Date.valueOf(strFecha);
	}

	/**
	 * Setea una campo en la tabla dada.
	 *
	 * @param field El campo a cambiar p.e. nombre
	 * @param value El valor deseao por ejemplo "tasajo de res"
	 * @return true, si lo pudo guardar de forma correcta
	 */
	public boolean saveField(String field, String value) {
		String query = "UPDATE " + this.tableName()
				+ " SET `" + field + "` = '" + value + "'"
				+ " WHERE `" + this.primaryKey() + "` =  '"+this.get(this.primaryKey()) + "' ;";
		if (this.debug_mode) {
			this.debug("saveField: " + query);
		}
		return QueryManager.update(query);
	}

	/**
	 * Lista de ids correspondientes a la consulta recibida como argumento
	 *
	 * @param query the consulta sql a ejecutar
	 * @return un vector de Enteros Vector&lt;Integer&gt;
	 */
	protected Vector<Integer> listaDeIds(String query) {
		Vector<Integer> ids = new Vector <Integer>();
		try {
			Statement s = DataBase.conexion().createStatement();
			ResultSet result = s.executeQuery(query);
			while (result.next()) {
				ids.add(result.getInt("id"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			this.appendError("Error en Model.listaDeIds sobre la consulta \n"+query);
		}
		return ids;
	}

	protected Vector<String> keysByQuery(String query) {
		Vector<String> keys = new Vector <String>();
		try {
			Statement s = DataBase.conexion().createStatement();
			ResultSet result = s.executeQuery(query);
			while (result.next()) {
				keys.add(result.getString(this.primaryKey()));
			}
		} catch (Exception e) {
			e.printStackTrace();
			this.appendError("Error en Model.listaDeIds sobre la consulta \n"+query);
		}
		return keys;
	}
	/**
	 * Maximo.
	 *
	 * @param query the consulta sql
	 * @return the int
	 */
	@Deprecated
	protected int maximo(String query) {
		int maximo = 0;
		try {
			Statement s = DataBase.conexion().createStatement();
			ResultSet result = s.executeQuery(query);
			while (result.next()) {
				maximo = result.getInt("maximo");
			}
		} catch (Exception e) {
			e.printStackTrace();
			this.appendError("Error en Model.listaDeIds sobre la consulta \n" + query);
		}
		return maximo;
	}

	/**
	 * Inicia una transacción
	 *
	 * @return success true si la transacción fue inicializada, false en caso contrario
	 **/
	public boolean startTransaction() {
		boolean resultado = false;
		try {
			PreparedStatement begin = DataBase.conexion().prepareStatement("START TRANSACTION;");
			begin.execute();
			this.transaccionActiva = true;
			resultado = true;
		}catch (Exception es) {
			es.printStackTrace();
			this.appendError("Error startTransaction() sobre\n"+this);
		}
		return resultado;
	}

	/**
	 * Confirma la transaccion una transacción
	 *
	 * @return success true si la transacción fue confirmada, false en caso contrario
	 **/
	public boolean commitTransacction() {
		boolean resultado = false;
		if (this.transaccionActiva == false) {
			return false;
		}
		try {
			PreparedStatement commit = DataBase.conexion().prepareStatement("COMMIT");
			commit.execute();
			this.transaccionActiva = false;
			resultado = true;

		}catch (Exception es) {
			es.printStackTrace();
			this.appendError("Error en PedidoModel.nuevo sobre\n"+this);

		}
		return resultado;
	}

	/**
	 *  Devuelve un vector de los meta datos de las columnas correspondientes a la tabla del modelo
	 *
	 * @return Vector&lt;ColumnMetadata&gt; vector de metadatos de las columnas
	 */
	public Vector<ColumnMetadata> getFields() {
		return BDTables.columns(this.tableName());
	}

	public String toString() {
		String out =  "Model " + this.tableName() + "\n";
		out +=  "=============================================\n";
		Vector<ColumnMetadata> fieldsData = this.getFields();
		for (int i = 0; i < fieldsData.size(); i++) {
			ColumnMetadata cMdata = fieldsData.get(i);
			out += "\t" + cMdata.name() + " = " + this.get(cMdata.name()) + "\n";
		}
		return out + "\n";
	}

	public void disableAutoCreated() {
		this.auto_created = false;
	}
	public void disableAutoModified() {
		this.auto_modified = false;
	}
	public void enableAutoCreated() {
		this.auto_created = true;
	}
	public void enableAutoModified() {
		this.auto_modified = true;
	}
	public void enableDebug() {
		this.debug_mode = true;
	}
	/**
	 * Insert ejecuta una insercción en el registro
	 *
	 * @return success, true si lo pudo guardar, false en caso contrario
	 */
	public boolean insert() {
		if (this.beforeInsert() == false) {
			return false;
		}
		Vector<String> columnsNames  = new Vector<String>();
		Vector<String> columnsValues = new Vector<String>();
		Vector<ColumnMetadata> fieldsData = this.getFields();
		for (int i = 0; i < fieldsData.size(); i++) {
			ColumnMetadata columnMeta = fieldsData.get(i);
			String value = this.get(columnMeta.name());
			if (this.auto_created && columnMeta.name().equals("created") && columnMeta.type().equals("DATETIME")) {
				value = QueryManager.now();
			}
			if (value != null) {
				columnsNames.add(columnMeta.name());
				value = value.replaceAll("'", "''");
				columnsValues.add(value);
			}
		}
		if (QueryManager.insert(this.tableName(), columnsNames , columnsValues, this.debug_mode)) {
			return this.afterInsert();
		}
		return false;
	}

	/**
	 * Realiza un UPDATE en la base de datos con los datos actuales del objeto
	 *
	 * @return success, true si lo pudo actualizar, false en caso contrario
	 */
	public boolean update() {
		if (this.beforeUpdate() == false) {
			return false;
		}
		Vector<String> setFields = new Vector<String>();
		String where = "`" + this.primaryKey() + "` = '" + this.get(this.primaryKey()) + "'";
		Vector<ColumnMetadata> columnsMetadata = this.getFields();
		for (int i = 0; i < columnsMetadata.size(); i++) {
			ColumnMetadata columnMeta = columnsMetadata.get(i);
			String value = this.get(columnMeta.name());
			if (columnMeta.name().equals(this.primaryKey())) {
				continue;
			}
			if (this.auto_modified && columnMeta.name().equals("modified") && columnMeta.type().equals("DATETIME")) {
				value = QueryManager.now();
			}
			if (columnMeta.name().equals("created")) {
				continue;
			}
			if (value != null) {
				value = value.replaceAll("'", "''");
				setFields.add(columnMeta.name() +" = '"+ value + "'");
			} else {
				setFields.add(columnMeta.name() +" = NULL");
			}
		}
		if ( QueryManager.update(this.tableName(), setFields , where, this.debug_mode) ) {
			return this.afterUpdate();
		}
		return false;
	}

	/**
	 *  Guardar: Revisa si existe el registro para ejecutar un UPDATE o si no existe para ejecutar un INSERT
	 *
	 * @return success: true si lo pudo guardar, false en caso contrario
	 */
	public Boolean save() {
		boolean exito = false;
		if (this.beforeSave()== false) {
			return false;
		}
		if ( this.exists()) {
			exito = this.update();
		} else {
			exito = this.insert();
		}
		if (exito) {
			return this.afterSave();
		}
		return exito;
	}

	public Boolean delete() {
		if (!this.beforeDelete()) {
			return false;
		}
		String where = "`" + this.primaryKey() + "` = '" + this.get(this.primaryKey()) + "'";
		QueryManager.delete(this.tableName(), where);
		return this.afterDelete();
	}

	public Boolean deleted(String primaryKeyValue) {
		this.set(this.primaryKey(), primaryKeyValue);
		return this.delete();
	}

	public void tableName(String table_name) {
		this.table_name = table_name;
		BDTables.addIfNotExits(table_name);
	}

	public void truncate() {
		String query = "TRUNCATE " + this.tableName();
		QueryManager.executeUpdate(query);
	}

	/**
	 * Crea un clone del modelo
	 *
	 * Mayor info: http://docs.oracle.com/javase/7/docs/api/java/lang/Object.html#clone()
	 */
	public Model clone() {
		Model newModel = null;
		try {
			newModel = this.getClass().newInstance();
			Vector<ColumnMetadata> columnsMetadata = this.getFields();
			for (int i = 0; i < columnsMetadata.size(); i++) {
				ColumnMetadata columnMeta = columnsMetadata.get(i);
				String value = this.get(columnMeta.name());
				if (columnMeta.name().equals(this.primaryKey())) {
					continue;
				}
				if (value != null) {
					newModel.set(columnMeta.name(), value);
				}
			}
		} catch (InstantiationException | IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return newModel;
	}

	/**
	 * Se encarga de imprimir el toString generado
	 */
	public void print() {
		System.out.println(this);
	}

	public String toJSON() {
		Vector<String> modelData = new Vector<String>();
		Vector<ColumnMetadata> fieldsData = this.getFields();
		for (int i = 0; i < fieldsData.size(); i++) {
			ColumnMetadata cMdata = fieldsData.get(i);
			String value = this.get(cMdata.name());
			if (value == null) {
				value = "null";
			} else {
				if (!StringUtils.isNumeric(value) || (value.length()>0 && value.charAt(0) == '0')) {
					value = '"'+ value + '"';
				}
			}
			modelData.add('"' + cMdata.name() + "\":" + value);
		}
		return "{" + join(modelData, ",") + "}";
	}

	public void loadJson(JSONObject json) {
		try {
			Vector<String> columnas = this.fieldsNames(true);
			for(int i = 0; i < columnas.size(); i++) {
				String value = json.get(columnas.get(i)).toString();
				value = value.replaceAll("false", "0");
				value = value.replaceAll("true", "1");
				this.set(columnas.get(i), value);
			}
		} catch (Exception e) {
			System.out.println("error en loadjson "+e.toString());
		}
	}

	public boolean saveByJson(JSONObject json) {
		try {
			this.loadJson(json);
			if(Request.print) {
				this.print();
			}
			return this.save();
		} catch (Exception e) {
			System.out.println("error en save by json"+e.toString());
			return false;
		}
	}

	public void saveByJsonArray(JSONArray array) {
		for(int i = 0;i < array.length(); i++) {
			this.saveByJson(array.getJSONObject(i));
		}
	}
	public Vector<String> fieldsNames(boolean with_id) {
		Vector<ColumnMetadata> colums = this.getFields();
		Vector<String> fieldsNames = new  Vector<String>();
		for(int i = 0; i < colums.size(); i++) {
			fieldsNames.add(colums.get(i).name());
		}

		if(!with_id) {
			fieldsNames.remove("id");
		}
		return fieldsNames;
	}
}
