package com.mundosica.core.Model;

import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import com.mysql.jdbc.ResultSetMetaData;

/**
 * Extrae la meta información para una tabla
 * 
 * @author fitorec
 */
public class TableInfo {
	Vector<ColumnMetadata> columns = new Vector<ColumnMetadata>();
	protected String name = null;
	protected String primary_key = null;

	public String name() {
		return name;
	}

	public String primaryKey() {
		return this.primary_key;
	}

	public Vector<ColumnMetadata> columns() {
		return this.columns;
	}

	public TableInfo(String tableName) {
		this.name = tableName;
		this.columns = new Vector<ColumnMetadata>();
		DatabaseMetaData meta;
		try {
			String query = "SHOW KEYS FROM " + this.name() + " WHERE Key_name = 'PRIMARY'";
			ResultSet rs = DataBase.conexion().createStatement().executeQuery(query);
			if(rs != null && rs.first()) {
				primary_key = rs.getString("Column_name");
			}
			meta = DataBase.conexion().getMetaData();
			rs = meta.getColumns(null, null, this.name(), null);
			while (rs.next()) {
				if ("INFORMATION_SCHEMA".equals(rs.getString("TABLE_SCHEM"))) {
					continue;
				}
				ColumnMetadata c = new ColumnMetadata(
						rs.getString("COLUMN_NAME"),
						rs.getString("TYPE_NAME"),
						rs.getInt("COLUMN_SIZE"),
						rs.getInt("ORDINAL_POSITION")
				);
				this.columns.add(c);
			}
		} catch (SQLException e) {
			System.out.println(
				"com.mundosica.core.Model.TableInfo: Error al intentar "
				+ "cargar la informacion de la tabla: "+tableName+", revise el estado de su conexión"
			);
			e.printStackTrace();
		}
	}
}
