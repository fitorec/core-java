package com.mundosica.core.Model;

import java.util.Vector;

/**
 * Administra el contenedor de los contenedores(<b>TableInfo</b>) de información de las tablas
 *
 * @author fitorec <a href='mailto:chenerec@gmail.com'>chenerec@gmail.com</a>
 */
public class BDTables {
	/**
	 * El <b>contenedor de información de las tablas</b>
	 */
	private static Vector<TableInfo> tables = new Vector<TableInfo>();

	/**
	 * Devuelve la información de una tabla en especifica.
	 * 
	 * @param tableName el nombre de la tabla
	 * @return tableInfo un objeto que contiene los metadatos de la tabla
	 */
	public static TableInfo info(String tableName) {
		int index = BDTables.indexOf(tableName);
		if (index != -1) {
			return BDTables.tables.get(index);
		}
		return null;
	}

	/**
	 * Devuelve la posición en la que se encuentra la información de una tabla
	 * 
	 * @param tableName el nombre de la tabla
	 * @return la posición en el vector, en caso de no existir devuelve -1
	 */
	public static int indexOf(String tableName) {
		for (int index = 0; index < BDTables.tables.size(); index++) {
			if (BDTables.tables.get(index).name().equals(tableName)) {
				return index;
			}
		}
		return -1;
	}

	/**
	 * Agrega información de una tabla en caso de no existir
	 * 
	 * @param tableName el nombre de la tabla en cuestion
	 * @return success devuelve true si lo agrego, false en caso contrario
	 */
	public static Boolean addIfNotExits(String tableName) {
		if (BDTables.indexOf(tableName) == -1) {
			BDTables.tables.add(new TableInfo(tableName));
			return true;
		}
		return false;
	}

	/**
	 * Devuelve el vector de columnas de una tabla en especificos.
	 * 
	 * @param tableName el nombre de la tabla
	 * @return regresa el vector de columnas de dicha tabla en caso contrario devuelve null
	 */
	public static Vector<ColumnMetadata> columns(String tableName) {
		TableInfo table = BDTables.info(tableName);
		if ( table != null) {
			return table.columns(); 
		}
		System.out.println("BDTables.info devolvio null para: " + tableName);
		return null;
	}
}
