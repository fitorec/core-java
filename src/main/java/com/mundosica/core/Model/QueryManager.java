package com.mundosica.core.Model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Time;
import java.util.Vector;

import static com.mundosica.core.common.Util.*;
 
public class QueryManager {

	/**
	 * Realiza una consulta del tipo SELECT
	 * 
	 * @param query la consulta a ejecutar
	 * @return ResultSet el resultado de la consulta
	 */
	static public ResultSet select(String query) {
		try {
			Statement s = DataBase.conexion().createStatement();
			return s.executeQuery(query);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("No se pudo ejecutar existosamente la consulta:\n" + query);
			return null;
		}
	}

	/**
	 * Realiza una consulta del tipo UPDATE
	 * 
	 * @param tableName El nombre de la tabla
	 * @param setFields un vector con cadenas del tipo colum_name = 'valor nombre'
	 * @param where contiene la condición de insercción.
	 * @return boolean success true si lo pudo realizar, false en caso contrario
	 */
	static public boolean update(String tableName, Vector<String> setFields, String where) {
		return QueryManager.update(tableName, setFields, where, false);
	}
	
	static public boolean update(String tableName, Vector<String> setFields, String where, boolean debug_mode) {
		String query  = String.format(
				"UPDATE  `%s` SET  %s WHERE %s;",
				tableName, join(setFields, ", "), where
			);
		if (debug_mode) {
			System.out.println(query);
		}
		return QueryManager.update(query);
	}

	/**
	 * Realiza una consulta del tipo UPDATE
	 * 
	 * @param query la consulta SQL a realizar
	 * @return success true si lo pudo realizar, false en caso contrario
	 */
	static public boolean update(String query) {
		return QueryManager.executeUpdate(query) > 0;
	}

	/**
	 * Realiza una consulta del tipo INSERT
	 * 
	 * @param tableName el nombre de la tabla.
	 * @param columnsNames un vector que contiene el nombre de las columnas.
	 * @param columnsValues un vector que contiene los valores a las columnas.
	 * @param debug_mode Si este es true imprime la consulta SQL
	 * @return boolean success true si lo pudo realizar, false en caso contrario
	 */
	static public boolean insert(String tableName, Vector<String> columnsNames, Vector<String> columnsValues, boolean debug_mode) {
		String fields = join(columnsNames, "`, `");
		String values = join(columnsValues, "', '");
		String query  = String.format(
			"INSERT INTO %s (`%s`) VALUES ('%s');",
			tableName, fields, values
		);
		if (debug_mode) {
			System.out.println(debug_mode);
		}
		return QueryManager.insert(query);
	}
	
	static public boolean insert(String tableName, Vector<String> columnsNames, Vector<String> columnsValues) {
		return QueryManager.insert( tableName, columnsNames, columnsValues, false);
	}

	/**
	 * Realiza una insercción SQL
	 * 
	 * @param query la consulta de insercción
	 * @return success true si lo pudo realizar, false en caso contrario
	 */
	static public boolean insert(String query) {
		return QueryManager.executeUpdate(query) > 0;
	}

	static public boolean delete(String query) {
		return QueryManager.executeUpdate(query) > 0;
	}

	static public boolean delete(String tableName, String condition) {
		String query  = String.format(
			"DELETE FROM %s WHERE %s;",
			tableName, condition
		);
		return QueryManager.delete(query);
	}

	/**
	 * Ejecuta una consulta del tipo executeUpdate 
	 * 
	 * @param query es la consulta a ejecutar
	 * @return success true si la consulta se ejecuta correctamente, false en caso contrario.
	 */
	static public int executeUpdate(String query) {
		if (query == null || query.length() < 10) {
			System.out.println("Consulta invalida: com.mundosica.core.Model.QueryManager:executeUpdate consulta:\n" + query);
			return 0;
		}
		try {
			PreparedStatement preparedStmt = DataBase.conexion().prepareStatement(query);
			preparedStmt.executeUpdate();
			return 1;
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Error : com.mundosica.core.Model.QueryManager:executeUpdate consulta:\n" + query);
		}
		return 0;
	}

	/**
	 * Regresa la fecha y hora en formato similar al now de mysql.
	 * 
	 * info: http://dev.mysql.com/doc/refman/5.5/en/date-and-time-functions.html#function_now
	 * 
	 * @return now la fecha y hora actual p.e. "2083-06-09 13:47:36"
	 */
	public static String now() {
		String now = (new Timestamp(System.currentTimeMillis())).toString();
		if (now.indexOf(".") > 0) {
			now = now.substring(0, now.indexOf("."));
		}
		return now;
	}

	/**
	 * Regresa la fecha y hora en formato similar al curdate de mysql.
	 * El formato en el que lo regresa es en yyyy-MM-dd
	 * 
	 * mayor info: http://dev.mysql.com/doc/refman/5.5/en/date-and-time-functions.html#function_curdate
	 * 
	 * @return curdate La fecha actual
	 */
	public static String curdate() {
		String[] curtDate = QueryManager.now().split(" ");
		return curtDate[0];
	}

	/**
	 * Regresa la fecha y hora en formato similar al curdate de mysql hh:mm:ss
	 * 
	 * mayor info: http://dev.mysql.com/doc/refman/5.5/en/date-and-time-functions.html#function_curdate
	 * 
	 * @return curdate La fecha actual
	 */
	public static String curtime() {
		return new Time(System.currentTimeMillis()).toString();
	}

}
