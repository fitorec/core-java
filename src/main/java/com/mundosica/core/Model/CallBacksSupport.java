package com.mundosica.core.Model;

/**
 * Contiene los metodos <i>callBacks</i> soportados por el Modelo 
 * 
 * @author fitorec
 */
public abstract class CallBacksSupport {
	/**
	 * beforeSave Callback para sobre-escritura
	 *
	 * @return si devuelve true continua la ejecución si devuelve false se detiene.
	 */
	public Boolean beforeSave() { return true; }

	/**
	 * afterSave Callback para sobre-escritura
	 *
	 * @return si devuelve true continua la ejecución si devuelve false se detiene.
	 */
	public Boolean afterSave() { return true;}

	/**
	 * beforeUpdate Callback para sobre-escritura
	 *
	 * @return si devuelve true continua la ejecución si devuelve false se detiene.
	 */
	public Boolean beforeUpdate() { return true; }

	/**
	 * afterUpdate Callback para sobre-escritura
	 *
	 * @return si devuelve true continua la ejecución si devuelve false se detiene.
	 */
	public Boolean afterUpdate() { return true;}

	/**
	 * beforeInsert Callback para sobre-escritura
	 *
	 * @return si devuelve true continua la ejecución si devuelve false se detiene.
	 */
	public Boolean beforeInsert() { return true; }

	/**
	 * afterInsert Callback para sobre-escritura
	 *
	 * @return si devuelve true continua la ejecución si devuelve false se detiene.
	 */
	public Boolean afterInsert() { return true;}

	/**
	 * beforeLoad Callback para sobre-escritura
	 *
	 * @return si devuelve true continua la ejecución si devuelve false se detiene.
	 */
	public Boolean beforeLoad() { return true; }

	/**
	 * afterLoad Callback para sobre-escritura
	 *
	 * @return si devuelve true continua la ejecución si devuelve false se detiene.
	 */
	public Boolean afterLoad() { return true;}
	
	/**
	 * beforeDelete Callback para sobre-escritura
	 *
	 * @return si devuelve true continua la ejecución si devuelve false se detiene.
	 */
	public Boolean beforeDelete() { return true; }

	/**
	 * afterDelete Callback para sobre-escritura
	 *
	 * @return si devuelve true continua la ejecución si devuelve false se detiene.
	 */
	public Boolean afterDelete() { return true;}
	
	/**
	 * beforeValidation Callback para sobre-escritura
	 *
	 * @return si devuelve true continua la ejecución si devuelve false se detiene.
	 */
	public Boolean beforeValidation() { return true; }

	/**
	 * afterValidation Callback para sobre-escritura
	 *
	 * @return si devuelve true continua la ejecución si devuelve false se detiene.
	 */
	public Boolean afterValidation() { return true;}
}
