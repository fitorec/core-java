package com.mundosica.core.Model;

/**
* Se encarga de administrar los meta datos de una columna de una tabla en la Base de Datos.
*  
*  
* @author fitorec - programacion@mundosica.com
* @version 2.0
*/

public class ColumnMetadata {
	/**
	 * El <b>nombre</b> de la columna
	 */
	private String name;

	/**
	 * El tipo de columna p.e.: varchar, string, int, boolean, text, datetime, etc..
	 */
	private String type;

	/**
	 * El tamaño del dato
	 */
	private int size;

	/**
	 * La posición que tiene en la tabla
	 */
	private int position;

	/**
	 * Información si el campo es escribible
	 */
	public boolean isWrite = true;

	/**
	 * Constructor vacio
	 */
	ColumnMetadata() {}

	/**
	 * Constructor
	 * 
	 * @param name el nombre de la columna
	 * @param type el tipo de dato
	 * @param size la longitud del dato
	 * @param position la posición de la columna en la tabla
	 */
	ColumnMetadata(String name, String type, int size, int position) {
		this.name = name;
		this.type = type;
		this.size = size;
		this.position = position;
	}

	/**
	 * Crea y lo devuelve un ColumnMetadata a partir de params 
	 * 
	 * @param params parametros recibidos por pares nombreParametro, valorParametro.
	 * @return un ColumnMetadata de los parametros
	 */
	static public ColumnMetadata create(String... params) {
		ColumnMetadata c = new ColumnMetadata();
		if (params == null || params.length % 2 == 1) {
			return c;
		}
		for (int i = 0; i < params.length - 1; i += 2) {
            if (params[i] == null) {
            	throw new IllegalArgumentException("Los nombre de los atributos no pueden ser null");
            }
            String atributeName = params[i].toLowerCase(), atributeValue = params[i + 1];
            c.set(atributeName, atributeValue);
        }
		return c;
	}

	/**
     * {@link ColumnMetadata#name}
     * 
     * @return name el nombre de la columna
     */
	public String name() { return this.name; }

	/**
     * {@link ColumnMetadata#type}
     * 
     * @return type el tipo de dato p.e. varchar, int, text, dateTime, etc...
     */
	public String type() { return this.type; }

	/**
     * {@link ColumnMetadata#size}
     * 
     * @return size la longitud del tipo de dato, p.e. para varchar(50) el valor es 50
     */
	public int size() { return this.size; }

	/**
     * {@link ColumnMetadata#position}
     * 
     * @return position la posicion de la columna en el esquema de la tabla
     */
	public int position() { return this.position; }

	public String toString() {
		return this.position() + " .- " + this.name() + " "
				+ this.type() + " (" + this.size() + ")";
	}

	/**
	 * Setea los atributos de un field.
	 * 
	 * @param nombre correspondiente al atributo de columna(name, type, position, isWrite)
	 * @param valor correspondiente a setear
	 * @return success, true si lo pudo setear, false en caso contrario
	 */
	public boolean set(String nombre, String valor) {
        if (nombre == null || valor == null) {
        	return false;
        }
        switch(nombre) {
	        case "name":
	        	this.name = valor;
	        	break;
	        case "type":
	        	this.type = valor;
	        	break;
	        case "size":
	        	this.size = Integer.parseInt(valor);
	        	break;
	        case "position":
	        	this.position = Integer.parseInt(valor);
	        	break;
	        case "isWrite":
	        	this.isWrite = new Boolean(valor);
	        	break;
	        default:
	        	return false;
        }
        return true;
	}
}
