package com.mundosica.core.Model;

class DefaultModel extends Model {
	static final public String primary_key = "id";
	static final public int limit = 20 ;
	static final public int page  = 1 ;
	static final public int offset  = 0 ;
}
