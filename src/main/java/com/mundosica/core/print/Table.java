package com.mundosica.core.print;

import java.awt.Graphics;
import java.util.Vector;

public class Table extends Drawable {
	
	private int heightCelda = 10;
	private int []widthCeldas = null;
	private int []textPositionCeldas = null;
	private String[] cabecera = null;
	private Vector<String []>info;
	
	public Table(Vector<String []>info) {
		this.info = info;
		this.fontSize(10);
	}
	
	public void posicionTextoCeldas(int []textPositionCeldas) {
		this.textPositionCeldas = textPositionCeldas;
	}
	
	public void cabecera(String[] c) {
		this.cabecera = c;
	}
	
	public void widthCeldas(int []widthCeldas) {
		this.widthCeldas = widthCeldas;
	}
	
	public void heightCelda(int heightCelda) {
		this.heightCelda = heightCelda;
	}
	public int heightCelda() {
		return this.heightCelda;
	}

	private void drawRow(Graphics g,int posCeldaY, String data[]) {
		if(data == null || data.length <= 0) {
			return;
		}
		int posCeldaX = x;
		int widthCelda = 0;
		int textPosition = this.posicionTexto();
		boolean usarWidthDefault = widthCeldas == null;
		boolean usarTextPosistionDefault = textPositionCeldas == null;
		int ultimo = data.length - 1;
		for (int i = 0; i < ultimo && posCeldaX < w + x; i++) {			
			textPosition = usarTextPosistionDefault? 
					this.posicionTexto() : textPositionCeldas[i];
			widthCelda = usarWidthDefault? this.countPixeles(g, data[i]) + 5 : widthCeldas[i];
			this.text(g, posCeldaX, posCeldaY, widthCelda, textPosition, data[i]);
			posCeldaX += widthCelda;
		}
		widthCelda = usarWidthDefault? 
				this.countPixeles(g, data[ultimo]) + 5 : widthCeldas[ultimo];
		textPosition = usarTextPosistionDefault? 
				this.posicionTexto() : textPositionCeldas[ultimo];
		if((widthCelda + posCeldaX) > w + x) {
			//System.out.println(widthCelda + "+" + posCeldaX + ">" + w + "+" + x);
			widthCelda = (w + x) - posCeldaX;
			//System.out.println("widthCelda = " + (w + x) + "-" + posCeldaX);
		}
		this.text(g, posCeldaX, posCeldaY, widthCelda, textPosition, data[ultimo]);
	}
	private void drawRows(Graphics g) {
		int posCeldaY = y;
		if (cabecera != null && cabecera.length != 0) {
			g.drawLine(x, posCeldaY + 1, w + x, posCeldaY + 1); 
			drawRow(g,posCeldaY,cabecera);
			posCeldaY += heightCelda;
		}
		g.drawLine(x, posCeldaY + 1, w + x, posCeldaY + 1); 
		for (int i = 0; i < this.info.size(); i++) {
			drawRow(g,posCeldaY,info.get(i));
			posCeldaY += heightCelda;
		}
		g.drawLine(x, posCeldaY + 1, w + x, posCeldaY + 1 );
	}
	public void print(Graphics g) {
		drawRows(g);
	}
	private Text text(Graphics g,int posCeldaX,int posCeldaY,int widthCelda,int textposition,
			String data) {
		Text t = new Text(data);
		t.setBounds(posCeldaX,posCeldaY + heightCelda,widthCelda,this.fontSize());
		t.posicionTexto( textposition);
		t.print(g);
		return t;
	}
}
