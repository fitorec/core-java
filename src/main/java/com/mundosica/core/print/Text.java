package com.mundosica.core.print;

import java.awt.Font;
import java.awt.Graphics;

public class Text extends Drawable {
	String line = "";
	public Text(String t) {
		this.line = t;
	}
	public void drawString(Graphics g) {
		//this.fontSize(this.h);
		g.setFont(new Font(this.font(), Font.PLAIN, fontSize()));
		int posX = x;
		int tamanioString = this.countPixeles(g, line);
		if(this.posicionTexto() == Drawable.CENTER) {
			posX = posCenter(tamanioString);
		} else if(posicionTexto() == Drawable.RIGHT) {
			posX = posRight(tamanioString);
		} 
		if(tamanioString > this.w) {
			int tamanioStringPermitido = this.countChars(g, this.w, line);
			/*
			System.out.println("string: " + line);
			System.out.println("tamaño del string: " + tamanioString);
			System.out.println("tamaño premitido: " + tamanioStringPermitido);
			System.out.println("tamaño del field: " + w);
			////*/
			if(line.length() > tamanioStringPermitido) {
				line = line.substring(0, tamanioStringPermitido - 1);
			}
		}
		g.drawString(line,posX,this.y);
	}
	
	public void print(Graphics g) {
		drawString(g);
	}
	
	private int posCenter(int tamanhoString) {
		int center = (int)(((w/2) + this.x)-(tamanhoString/2));
		return center;
	}
	
	private int posRight(int tamanhoString) {
		int right = ((int)((this.x + w)-tamanhoString));
		if(right < x) {
			return x;
		}
		return right;
	}
	
	@Override
	public void setBounds(int x, int y, int w, int h) {
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h;
		this.fontSize(h);
	}
}
