package com.mundosica.core.print;

import java.awt.Graphics;
import java.util.Vector;


public class TextArea extends Drawable {
	
	Vector<String> texto;
	
	public TextArea(Vector<String> texto) {
		this.texto = texto;
	}
	
	public void drawStrings (Graphics g) {
		int posY = y;		
		for (int i = 0; i < texto.size() && posY < h + y; i++) {
			//drawString(g,texto.get(i),posY);
			Text t = new Text(texto.get(i));
			//System.out.println(x +","+posY+","+this.w+","+this.fontSize());
			t.setBounds(this.x, posY, this.w, this.fontSize());
			t.posicionTexto(this.posicionTexto());
			t.print(g);
			posY += this.fontSize();
		}
	}
	public void print(Graphics g) {
		drawStrings(g);
	}
}
