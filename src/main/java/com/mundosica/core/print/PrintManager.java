package com.mundosica.core.print;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.print.Book;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.util.Vector;

import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.attribute.AttributeSet;
import javax.print.attribute.HashAttributeSet;
import javax.print.attribute.standard.PrinterName;

import org.eclipse.swt.SWT;
import org.eclipse.swt.printing.PrintDialog;
import org.eclipse.swt.printing.PrinterData;
import org.eclipse.swt.widgets.Shell;



public class PrintManager {
	
	private String nombreImpresora = "";
	public double alto =  842;
	public double ancho = 596;
	public double x = 0;
	public double y = 596;
	public double w = 0;
	public double h = 842 ;
	public static final int  HORIZONTAL = PageFormat.LANDSCAPE;
	public static final int   VERTICAL= PageFormat.PORTRAIT;
	private int orientacion = VERTICAL;
	public static int fontSize = 12;
	private Vector<String> impresoras = new Vector<String>();
	private Vector<Drawable> elements = new Vector<Drawable>();
	
	public void addElement(Drawable e) {
		elements.add(e);
	}
	public void draw(Graphics g) {	}
	
	public void imprimir() {
		try {
			PrinterJob printJob = PrinterJob.getPrinterJob();
			Book book = new Book();
			book.append(new TiketImprimible(), configuracionPapel());
			printJob.setPageable(book);
			establcerImpresora(printJob);
		    System.out.println("impresora seleccionada: " + printJob.getPrintService().getName());
		    printJob.print();//imprimimos lo  que esta en el job
		}
		catch (Exception a) {
			System.out.println("error al imprimir\n" + a);
			a.printStackTrace();
		}
	}
	private void establcerImpresora(PrinterJob printJob) {
		try {
			AttributeSet aset = new HashAttributeSet();
		     aset.add(new PrinterName(this.nombreImpresora, null));
		     //busca la impresora
		     PrintService[] services = PrintServiceLookup.lookupPrintServices(null, aset);
		     for (PrintService printService : services) {
		    	 PrintService printers[] = PrintServiceLookup.lookupPrintServices(null, aset);
		    	//le asignamos la impresora de trabajo al job
		    	 if (printers.length == 1) {
		    		 printJob.setPrintService(printers[0]);
	            }
		     }
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}
	private PageFormat configuracionPapel() {
		Paper paper = new Paper();
        paper.setSize(this.ancho, this.alto);
        paper.setImageableArea(x,w,y,h );
	    PageFormat pF= new PageFormat();
	    pF.setPaper(paper);
	    pF.setOrientation(this.orientacion);
	    return pF;
	}
	
	public String impresora(String nombreImpresora) {
		this.nombreImpresora = nombreImpresora;
		return this.nombreImpresora;
	}
	public String impresora() {
		return this.nombreImpresora;
	}
	public void orientacion(int o) {
		this.orientacion = o;
	}
	public Vector<String> impresorasDisponibles() {
        PrintService[] services = PrintServiceLookup.lookupPrintServices(null, null);//con este vemos las impresoras instaladas
        for (PrintService printService : services) {
        	impresoras.add(printService.getName());
            System.out.println(printService.getName());
        }
        return impresoras;
	}
	
	class TiketImprimible implements Printable {

		public int print(Graphics g, PageFormat pF, int pageIndex) throws PrinterException {
			try {
				for (int i = 0; i < elements.size(); i++) {
					g.setFont(new Font("TimesRoman", Font.PLAIN, fontSize));
					elements.get(i).print(g);
				}
				draw(g);
			} catch(Exception ex) {
				ex.printStackTrace();
			}
			return 0;
		}
	}
	
	public static String seleccionarImpresora() {
		PrintDialog printDialog = new PrintDialog(new Shell(), SWT.NONE);
        printDialog.setText("Seleccione su impresora predeterminada");
        PrinterData printerData = printDialog.open();
        if (!(printerData == null)) {
          return printerData.name;
        }
        return null;
	}

}
