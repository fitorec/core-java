package com.mundosica.core.print;

import java.awt.FontMetrics;
import java.awt.Graphics;
import java.util.Vector;

public abstract class Drawable {
	
	private String font = "TimesRoman";
	private int fontSize = 12;
	
	private int posicionTexto = 2;
	public static final int CENTER = 1;
	public static final int LEFT = 2;
	public static final int RIGHT = 3;
	int x;
	int y;
	int w;
	int h;
	
	public void setBounds(int x, int y, int w, int h) {
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h;
	}
	
	public void setPos(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public void posicionTexto(int posicion) {
		this.posicionTexto = posicion;
	}
	
	public int posicionTexto() {
		return this.posicionTexto;
	}
	public void font(String nombreFont) {
		this.font = nombreFont;
	}
	public String font() {
		return this.font;
	}
	public void fontSize(int fontSize) {
		this.fontSize = fontSize;
	}
	
	public int fontSize() {
		return this.fontSize;
	}
	
	public int countPixeles(Graphics g,String string) {
		FontMetrics f = g.getFontMetrics();
		//System.out.println(line);
		return f.stringWidth(string);
	}
	
	public int countChars(Graphics g,int limit, String string) {
		int tamString = countPixeles(g,string);
		int length = string.length();
		return ((int)(length * limit)/tamString);
	}

	public abstract void print(Graphics g);
}
