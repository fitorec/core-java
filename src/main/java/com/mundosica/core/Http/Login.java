package com.mundosica.core.Http;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.async.Callback;
import com.mashape.unirest.http.exceptions.UnirestException;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.io.IOUtils;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.log4j.Logger;


public class Login {
	private static final String IOUtils = null;
	private static String url   = null;
	private static String username = null;
	private static String password = null;
	private static org.apache.log4j.Logger log = Logger.getLogger(Login.class);
	private static HttpResponse<JsonNode> responseLogin = null;

	public static String urlEncoder(String url) {
		return url.replaceAll("\\s", "%20");
	}

	public static String url() {
		return Login.url;
	}
	public static String username() {
		return Login.username;
	}
	public static String password() {
		return Login.password;
	}
	public static HttpResponse<JsonNode> response() {
		return Login.responseLogin;
	}

	public static Boolean set(String ... fieldsAndValues) {
		if (fieldsAndValues == null || fieldsAndValues.length % 2 == 1) {
			return false;
		}
		for (int i = 0; i < fieldsAndValues.length - 1; i += 2) {
            if (fieldsAndValues[i] == null) {
            	throw new IllegalArgumentException("Los nombre de los atributos no pueden ser null");
            }
            String nombre = fieldsAndValues[i].toLowerCase(), valor = fieldsAndValues[i + 1];
            if (valor == null) {
            	return false;
            }
            switch(nombre) {
            case "url":
            	Login.url = Login.urlEncoder(valor);
            	break;
            case "username":
            	Login.username = valor;
            	break;
            case "password":
            	Login.password = valor;
            	break;
            default:
            	return false;
            }
        }
		return true;
	}
	public static Boolean startSession() {
		try {
			HttpResponse<JsonNode> response = Unirest
				.post(Login.url())
				.field("User[username]", Login.username())
				.field("User[password]", Login.password())
				.basicAuth(Login.username(), Login.password())
				.asJson();
			System.out.println(response.getBody());
			responseLogin = response;
			if (response.getStatus() == 200) {
				return true;
			}
		} catch (UnirestException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}


	public static HttpResponse<JsonNode> getJSON(String url) {
		HttpResponse<JsonNode> response = null;
		try {
			response = Unirest
			.get(Login.urlEncoder(url))
			.asJson();
			if (response.getStatus() != 200) {
				//** Pensando que hacer aquí **/
				Login.logOut();
			}
		} catch (UnirestException e) {
			e.printStackTrace();
			System.out.println("Login.upload Error al cargar la URL: " + url);
		}
		return response;
	}

	public static void download(String url, final String localPath) {
		HttpClient client = new HttpClient();
		GetMethod method = new GetMethod(url);
		//client.getState().setCredentials(...);
		method.setDoAuthentication(true);
		int statusCode;
		try {
			statusCode = client.executeMethod(method);
			if (statusCode == HttpStatus.SC_OK) {
				InputStream in = new BufferedInputStream(method.getResponseBodyAsStream());
				OutputStream out = new BufferedOutputStream(new FileOutputStream(new File(localPath)));
				org.apache.commons.io.IOUtils.copy(in,out);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void upload(String url, String localPath, String fieldName) {
		if ( fieldName == null) {
			fieldName = "file";
		}
		try {
			Unirest.post(Login.urlEncoder(url))
			.header("accept", "application/json")
			  .field(fieldName, new File(localPath))
			  .asJson();
		} catch (UnirestException e) {
			e.printStackTrace();
			log.error("Login.upload Error al cargar la URL: " + url);
		}
	}

	public static void logOut() {
		
	}

	/**
	 * Se sale del sistema
	 */
	public static void exit() {
		try {
			Unirest.shutdown();
		} catch (IOException e) {
			e.printStackTrace();
			log.error("Login.exit Error al salir");
		}
	}
}

class CoreLoginException extends Exception {

	private static final long serialVersionUID = 1L;

	public CoreLoginException(Exception e) {
		super(e);
	}
	
	public CoreLoginException(String msg) {
		super(msg);
	}
	
}
