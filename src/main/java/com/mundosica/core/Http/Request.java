package com.mundosica.core.Http;

import org.json.JSONArray;
import org.json.JSONObject;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;

public class Request {
	public static String response;
	public static int status;
	public static boolean print = true;
	public static JSONObject json;
	public static JSONArray jsonArray;
	
	public static  boolean post(String url,String name,String value) {
		try {
			HttpResponse response = Unirest.post(url)
					.field(name, value).asString();
			Request.response = response.getBody().toString();
			Request.status = response.getStatus();
			if(Request.print) {
				System.out.println("URL : " + url);
				System.out.println("status: " + Request.status);
				System.out.println("enviado: "+name+": "+value);
				System.out.println("recibido: "+Request.response);
			}
			return true;
		} catch(Exception e) {
			System.out.println(e.toString());
			return false;
		}
	}
	
	public static boolean postJObject(String url,String name,String value) {
		if(Request.post(url,name,value)) {
			try {
				Request.json = new JSONObject (Request.response);
				return true;
			} catch (Exception e) {
				System.out.println(e.toString());
				return false;
			}
		}
		return false;
	}
	
	public static boolean postJArray(String url,String name,String value) {
		if(Request.post(url,name,value)) {
			try {
				Request.jsonArray = new JSONArray(Request.response);
				if(Request.print) {
					System.out.println("tamanho del arreglo: " + Request.jsonArray.length());
				}
				return true;
			} catch (Exception e) {
				System.out.println(e.toString());
				return false;
			}
		}
		return false;
	}

}
